﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generator : MonoBehaviour
{
    [SerializeField] bool lastGenerator;
    public bool activated;
    Animator anim;
    [SerializeField] GameObject lastCutscene;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        activated = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        { activated = true; anim.SetBool("activated",true);
            //if (lastGenerator) { lastCutscene.SetActive(true); } 
        }
    }
}
