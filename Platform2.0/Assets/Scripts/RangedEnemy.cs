﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : MonoBehaviour
{
    [SerializeField] float health;
    [SerializeField] float speed;
    [SerializeField] float triggerDistance;
    [SerializeField] float bulletForce;
    [SerializeField] float fireRate;
    [SerializeField] float cooldownBreak;
    Player player;
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject explosionParticles;

    int burstCount;
    float cooldownTime;
    bool isCoolingDown;

    Rigidbody2D myRigidBody;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        myRigidBody = GetComponent<Rigidbody2D>();
        cooldownTime = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isCoolingDown) { Move(); }
        Flip();
        BurstCoolDown();
        if (IsSeingPlayer())
        {
            Burst();
        }
    }

    private void Move()
    {
        myRigidBody.velocity = new Vector2(speed, myRigidBody.velocity.y);
    }

    private void Flip()
    {
        if (!IsSeingPlayer() && cooldownTime >= cooldownBreak)
        {
            float direction = Mathf.Sign(myRigidBody.velocity.x);
            Vector2 newScale = new Vector2(direction * 1.5f, transform.localScale.y);
            transform.localScale = newScale;
        }

        else
        {
            if (player.transform.position.x - transform.position.x > Mathf.Epsilon)
            {
                Vector2 newScale = new Vector2(1.5f, transform.localScale.y);
                transform.localScale = newScale;
            }
            else if (player.transform.position.x - transform.position.x < Mathf.Epsilon)
            {
                Vector2 newScale = new Vector2(-1.5f, transform.localScale.y);
                transform.localScale = newScale;
            }
        }

    }

    private bool IsSeingPlayer()
    {
        var distToPlayer = Vector2.Distance(player.transform.position,
            transform.position);
        return distToPlayer <= triggerDistance;
    }

    private void Shoot()
    {
        //mudar a direção do tiro
        if (player.transform.position.x - transform.position.x > Mathf.Epsilon)
        {
            GameObject bullets = Instantiate(bullet,
           new Vector3(transform.position.x, transform.position.y, transform.position.z),
           Quaternion.identity) as GameObject;
            bullets.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletForce, 0f));
            burstCount++;
            cooldownTime = 0;
            Destroy(bullets.gameObject, 5);
        }
        if (player.transform.position.x - transform.position.x < Mathf.Epsilon)
        {
            GameObject bullets = Instantiate(bullet,
           new Vector3(transform.position.x, transform.position.y, transform.position.z),
           Quaternion.identity) as GameObject;
            bullets.GetComponent<Rigidbody2D>().AddForce(new Vector2(-bulletForce, 0f));
            burstCount++;
            cooldownTime = 0;
            Destroy(bullets.gameObject, 5);
        }
    }

    private void Burst()
    {
        if (!IsInvoking("Shoot") && burstCount < 3) { Invoke("Shoot", fireRate); }
    }

    private void BurstCoolDown()
    {
        //print("is coolingdown: " + isCoolingDown);
        print(cooldownTime);
        if(burstCount > 2) { cooldownTime += Time.deltaTime; }
        if(cooldownTime <  cooldownBreak)
        {
            myRigidBody.velocity = new Vector2(0f, 0f);
            isCoolingDown = true;
        }
        else if (cooldownTime >= cooldownBreak)
        {
            isCoolingDown = false;
            burstCount = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (damageDealer) { return; }
        speed *= -1;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (collision.gameObject.GetComponent<DamageDealer>() != null)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            if (health <= 0)
            {
                Destroy(gameObject);
                GameObject explosion = Instantiate(explosionParticles,
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    Quaternion.identity) as GameObject;
                Destroy(explosion.gameObject, 1f);
            }
        }
    }
}
