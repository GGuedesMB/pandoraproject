﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirForce : MonoBehaviour
{
    [Header("1=direita; 2=baixo; 3=esquerda; 4=cima")]
    [Range(1, 4)] [SerializeField] int direction;
    [SerializeField] float airForce;

    public Vector2 GetAirPushVector()
    {
        if (direction == 1) { return new Vector2(airForce, 0); }
        else if (direction == 2) { return new Vector2(0, -airForce); }
        else if (direction == 3) { return new Vector2(-airForce, 0); }
        else if (direction == 4) { return new Vector2(0, airForce); }
        else return new Vector2(0, 0);
    }
}
