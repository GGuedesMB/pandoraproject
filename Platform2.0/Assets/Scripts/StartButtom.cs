﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartButtom : MonoBehaviour
{
    [SerializeField] bool end;
    private void Start()
    {
        if (end) { Invoke("GoToStartMenu", 25); }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void GoToStartMenu()
    {
        SceneManager.LoadScene(0);
    }
}
