﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticGate : MonoBehaviour
{
    [SerializeField] int openCamera;
    [SerializeField] bool startClosed;
    [SerializeField] bool onlyOpen;
    [SerializeField] bool closePermanentlyAfterPassing;
    [SerializeField] bool openAndCloseUnlimitly;
    [SerializeField] float openingSize;
    [SerializeField] float openingSpeed;
    [SerializeField] float closingSpeed;

    Player player;
    int playerHealth;

    Vector3 originalPos;
    Vector3 openingTargetPos;
    bool activated;
    bool closed;
    bool turnItOff;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
       
        originalPos = transform.position;
        openingTargetPos = new Vector3(originalPos.x, originalPos.y + openingSize, originalPos.z);
        if (startClosed)
        {
            transform.position = openingTargetPos;
            closed = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        playerHealth = player.GetComponent<Player>().GetHealth();
        if (transform.position == originalPos) { closed = true; }
        if (onlyOpen)
        {
            if (closed && activated)
            {
                transform.position = Vector2.MoveTowards(transform.position, openingTargetPos, openingSpeed * Time.deltaTime);
            }
        }
        else if (closePermanentlyAfterPassing)
        {
            if(closed && activated && !turnItOff)
            {
                transform.position = Vector2.MoveTowards(transform.position, openingTargetPos, openingSpeed * Time.deltaTime);
                if(transform.position.y >= openingTargetPos.y)
                {
                    activated = false;
                    closed = false;
                }
            }
            if(!closed && activated)
            {
                transform.position = Vector2.MoveTowards(transform.position, originalPos, closingSpeed * Time.deltaTime);
                turnItOff = true;
            }
        }

        if (playerHealth <= 0)
        {
            transform.position = openingTargetPos;
            closed = false;
            activated = false;
        }
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Player>() != null)
        {
            activated = true;
        }
    }
}
