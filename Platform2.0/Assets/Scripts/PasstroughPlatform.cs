﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PasstroughPlatform : MonoBehaviour
{
    //[SerializeField] Player player;
    BoxCollider2D myCollider;
    bool isColliding;
    float buttomTimer;
    public static float myFeetTimer;

    Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        myCollider = GetComponent<BoxCollider2D>();
        isColliding = false;
        buttomTimer = 2;
        myFeetTimer = 2;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Timer: " + myFeetTimer);
        //Debug.Log("OEOEOE"+buttomTimer);
        buttomTimer += Time.deltaTime;
        //if(player.GetComponent<Rigidbody2D>().velocity.y == Mathf.Epsilon) { isColliding = false; }
        if(player.GetComponent<Rigidbody2D>().velocity.y >= Mathf.Epsilon || isColliding || buttomTimer < 0.2f)
        {
            myCollider.enabled = false;
        }
        else if (player.GetComponent<Rigidbody2D>().velocity.y <= Mathf.Epsilon &&
            !isColliding && buttomTimer > 0.2f) { myCollider.enabled = true; }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            buttomTimer = 0;
            if (myCollider.IsTouchingLayers(LayerMask.GetMask("Player"))) { myFeetTimer = 0; }
        }
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            //Debug.Log("PORQUE NAO TA INDO");
            isColliding = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            isColliding = false;
        }
    }

    public bool GetIsColliding()
    {
        return isColliding;
    }


}
