﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualFeedback : MonoBehaviour
{
    [SerializeField] GameObject takeDamagePanel;

    // Start is called before the first frame update
    void Start()
    {
        takeDamagePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DamageFeedback()
    {
        takeDamagePanel.SetActive(true);
        Invoke("Fade", 2);
    }

    private void Fade()
    {
        takeDamagePanel.SetActive(false);
    }
}
