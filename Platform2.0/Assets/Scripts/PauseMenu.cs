﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pausePanel;
    [SerializeField] GameObject menuConfirmationPanel;
    [SerializeField] GameObject quitConfirmationPanel;
    [SerializeField] GameObject optionsPanel;

    public static bool isGamePaused;

    private void Start()
    {
        pausePanel.SetActive(false);
        menuConfirmationPanel.SetActive(false);
        quitConfirmationPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGamePaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }

    void Pause()
    {
        pausePanel.SetActive(true);
        Time.timeScale = 0;
        isGamePaused = true;
    }

    public void Resume()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1;
        isGamePaused = false;
    }

    public void Options()
    {
        optionsPanel.SetActive(true);
    }
    
    public void MenuConfirmation()
    {
        menuConfirmationPanel.SetActive(true);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
        Time.timeScale = 1;
        isGamePaused = false;
    }

    public void Return()
    {
        menuConfirmationPanel.SetActive(false);
        quitConfirmationPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }

    public void QuitConfirmation()
    {
        quitConfirmationPanel.SetActive(true);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
