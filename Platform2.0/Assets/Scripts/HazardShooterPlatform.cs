﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardShooterPlatform : MonoBehaviour
{
    [SerializeField] bool f2;
    [SerializeField] bool normalShooter;
    [SerializeField] Vector2 cameraActivityRange;
    [SerializeField] bool bossHS;
    [SerializeField] int resetCamera;
    [SerializeField] bool colliderTrigger;
    [SerializeField] bool shootLeft;
    [SerializeField] bool shootRight;
    [SerializeField] bool shootUp;
    [SerializeField] bool shootDown;

    //
    [SerializeField] bool hasGravity;

    [SerializeField] float startCounter;
    [SerializeField] float startTime;
    [SerializeField] float secondsBetweenShots;
    [SerializeField] float fireForce;
    [SerializeField] float destroyAfter;

    [SerializeField] Vector2 projectileScale;
    [SerializeField] GameObject underGravityProjectile;
    [SerializeField] GameObject noGravityProjectile;

    Animator myAnimator;

    bool isActivated;
    float timer;

    Player player;
    CameraController cameraController;
    int playerHealth;

    Collider2D myCollider;

    float timeCounter1;
    float timeCounter2;
    float timeCounter3;
    float timeCounter4;

    Som som;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        cameraController = FindObjectOfType<CameraController>();
        timeCounter1 = startTime;
        timeCounter2 = startTime;
        timeCounter3 = startTime;
        timeCounter4 = startTime;
        myCollider = GetComponent<Collider2D>();
        if (!f2) { myAnimator = GetComponent<Animator>(); }
        som = FindObjectOfType<Som>();
    }

    // Update is called once per frame
    void Update()
    {
        if (bossHS) { return; }
        if (normalShooter && 
            (cameraController.cameraIndex < cameraActivityRange.x || cameraController.cameraIndex > cameraActivityRange.y)) { return; }
        if (!normalShooter && cameraController.cameraIndex >= 18) { return; }
        playerHealth = player.GetComponent<Player>().GetHealth();
        if (isActivated) { timer += Time.deltaTime; }

        if(timer > startCounter || !colliderTrigger)
        {
            if (shootUp) { ShootProjectileUpwards(); }
            if (shootLeft) { ShootProjectileLeftward(); }
            if (shootDown) { ShootProjectileDownwards(); }
            if (shootRight) { ShootProjectileRightward(); }
        }
        if (cameraController.cameraIndex <= resetCamera)
        {
            isActivated = false;
            timer = 0;
            if (myCollider != null) { myCollider.enabled = true; }
        }
    }

    public void SingleShot()
    {
        som.Canhao_Tiro(transform.position);
        if (!f2) { myAnimator.SetTrigger("shoot"); }
        GameObject projectile = Instantiate(noGravityProjectile,
                new Vector3(transform.position.x, transform.position.y - 0.5f, -1),
                Quaternion.identity) as GameObject;
        projectile.GetComponent<Transform>().localScale = projectileScale;
        projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -fireForce));
        timeCounter2 = 0;
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    private void ShootProjectileUpwards()
    {
        timeCounter4 += Time.deltaTime;
        if (hasGravity)
        {
            if (timeCounter4 > secondsBetweenShots)
            {
                GameObject projectile = Instantiate(underGravityProjectile,
                new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, fireForce));
                timeCounter4 = 0;
            }
        }
        else
        {
            if (timeCounter4 > secondsBetweenShots)
            {
                som.Canhao_Tiro(transform.position);
                if (!f2) { myAnimator.SetTrigger("shoot"); }
                GameObject projectile = Instantiate(noGravityProjectile,
                new Vector3(transform.position.x, transform.position.y + 0.5f, -1),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, fireForce));
                timeCounter4 = 0;
            }
        }
    }

    private void ShootProjectileDownwards()
    {
        timeCounter2 += Time.deltaTime;
        if (hasGravity)
        {
            if (timeCounter2 > secondsBetweenShots)
            {
                GameObject projectile = Instantiate(underGravityProjectile,
                new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -fireForce));
                timeCounter2 = 0;
            }
        }
        else
        {
            if (timeCounter2 > secondsBetweenShots)
            {
                som.Canhao_Tiro(transform.position);
                if (!f2) { myAnimator.SetTrigger("shoot"); }
                GameObject projectile = Instantiate(noGravityProjectile,
                new Vector3(transform.position.x, transform.position.y - 0.5f, -1),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -fireForce));
                timeCounter2 = 0;
            }
        }
    }

    private void ShootProjectileLeftward()
    {
        timeCounter3 += Time.deltaTime;
        if (hasGravity)
        {
            if (timeCounter3 > secondsBetweenShots)
            {
                GameObject projectile = Instantiate(underGravityProjectile,
                new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fireForce, 0f));
                timeCounter3 = 0;
            }
        }
        else
        {
            if (timeCounter3 > secondsBetweenShots)
            {
                som.Canhao_Tiro(transform.position);
                if (!f2) { myAnimator.SetTrigger("shoot"); }
                GameObject projectile = Instantiate(noGravityProjectile,
                new Vector3(transform.position.x - 0.5f, transform.position.y, -1),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fireForce,0f));
                timeCounter3 = 0;
            }
        }
    }

    private void ShootProjectileRightward()
    {
        timeCounter1 += Time.deltaTime;
        if (hasGravity)
        {
            if (timeCounter1 > secondsBetweenShots)
            {

                GameObject projectile = Instantiate(underGravityProjectile,
                new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(fireForce, 0f));
                timeCounter1 = 0;
            }
        }
        else
        {
            if (timeCounter1 > secondsBetweenShots)
            {
                som.Canhao_Tiro(transform.position);
                if (!f2) { myAnimator.SetTrigger("shoot"); }
                GameObject projectile = Instantiate(noGravityProjectile,
                new Vector3(transform.position.x + 0.5f, transform.position.y, -1),
                Quaternion.identity) as GameObject;
                projectile.GetComponent<Transform>().localScale = projectileScale;
                projectile.GetComponent<Rigidbody2D>().AddForce(new Vector2(fireForce, 0f));
                timeCounter1 = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            isActivated = true;
            if (myCollider != null) { myCollider.enabled = false; }
        }
    }
}
