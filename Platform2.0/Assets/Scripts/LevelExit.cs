﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    [SerializeField] float coroutineTimer;
    [SerializeField] Animator fadeOut;

    public IEnumerator ExitLevel()
    {
        yield return new WaitForSecondsRealtime(coroutineTimer);
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene+1);
    }

    private void Fade()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(ExitLevel());
    }
}
