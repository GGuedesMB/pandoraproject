﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlayer : MonoBehaviour
{

    [SerializeField] GameObject[] players;

    Collider2D myCollider;
    PasstroughPlatform[] pp;
    AutomaticGate[] ag;
    FlyingEnemy[] fe;
    CheckpointPlatform[] cp;
    CameraShake[] cs;
    CameraController[] cc;
    CutsceneManager[] cm;
    HazardShooterPlatform[] hs;
    HudUI[] hu;
    Enemy3[] e3;

    public Vector2 player0Pos;

    public static bool spaceSuit;

    float timer;
    // Start is called before the first frame update
    void Start()
    {
        pp = FindObjectsOfType<PasstroughPlatform>();
        cs = FindObjectsOfType<CameraShake>();
        cc = FindObjectsOfType<CameraController>();
        cm = FindObjectsOfType<CutsceneManager>();
        ag = FindObjectsOfType<AutomaticGate>();
        hs = FindObjectsOfType<HazardShooterPlatform>();
        fe = FindObjectsOfType<FlyingEnemy>();
        hu = FindObjectsOfType<HudUI>();
        e3 = FindObjectsOfType<Enemy3>();

        spaceSuit = false;
        myCollider = GetComponent<Collider2D>();
        for(int i = 0; i < players.Length; i++)
        {
            if(i == 0) { players[i].SetActive(true); }
            else { players[i].SetActive(false); }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //timer += Time.deltaTime;
        //if(timer > 1 && !spaceSuit) { players[0].SetActive(true); }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //PlayerChange();
    }

    public void PlayerChange()
    {
        player0Pos = players[0].transform.position;
        players[0].SetActive(false);
        players[1].SetActive(true);
        spaceSuit = true;
        myCollider.enabled = false;
        Invoke("FindPlayerOnScripts", 0.6f);
        players[1].transform.position = player0Pos;
    }

    void FindPlayerOnScripts()
    {
        Debug.Log("Calling the function");
        for (int i = 0; i < pp.Length; i++) { pp[i].FindPlayer(); }
        for (int i = 0; i < cs.Length; i++) { cs[i].FindPlayer(); }
        for (int i = 0; i < cc.Length; i++) { cc[i].FindPlayer(); }
        for (int i = 0; i < cm.Length; i++) { cm[i].FindPlayer(); }
        for (int i = 0; i < ag.Length; i++) { ag[i].FindPlayer(); }
        for (int i = 0; i < hs.Length; i++) { hs[i].FindPlayer(); }
        for (int i = 0; i < fe.Length; i++) { fe[i].FindPlayer(); }
        for (int i = 0; i < hu.Length; i++) { hu[i].FindPlayer(); }
    }
}
