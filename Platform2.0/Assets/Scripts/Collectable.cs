﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    [SerializeField] Vector2 shield;
    [SerializeField] Vector2 tecnology;
    [SerializeField] Player player;

    Animator myAnimator;

    int shieldAddition;
    int tecnologyAddition;

    bool off;

    private void Start()
    {
        shieldAddition = (int)Random.Range(shield.x,shield.y);
        tecnologyAddition = (int)Random.Range(tecnology.x, tecnology.y);
        myAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        myAnimator.SetBool("off",off);
    }

    public int GetShield()
    {
        return shieldAddition;
    }

    public int GetTecnology()
    {
        return tecnologyAddition;
    }

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null) { Debug.Log("PH: " + player.GetHealth2());
            Debug.Log("MS: " + GameGlobalInfo.maxShields);
        }
        if (collision.gameObject.GetComponent<Player>() != null && player.GetHealth2() != GameGlobalInfo.maxShields)
        {
            TurnOff();
        }
    }*/

    public void TurnOff()
    {
        off = true; GetComponent<Collider2D>().enabled = false;
    }
}
