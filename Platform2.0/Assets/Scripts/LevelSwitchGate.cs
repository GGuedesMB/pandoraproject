﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//FAZER UM COLISOR NO LEVEL INTEIRO
//colocar um serializefield no colisor que passe o número da VCâmera;


public class LevelSwitchGate : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] int gateForEachRoom;
    [SerializeField] bool isReturning;
    [SerializeField] CameraShake[] cameraShake;

    CameraController cameraController;
    EnemySpawner enemySpawner;
    Collider2D myCollider;
    //CameraShake cameraShake;

    public bool nextLevel;
    bool[] canSpawn;
    // Start is called before the first frame update
    void Start()
    {
        cameraController = FindObjectOfType<CameraController>();
        myCollider = GetComponent<Collider2D>();
        enemySpawner = FindObjectOfType<EnemySpawner>();
        canSpawn = new bool[cameraShake.Length];
        for (int i = 0; i < canSpawn.Length; i++) { canSpawn[i] = true; };
    }

    // Update is called once per frame
    void Update()
    {
        if (gateForEachRoom == cameraController.cameraIndex) { myCollider.enabled = false; }
        else { myCollider.enabled = true; }
        if (player == null) { return; }
        if (player.GetHealth2() <= -1) { for (int i = 0; i < canSpawn.Length; i++) { canSpawn[i] = true; } ; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1")) //&& !isReturning)
        {
            cameraController.cameraIndex = gateForEachRoom;

            for (int i = 0; i < cameraShake.Length; i++)
            {
                if (Mathf.Abs(i - gateForEachRoom) > 1) { canSpawn[i] = true; }
            }
            if (canSpawn[gateForEachRoom])
            {
                cameraShake[gateForEachRoom].SpawnEnemies();
                canSpawn[gateForEachRoom] = false;
            }
        }
    }
}
