﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidePlatform : MonoBehaviour
{
    [SerializeField] float secondsTurnedOn;
    [SerializeField] float secondsTurnedOff;
    [SerializeField] float inicialTime;

    BoxCollider2D myCollider;

    bool isTurnedOff;
    float time;
    bool inFront;

    // Start is called before the first frame update
    void Start()
    {
        myCollider = GetComponent<BoxCollider2D>();
        time = inicialTime;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time < secondsTurnedOn) { myCollider.enabled = true; }
        if (time >= secondsTurnedOn) { myCollider.enabled = false; }
        if (time >= secondsTurnedOn + secondsTurnedOff && !inFront) { time = 0; }
        Debug.Log("inFront: " + inFront);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            inFront = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            inFront = false;
        }
    }
}
