﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    //Config parameters
    [SerializeField] float bulletSpeed = 20;
    [SerializeField] bool blocker;
    [SerializeField] GameObject impactParticles;
    [SerializeField] GameObject enemyParticles;

    Rigidbody2D myRigidBody;
    Som som;

    private void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        som = FindObjectOfType<Som>();
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.gameObject.GetComponent<PlatformProjectile>() != null && !blocker) || 
            collision.gameObject.GetComponent<LevelSwitchGate>() != null ||
            collision.gameObject.GetComponent<HazardShooterPlatform>() != null ||
            collision.gameObject.GetComponent<Collectable>() != null) { return; }

        Debug.Log(collision.name);

        som.Pj_BulletCollision(transform.position);
        GameObject bulletParticles = Instantiate(impactParticles, new Vector3(transform.position.x, 
            transform.position.y, -1), Quaternion.identity);
        //Debug.Log("Velocidade bala: " + transform.localScale.x *
            //myRigidBody.velocity.x / Mathf.Abs(myRigidBody.velocity.x));
        float rotY;
        if(myRigidBody.velocity.x > 0) { rotY = 0; }
        else { rotY = 180; }

        bulletParticles.transform.rotation = Quaternion.Euler( 0, rotY, 0);
        Destroy(bulletParticles, 2);

        if (collision.gameObject.GetComponent<Enemy1>() != null ||
    collision.gameObject.GetComponent<Enemy2>() != null ||
    collision.gameObject.GetComponent<FlyingEnemy>() != null || collision.gameObject.GetComponent<Boss>() != null)
        {
            som.Ene_Recebe_Dano(transform.position);
            GameObject enemyImpact = Instantiate(enemyParticles, new Vector3(transform.position.x,
            transform.position.y, -1), Quaternion.identity);
            //Debug.Log("Velocidade bala: " + transform.localScale.x *
                //myRigidBody.velocity.x / Mathf.Abs(myRigidBody.velocity.x));
            if (myRigidBody.velocity.x > 0) { rotY = 0; }
            else { rotY = 180; }

            enemyImpact.transform.rotation = Quaternion.Euler(0, rotY, 0);
            Destroy(enemyImpact, 2);
        }

        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject bulletParticles = Instantiate(impactParticles, new Vector3(transform.position.x,
            transform.position.y, -1), Quaternion.identity);
        Debug.Log("Velocidade bala: " + transform.localScale.x *
            myRigidBody.velocity.x / Mathf.Abs(myRigidBody.velocity.x));
        float rotY;
        if (myRigidBody.velocity.x > 0) { rotY = 0; }
        else { rotY = 180; }

        bulletParticles.transform.rotation = Quaternion.Euler(0, rotY, 0);
        Destroy(bulletParticles, 2);
        Destroy(gameObject);
    }

    public float GetBulletSpeed()
    {
        return bulletSpeed;
    }
}
