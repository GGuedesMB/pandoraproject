﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundEnemy : MonoBehaviour
{
    [SerializeField] float health;
    [SerializeField] float speed;
    [SerializeField] float triggerDistance;
    [SerializeField] float attackTriggerDistance;
    [SerializeField] float attackSpeed;
    [SerializeField] float attackAntecipation;
    [SerializeField] float attackDuration;
    [SerializeField] float cooldownTime;
    [SerializeField] float followSpeed;

    Player player;
    [SerializeField] GameObject arm;
    [SerializeField] GameObject explosionParticles;

    float timeToAttack;
    bool willAttack;
    bool isAttacking;

    Rigidbody2D myRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<Player>();
        timeToAttack = 3;
    }

    // Update is called once per frame
    void Update()
    {
        //print("Sign: " + Mathf.Sign(myRigidBody.velocity.x));
        Flip();
        IsThePlayerClose();
        if (!IsSeingPlayer() && !isAttacking)
        {
            Push();
            Move();
        }
        else if (IsSeingPlayer() && !isAttacking) { FollowPlayer(); }
        
    }

    private void Move()
    {
        myRigidBody.velocity = new Vector2(speed, myRigidBody.velocity.y);
    }

    private void Push()
    {
        if (myRigidBody.velocity.x == 0)
        {
            //print("HEYYOU");
            myRigidBody.AddForce(new Vector2(500 * Random.Range(-1,1), 0f));
        }
    }

    private void Flip()
    {
        float direction = Mathf.Sign(myRigidBody.velocity.x);
        if(isAttacking && player.transform.position.x - transform.position.x < Mathf.Epsilon)
        {
            direction = -1;
        }
        Vector2 newScale = new Vector2(direction, transform.localScale.y);
        transform.localScale = newScale;
    }

    private void IsThePlayerClose()
    {
        //print(myRigidBody.velocity);
        var distToPlayer = Vector2.Distance(player.transform.position,
            transform.position);
        if (distToPlayer <= attackTriggerDistance && timeToAttack > cooldownTime)
        {
            timeToAttack = 0;
            willAttack = true;
            isAttacking = true;
        }
        if(willAttack == true && !IsInvoking("Attack"))
        {
            Invoke("Attack", attackAntecipation); 
        }
        if (isAttacking)
        {
            timeToAttack += Time.deltaTime;
            AttackStop();
            if(timeToAttack > cooldownTime) { isAttacking = false; }
        }
    }

    private bool IsSeingPlayer()
    {
        var distToPlayer = Vector2.Distance(player.transform.position,
            transform.position);
        return distToPlayer <= triggerDistance;
    }

    private void FollowPlayer()
    {       
        if (player.transform.position.x - transform.position.x > Mathf.Epsilon)
        { myRigidBody.velocity = new Vector2(followSpeed, 0f); }
        else if (player.transform.position.x - transform.position.x < Mathf.Epsilon)
        { myRigidBody.velocity = new Vector2(-followSpeed, 0f); }
    }

    private void Attack()
    {
        if (player.transform.position.x - transform.position.x > Mathf.Epsilon)
        {
            GameObject enemyArm = Instantiate(arm, new Vector3(transform.position.x + 1f,
            transform.position.y + 1.25f, transform.position.z), Quaternion.identity) as GameObject;
            enemyArm.transform.rotation = Quaternion.Euler(0, 0, 0);
            enemyArm.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -attackSpeed));
            Destroy(enemyArm.gameObject, 1);
        }
        if (player.transform.position.x - transform.position.x < Mathf.Epsilon)
        {
            GameObject enemyArm = Instantiate(arm, new Vector3(transform.position.x - 1f,
            transform.position.y + 1.25f, transform.position.z), Quaternion.identity) as GameObject;
            enemyArm.transform.rotation = Quaternion.Euler(0, 0, 180);
            enemyArm.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -attackSpeed));
            Destroy(enemyArm.gameObject, 1);
        }
        
        willAttack = false;
    }

    private void AttackStop()
    {
        myRigidBody.velocity = new Vector2(0f, 0f);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Debug.Log("hey");
        DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (damageDealer) { return; }
        if (!IsSeingPlayer())
        {
            speed *= -1;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (collision.gameObject.GetComponent<DamageDealer>() != null)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            if (health <= 0)
            {
                Destroy(gameObject);
                GameObject explosion = Instantiate(explosionParticles,
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    Quaternion.identity) as GameObject;
                Destroy(explosion.gameObject, 1f);
            }
        }
    }
}
