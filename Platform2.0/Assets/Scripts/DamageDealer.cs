﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{

    //Config parameters
    [SerializeField] int damage;

    Bullet bullet;

    private void Start()
    {
        bullet = GetComponent<Bullet>();
    }


    public int GetDamage()
    {
        return damage;
    }
}
