﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class VcControler : MonoBehaviour
{

    CinemachineVirtualCamera vcam;

    // Start is called before the first frame update
    void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StopFollowing()
    {
        vcam.Follow = null;
    }

    public void StartFollowing()
    {
        vcam.Follow = FindObjectOfType<Player>().GetComponent<Transform>();
    }

    public IEnumerator Shake(float duration)
    {
        vcam.Follow = null;
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }
        vcam.Follow = FindObjectOfType<Player>().GetComponent<Transform>();
    }
}
