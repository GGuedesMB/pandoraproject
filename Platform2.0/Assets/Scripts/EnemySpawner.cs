﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] Transform[] wave1Pos;
    [SerializeField] Transform[] wave2Pos;
    [SerializeField] Transform[] wave3Pos;
    [SerializeField] Transform[] wave4Pos;
    [SerializeField] Transform[] wave5Pos;
    [SerializeField] Transform[] wave6Pos;
    [SerializeField] Transform[] wave7Pos;
    [SerializeField] GameObject[] wave1;
    [SerializeField] GameObject[] wave2;
    [SerializeField] GameObject[] wave3;
    [SerializeField] GameObject[] wave4;
    [SerializeField] GameObject[] wave5;
    [SerializeField] GameObject[] wave6;
    [SerializeField] GameObject[] wave7;
    [SerializeField] int[] cameras;

    CameraController cameraController;

    GameObject[] enemies;
    Vector2[] positions;

    public int spawn;
    int lastCamera;

    public float auxiliarTimer;
    // Start is called before the first frame update
    void Start()
    {
        cameraController = FindObjectOfType<CameraController>();
        spawn = 1;
        lastCamera = cameraController.cameraIndex;
        //cameras = new int[cameras.Length];
    }

    // Update is called once per frame
    void Update()
    {
        auxiliarTimer += Time.deltaTime;
        /*if(lastCamera != cameraController.cameraIndex && auxiliarTimer > 0.5f)
        {
            Debug.Log("ÓLÁCAMILA");
            lastCamera = cameraController.cameraIndex;
            auxiliarTimer = 0;
        }*/
        if (cameraController.cameraIndex == 7 && auxiliarTimer > 0.3f && spawn == 0)
        { InstantiateEnemies(wave1Pos, wave1); }
        else if (cameraController.cameraIndex == cameras[1] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave2Pos, wave2); }
        else if (cameraController.cameraIndex == cameras[2] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave3Pos, wave3); }
        else if (cameraController.cameraIndex == cameras[3] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave4Pos, wave4); }
        else if (cameraController.cameraIndex == cameras[4] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave5Pos, wave5); }
        else if (cameraController.cameraIndex == cameras[5] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave6Pos, wave6); }
        else if (cameraController.cameraIndex == cameras[6] && spawn == 0 && auxiliarTimer > 0.3f)
        { InstantiateEnemies(wave7Pos, wave7); }
    }

    public void InstantiateEnemies(Transform[] posArray, GameObject[] enemiesArray)
    {
        //auxiliarTimer += Time.deltaTime;
        Debug.Log("Spawning" + auxiliarTimer);
        Debug.Log("Camera" + cameraController.cameraIndex);
        enemies = new GameObject[enemiesArray.Length];
        for (int i = 0; i < posArray.Length; i++)
        {
            if(GameObject.Find(enemiesArray[i].name+"(Clone)")) { Debug.Log("ACHEI O MISERAVEL"); }
            else { enemies[i] = Instantiate(enemiesArray[i], posArray[i].transform.position, Quaternion.identity); } 
        }
        spawn = 1;
    }
}
