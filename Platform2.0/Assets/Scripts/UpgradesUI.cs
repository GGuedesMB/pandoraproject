﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradesUI : MonoBehaviour
{
    [SerializeField] GameObject[] equipedText;
    [SerializeField] GameObject[] purchased;
    [SerializeField] GameObject upgradePanel;
    [SerializeField] Button[] gunOptions; 
    [SerializeField] Button[] suitOptions; 
    [SerializeField] Button[] pjOptions; 
    //[SerializeField] GameObject playerPanel;
    [SerializeField] Button test;
    [SerializeField] TextMeshProUGUI tech;


    [SerializeField] int bullet2Price;
    [SerializeField] int bullet3Price;
    [SerializeField] int bullet4Price;
    [SerializeField] int bullet5Price;

    [SerializeField] int suit2Price;
    [SerializeField] int suit3Price;
    [SerializeField] int suit4Price;

    [SerializeField] int pj2Price;
    [SerializeField] int pj3Price;
    [SerializeField] int pj4Price;

    public static bool isAtUpgradeUI;

    bool showGunUpgrades;
    bool showSuitUpgrades;
    bool showPjUpgrades;

    Som som;


    // Start is called before the first frame update
    void Start()
    {
        som = FindObjectOfType<Som>();
        upgradePanel.SetActive(false);
        /*gunPanel.SetActive(false);
        spacesuitPanel.SetActive(false);
        habilitiesPanel.SetActive(false);
        playerPanel.SetActive(false);*/
        /*showGunUpgrades = false;
        showSuitUpgrades = false;
        showPjUpgrades = false;*/
        
    }

    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetKeyDown(KeyCode.U) && !isAtUpgradeUI && !PauseMenu.isGamePaused && GameGlobalInfo.canAccessUpgrades)
        {
            GoToUpgradeUI();
            showGunUpgrades = false;
            showSuitUpgrades = false;
            showPjUpgrades = false;
        }

        if (GameGlobalInfo.bulletChip == 2) { equipedText[0].SetActive(true); } else { equipedText[0].SetActive(false); }
        if (GameGlobalInfo.bulletChip == 3) { equipedText[1].SetActive(true); } else { equipedText[1].SetActive(false); }
        if (GameGlobalInfo.bulletChip == 4) { equipedText[2].SetActive(true); } else { equipedText[2].SetActive(false); }
        if (GameGlobalInfo.bulletChip == 5) { equipedText[3].SetActive(true); } else { equipedText[3].SetActive(false); }

        if (GameGlobalInfo.suitChip == 2) { equipedText[4].SetActive(true); } else { equipedText[4].SetActive(false); }
        if (GameGlobalInfo.suitChip == 3) { equipedText[5].SetActive(true); } else { equipedText[5].SetActive(false); }
        if (GameGlobalInfo.suitChip == 4) { equipedText[6].SetActive(true); } else { equipedText[6].SetActive(false); }

        if (GameGlobalInfo.pjChip == 2) { equipedText[7].SetActive(true); } else { equipedText[7].SetActive(false); }
        if (GameGlobalInfo.pjChip == 3) { equipedText[8].SetActive(true); } else { equipedText[8].SetActive(false); }
        if (GameGlobalInfo.pjChip == 4) { equipedText[9].SetActive(true); } else { equipedText[9].SetActive(false); }

        //division

        if (GameGlobalInfo.bullet2Acquired) { purchased[0].SetActive(true); } else { purchased[0].SetActive(false); }
        if (GameGlobalInfo.bullet3Acquired) { purchased[1].SetActive(true); } else { purchased[1].SetActive(false); }
        if (GameGlobalInfo.bullet4Acquired) { purchased[2].SetActive(true); } else { purchased[2].SetActive(false); }
        if (GameGlobalInfo.bullet5Acquired) { purchased[3].SetActive(true); } else { purchased[3].SetActive(false); }

        if (GameGlobalInfo.suit2Acquired) { purchased[4].SetActive(true); } else { purchased[4].SetActive(false); }
        if (GameGlobalInfo.suit3Acquired) { purchased[5].SetActive(true); } else { purchased[5].SetActive(false); }
        if (GameGlobalInfo.suit4Acquired) { purchased[6].SetActive(true); } else { purchased[6].SetActive(false); }

        if (GameGlobalInfo.pj2Acquired) { purchased[7].SetActive(true); } else { purchased[7].SetActive(false); }
        if (GameGlobalInfo.pj3Acquired) { purchased[8].SetActive(true); } else { purchased[8].SetActive(false); }
        if (GameGlobalInfo.pj4Acquired) { purchased[9].SetActive(true); } else { purchased[9].SetActive(false); }

        if (showGunUpgrades) { for (int i = 0; i < gunOptions.Length; i++) { gunOptions[i].gameObject.SetActive(true); } }
        else { for (int i = 0; i < gunOptions.Length; i++) { gunOptions[i].gameObject.SetActive(false); } }

        if (showSuitUpgrades) { for (int i = 0; i < suitOptions.Length; i++) { suitOptions[i].gameObject.SetActive(true); } }
        else { for (int i = 0; i < suitOptions.Length; i++) { suitOptions[i].gameObject.SetActive(false); } }

        if (showPjUpgrades) { for (int i = 0; i < pjOptions.Length; i++) { pjOptions[i].gameObject.SetActive(true); } }
        else { for (int i = 0; i < pjOptions.Length; i++) { pjOptions[i].gameObject.SetActive(false); } }

        if (GameGlobalInfo.tecnology < bullet2Price && !GameGlobalInfo.bullet2Acquired) { gunOptions[0].interactable = false; } else { gunOptions[0].interactable = true; }
        if (GameGlobalInfo.tecnology < bullet3Price && !GameGlobalInfo.bullet3Acquired) { gunOptions[1].interactable = false; } else { gunOptions[1].interactable = true; }
        if (GameGlobalInfo.tecnology < bullet4Price && !GameGlobalInfo.bullet4Acquired) { gunOptions[2].interactable = false; } else { gunOptions[2].interactable = true; }
        if (GameGlobalInfo.tecnology < bullet5Price && !GameGlobalInfo.bullet5Acquired) { gunOptions[3].interactable = false; } else { gunOptions[3].interactable = true; }

        if (GameGlobalInfo.tecnology < suit2Price && !GameGlobalInfo.suit2Acquired) { suitOptions[0].interactable = false; } else { suitOptions[0].interactable = true; }
        if (GameGlobalInfo.tecnology < suit3Price && !GameGlobalInfo.suit3Acquired) { suitOptions[1].interactable = false; } else { suitOptions[1].interactable = true; }
        if (GameGlobalInfo.tecnology < suit4Price && !GameGlobalInfo.suit4Acquired) { suitOptions[2].interactable = false; } else { suitOptions[2].interactable = true; }

        if (GameGlobalInfo.tecnology < pj2Price && !GameGlobalInfo.pj2Acquired) { pjOptions[0].interactable = false; } else { pjOptions[0].interactable = true; }
        if (GameGlobalInfo.tecnology < pj3Price && !GameGlobalInfo.pj3Acquired) { pjOptions[1].interactable = false; } else { pjOptions[1].interactable = true; }
        if (GameGlobalInfo.tecnology < pj4Price && !GameGlobalInfo.pj4Acquired) { pjOptions[2].interactable = false; } else { pjOptions[2].interactable = true; }

        tech.text = GameGlobalInfo.tecnology.ToString() + "gb";
    }

    public void GoToUpgradeUI()
    {
        upgradePanel.SetActive(true);
        Time.timeScale = 1;
        isAtUpgradeUI = true;
    }

    public void Return()
    {
        som.Int_Clique(FindObjectOfType<Player>().transform.position);
        upgradePanel.SetActive(false);
        Time.timeScale = 1;
        isAtUpgradeUI = false;
        
    }
    
    public void ShowGunUpgrades()
    {
        som.Int_Clique(FindObjectOfType<Player>().transform.position);
        Debug.Log("foi"+ FindObjectOfType<Player>().transform.position);
        if (showGunUpgrades) { showGunUpgrades = false; }
        else if (!showGunUpgrades) { showGunUpgrades = true; }
        
    }

    public void ShowSpacesuitUpgrades()
    {
        som.Int_Clique(FindObjectOfType<Player>().transform.position);
        if (showSuitUpgrades) { showSuitUpgrades = false; }
        else if (!showSuitUpgrades) { showSuitUpgrades = true; }
        
    }

    public void ShowPlayerUpgrades()
    {
        som.Int_Clique(FindObjectOfType<Player>().transform.position);
        if (showPjUpgrades) { showPjUpgrades = false; }
        else if (!showPjUpgrades) { showPjUpgrades = true; }
        
    }

    public void Bullet2()
    {
        if (GameGlobalInfo.tecnology >= bullet2Price && !GameGlobalInfo.bullet2Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.bulletChip = 2;
            GameGlobalInfo.tecnology -= bullet2Price;
            GameGlobalInfo.bullet2Acquired = true;
            
        }
        else if (GameGlobalInfo.bulletChip != 2) { GameGlobalInfo.bulletChip = 2; }
        else { GameGlobalInfo.bulletChip = 1; }
    }
    public void Bullet3()
    {
        if (GameGlobalInfo.tecnology >= bullet3Price && !GameGlobalInfo.bullet3Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.bulletChip = 3;
            GameGlobalInfo.tecnology -= bullet3Price;
            GameGlobalInfo.bullet3Acquired = true;
            
        }
        else if (GameGlobalInfo.bulletChip != 3) { GameGlobalInfo.bulletChip = 3; }
        else { GameGlobalInfo.bulletChip = 1; }
    }

    public void Bullet4()
    {
        if (GameGlobalInfo.tecnology >= bullet4Price && !GameGlobalInfo.bullet4Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.bulletChip = 4;
            GameGlobalInfo.tecnology -= bullet4Price;
            GameGlobalInfo.bullet4Acquired = true;
            
        }
        else if (GameGlobalInfo.bulletChip != 4) { GameGlobalInfo.bulletChip = 4; }
        else { GameGlobalInfo.bulletChip = 1; }
    }

    public void Bullet5()
    {
        if (GameGlobalInfo.tecnology >= bullet5Price && !GameGlobalInfo.bullet5Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.bulletChip = 5;
            GameGlobalInfo.tecnology -= bullet5Price;
            GameGlobalInfo.bullet5Acquired = true;
            
        }
        else if (GameGlobalInfo.bulletChip != 5) { GameGlobalInfo.bulletChip = 5; }
        else { GameGlobalInfo.bulletChip = 1; }
    }

    public void Suit2()
    {
        if (GameGlobalInfo.tecnology < suit2Price && !GameGlobalInfo.suit2Acquired) { return; }
        else if (GameGlobalInfo.tecnology >= suit2Price && !GameGlobalInfo.suit2Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.suitChip = 2;
            GameGlobalInfo.tecnology -= suit2Price;
            GameGlobalInfo.suit2Acquired = true;
            
        }
        else if (GameGlobalInfo.suitChip != 2) { GameGlobalInfo.suitChip = 2; }
        else { GameGlobalInfo.suitChip = 1; }
    }

    public void Suit3()
    {
        if (GameGlobalInfo.tecnology < suit3Price && !GameGlobalInfo.suit3Acquired) { return; }
        else if (GameGlobalInfo.tecnology >= suit3Price && !GameGlobalInfo.suit3Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.suitChip = 3;
            GameGlobalInfo.tecnology -= suit3Price;
            GameGlobalInfo.suit3Acquired = true;
            
        }
        else if (GameGlobalInfo.suitChip != 3) { GameGlobalInfo.suitChip = 3; }
        else { GameGlobalInfo.suitChip = 1; }
    }

    public void Suit4()
    {
        if (GameGlobalInfo.tecnology < suit4Price && !GameGlobalInfo.suit4Acquired) { return; }
        if (GameGlobalInfo.tecnology >= suit4Price && !GameGlobalInfo.suit4Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.suitChip = 4;
            GameGlobalInfo.tecnology -= suit4Price;
            GameGlobalInfo.suit4Acquired = true;
            
        }
        else if (GameGlobalInfo.suitChip != 4) { GameGlobalInfo.suitChip = 4; }
        else { GameGlobalInfo.suitChip = 1; }
    }

    public void Pj2()
    {
        if (GameGlobalInfo.tecnology < pj2Price && !GameGlobalInfo.pj2Acquired) { return; }
        else if (GameGlobalInfo.tecnology >= pj2Price && !GameGlobalInfo.pj2Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.pjChip = 2;
            GameGlobalInfo.tecnology -= pj2Price;
            GameGlobalInfo.pj2Acquired = true;
            
        }
        else if (GameGlobalInfo.pjChip != 2) { GameGlobalInfo.pjChip = 2; }
        else { GameGlobalInfo.pjChip = 1; }
    }

    public void Pj3()
    {
        if (GameGlobalInfo.tecnology < pj3Price && !GameGlobalInfo.pj3Acquired) { return; }
        else if (GameGlobalInfo.tecnology >= pj3Price && !GameGlobalInfo.pj3Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.pjChip = 3;
            GameGlobalInfo.tecnology -= pj3Price;
            GameGlobalInfo.pj3Acquired = true;
            
        }
        else if (GameGlobalInfo.pjChip != 3) { GameGlobalInfo.pjChip = 3; }
        else { GameGlobalInfo.pjChip = 1; }
    }

    public void Pj4()
    {
        if (GameGlobalInfo.tecnology < pj4Price && !GameGlobalInfo.pj4Acquired) { return; }
        else if (GameGlobalInfo.tecnology >= pj4Price && !GameGlobalInfo.pj4Acquired)
        {
            som.Equip_Upgrd(FindObjectOfType<Player>().transform.position);
            Debug.Log("Upgraded");
            GameGlobalInfo.pjChip = 4;
            GameGlobalInfo.tecnology -= pj4Price;
            GameGlobalInfo.pj4Acquired = true;
            
        }
        else if (GameGlobalInfo.pjChip != 4) { GameGlobalInfo.pjChip = 4; }
        else { GameGlobalInfo.pjChip = 1; }
    }
}
