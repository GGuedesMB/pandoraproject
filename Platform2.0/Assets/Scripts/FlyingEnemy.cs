﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    [SerializeField] float triggerDistance;
    [SerializeField] float followForce;
    [SerializeField] float health;
    [SerializeField] float maxSpeedX;
    [SerializeField] float maxSpeedY;
    [SerializeField] float negativeY;
    [SerializeField] float followDelay;
    Player player;
    [SerializeField] GameObject explosionParticles;
    [SerializeField] GameObject hitFB;

    Rigidbody2D myRigidBody;
    Animator myAnimator;
    CameraController cameraController;
    

    bool sawPlayer;
    bool blocked;
    int destroyAt;

    float velocityBack;

    Vector2 forceToPlayer;
    //Vector2 startPos;

    float feedbackTimer;
    Renderer rend;
    Vector4 startColor;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        player = FindObjectOfType<Player>();
        //startPos = transform.position;
        cameraController = FindObjectOfType<CameraController>();
        destroyAt = cameraController.cameraIndex - 1;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        
    }

    // Update is called once per frame
    void Update()
    {
        velocityBack *= 0.9f;
        if (Mathf.Abs(velocityBack) < 0.5f) { velocityBack = 0; }

        feedbackTimer += Time.deltaTime;
        HitFeedBack();

        //Debug.Log("VIDA: " + player.GetComponent<Player>().GetHealth());
        if (cameraController.cameraIndex <= destroyAt && sawPlayer)
        {
            //Debug.Log("ATIVOU");
            Destroy(gameObject, 0.5f);
        }

        if (!blocked)
        {
            IsSeingPlayer();
            if (sawPlayer) { StartCoroutine(Follow()); }
            else { StopAllCoroutines(); }//{ FollowPlayer(); }
            myRigidBody.velocity = new Vector2(Mathf.Clamp(myRigidBody.velocity.x, -maxSpeedX, maxSpeedX),
                Mathf.Clamp(myRigidBody.velocity.y, -maxSpeedY + negativeY, maxSpeedY));
            //if (player.GetHealth() <= 0) { sawPlayer = false; }
        }
        else { myRigidBody.velocity = new Vector2(0,myRigidBody.velocity.y*0.9f); }
        if (Mathf.Abs(velocityBack) <= Mathf.Epsilon) { Flip(); }
        

        if (CutsceneManager.OnCutscene || DialogueManager.OnDialogue || UpgradesUI.isAtUpgradeUI) { Return(); }
       
    }

    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    private void Return()
    {
        Destroy(gameObject);
    }

    private void Flip()
    {
        float direction = Mathf.Sign(myRigidBody.velocity.x);
        Vector2 newScale = new Vector2(direction*1.5f, transform.localScale.y);
        transform.localScale = newScale;
    }

    private void IsSeingPlayer()
    {
        var distToPlayer = Vector2.Distance(player.transform.position,
            transform.position);
        if (distToPlayer <= triggerDistance)
        {
            sawPlayer = true;
            myAnimator.SetTrigger("playerDetected");
        }
            
    }

    private void FollowPlayer()
    {
        Vector3 actuallPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        Vector3 targetPos = new Vector3(player.transform.position.x,
            player.transform.position.y, player.transform.position.z);
        transform.position = Vector2.MoveTowards(actuallPos,targetPos,followForce);
    }

    IEnumerator Follow()
    {
        float magnitude;
        Vector2 dist;
        Vector2 unitVector;
        myRigidBody.AddForce(forceToPlayer);
        yield return null;
        dist = new Vector2(player.transform.position.x - transform.position.x,
            player.transform.position.y - transform.position.y);
        magnitude = Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
        unitVector = new Vector2(dist.x / magnitude, dist.y / magnitude);
        forceToPlayer = new Vector2(unitVector.x * followForce, unitVector.y * followForce);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (collision.gameObject.GetComponent<MoleColisor>() != null) { return; }
        if (collision.gameObject.CompareTag("Enemy Blocker"))
        {
            blocked = true;
        }

        if (collision.gameObject.GetComponent<Bullet>() != null && !sawPlayer)
        {
            sawPlayer = true;
            return;
        }
        if (collision.gameObject.GetComponent<DamageDealer>() != null && sawPlayer)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            feedbackTimer = 0;
            if (collision.gameObject.CompareTag("Player Bullet 3"))
            {
                if (collision.gameObject.GetComponent<Bullet>().GetComponent<Rigidbody2D>().velocity.x > Mathf.Epsilon)
                { velocityBack = 14; }
                else { velocityBack = -14; }
                Debug.Log("FoundBullet");
                myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
                Debug.Log("Myvelocity: " + myRigidBody.velocity.x);
                myRigidBody.AddForce(new Vector2(velocityBack, 0));

            }
            if (health <= 0)
            {
                Destroy(gameObject);
                /*GameObject explosion = Instantiate(explosionParticles,
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    Quaternion.identity) as GameObject;
                Destroy(explosion.gameObject, 1f);*/
            }
            /*else
            {
                GameObject hitFeedback = Instantiate(hitFB, transform.position, Quaternion.identity) as GameObject;
                hitFeedback.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y,
                    transform.localScale.z);
                hitFeedback.GetComponent<Rigidbody2D>().velocity = myRigidBody.velocity;
                Destroy(hitFeedback.gameObject, 0.1f);
            }*/
        }
    }
}
