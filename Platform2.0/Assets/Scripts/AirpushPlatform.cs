﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirpushPlatform : MonoBehaviour
{
    [SerializeField] bool isTriggerByButtom;

    [SerializeField] GameObject air;
    [SerializeField] float airPushForce;
    [SerializeField] float inicialTime;
    [SerializeField] float secondsToTurnOn;
    [SerializeField] float secondsTurnedOn;
    [SerializeField] float airLength;
    [Header ("1=direita; 2=baixo; 3=esquerda; 4=cima")]
    [Range (1,4)][SerializeField] int airPushDirection;

    float airTime;
    bool isExisting;

    // Start is called before the first frame update
    void Start()
    {
        airTime = inicialTime;
    }

    // Update is called once per frame
    void Update()
    {
        TurnOnAndOff();
    }

    private void TurnOnAndOff()
    {
        airTime += Time.deltaTime;
        if (airTime >= secondsToTurnOn && !isExisting)
        {
            if(airPushDirection == 1)
            {
                GameObject airPush = Instantiate(air,
                   new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z),
                   Quaternion.identity) as GameObject;
                Destroy(airPush.gameObject, secondsTurnedOn);
                isExisting = true;
            }
            else if(airPushDirection == 2)
            {
                GameObject airPush = Instantiate(air,
                   new Vector3(transform.position.x, transform.position.y - 1.5f, transform.position.z),
                   Quaternion.identity) as GameObject;
                airPush.transform.rotation = Quaternion.Euler(0, 0, 90);
                Destroy(airPush.gameObject, secondsTurnedOn);
                isExisting = true;
            }
            else if (airPushDirection == 3)
            {
                GameObject airPush = Instantiate(air,
                   new Vector3(transform.position.x - (airLength/2 + 0.5f), transform.position.y, transform.position.z),
                   Quaternion.identity) as GameObject;
                Destroy(airPush.gameObject, secondsTurnedOn);
                isExisting = true;
            }
            else if (airPushDirection == 4)
            {
                GameObject airPush = Instantiate(air,
                   new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z),
                   Quaternion.identity) as GameObject;
                airPush.transform.rotation = Quaternion.Euler(0, 0, 90);
                Destroy(airPush.gameObject, secondsTurnedOn);
                isExisting = true;
            }
        }

        if (airTime >= secondsToTurnOn + secondsTurnedOn)
        {
            isExisting = false;
            airTime = 0;
        }
    }
}
