﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudUI : MonoBehaviour
{
    [SerializeField] Text lifeText;
    [SerializeField] TextMeshProUGUI gunCooldown;
    [SerializeField] TextMeshProUGUI nameInput;
    [SerializeField] TextMeshProUGUI tecnology;

    Player player;

    float percentage;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        percentage = Mathf.Clamp(player.GetTimer() * 100 / player.GetGunCharge(), 0, 100);
        int percentageValue = (int)percentage;
        lifeText.text = player.GetHealth().ToString();
        tecnology.text = GameGlobalInfo.tecnology.ToString() + "gb";
        gunCooldown.text = percentageValue.ToString() + "%";
        if (GameGlobalInfo.playerName == "" || GameGlobalInfo.playerName == " " || GameGlobalInfo.playerName == null)
        { nameInput.text = "ID: Tripulante"; }
        else { nameInput.text = "ID: " + GameGlobalInfo.playerName.ToString(); }
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    
}
