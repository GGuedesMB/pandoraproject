﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EletricDamage : MonoBehaviour
{
    [SerializeField] float inicialTime;
    [SerializeField] float secondsToTurnOn;
    [SerializeField] float secondsTurnedOn;
    [SerializeField] int eletricDamage;
    [SerializeField] GameObject antecipation;
    [SerializeField] GameObject turnedOn;

    Renderer rend;

    int instantDamage;
    float eletricTime;

    // Start is called before the first frame update
    void Start()
    {
        eletricTime = inicialTime;
        rend = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        TurnOnAndOff();
        ChangeColors();
    }

    private void TurnOnAndOff()
    {
        eletricTime += Time.deltaTime;
        if (eletricTime >= secondsToTurnOn + secondsTurnedOn)
        {
            instantDamage = 0;
            eletricTime = 0;
        }
        else if (eletricTime >= secondsToTurnOn) { instantDamage = eletricDamage; }
    }


    private void ChangeColors()
    {
        //eletricTime += Time.deltaTime;
        if (eletricTime > secondsToTurnOn - 1 && eletricTime < secondsToTurnOn - 0.85)
        {
            GameObject warning = Instantiate(antecipation, 
                new Vector3(transform.position.x, transform.position.y,-7), 
                Quaternion.identity) as GameObject;
            warning.transform.parent = transform;
            Destroy(warning, 0.9f);
        }
        if(eletricTime > secondsToTurnOn && eletricTime < secondsToTurnOn + 0.15f)
        {
            GameObject lightning = Instantiate(turnedOn,
                new Vector3(transform.position.x, transform.position.y, -7),
                Quaternion.identity) as GameObject;
            lightning.transform.parent = transform;
            //oi guedes
            Destroy(lightning, secondsTurnedOn - 0.15f);
        }
        if (eletricTime < secondsToTurnOn)
        {
            rend.material.color = new Color(255, 255, 255);
        }

        else if (eletricTime >= secondsToTurnOn + secondsTurnedOn)
        {
            eletricTime = 0;
        }

        else if (eletricTime >= secondsToTurnOn)
        {
            rend.material.color = new Color(0, 0f, 255f);
        }
    }

    public int GetEletricDamage()
    {
        return instantDamage;
    }
}
