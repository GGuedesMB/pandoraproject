﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashPlatform : MonoBehaviour
{
    [SerializeField] float inicialDelay;
    [SerializeField] int horizontalDelta;
    [SerializeField] int verticalDelta;
    [SerializeField] float smashSpeed;
    [SerializeField] float goingBackSpeed;
    [SerializeField] float secondsSmashing;
    [SerializeField] float secondsToSmash;


    Rigidbody2D myRigidBody;
    float time;
    int counter;
    Vector2 whereToSmash;
    Vector2 inicialPos;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        whereToSmash = new Vector2(transform.position.x + horizontalDelta, transform.position.y + verticalDelta);
        inicialPos = transform.position;
        counter = 2;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > inicialDelay) { Move(); }
    }

    private void Move()
    {
        if (counter % 2 == 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, whereToSmash, smashSpeed*Time.deltaTime);
            if (new Vector2(transform.position.x, transform.position.y) == whereToSmash && !IsInvoking("Counter"))
            { Invoke("Counter", secondsSmashing); }
        }
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, inicialPos, goingBackSpeed*Time.deltaTime);
            if (new Vector2(transform.position.x, transform.position.y) == inicialPos && !IsInvoking("Counter"))
            { Invoke("Counter", secondsToSmash); }
        }
    }

    private void Counter()
    {
        counter++;
    }
}
