﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformProjectile : MonoBehaviour
{

    [SerializeField] GameObject particlesPrefab;

    Rigidbody2D myRigidBody;

    private void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player Bullet") || collision.gameObject.CompareTag("Player Bullet 3") ||
            collision.gameObject.GetComponent<HazardShooterPlatform>() != null) { return; }

        GameObject bulletParticles = Instantiate(particlesPrefab, new Vector3(transform.position.x,
            transform.position.y, -1), Quaternion.identity);
        //Debug.Log("Velocidade bala: " + transform.localScale.x *
        //myRigidBody.velocity.x / Mathf.Abs(myRigidBody.velocity.x));
        float rotY;
        if (myRigidBody.velocity.x > 0) { rotY = 0; }
        else { rotY = 180; }

        bulletParticles.transform.rotation = Quaternion.Euler(0, rotY, 0);
        Destroy(bulletParticles, 2);

        Destroy(gameObject);
    }
}
