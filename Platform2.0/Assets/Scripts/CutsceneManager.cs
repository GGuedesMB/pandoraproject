﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    [SerializeField] bool f2_1;
    public bool f2;
    [SerializeField] CutsceneArea area1;
    [SerializeField] CutsceneArea area2;
    [SerializeField] CutsceneArea area3;
    [SerializeField] CutsceneArea area2_1;
    [SerializeField] DialogueTrigger dialogue0;
    [SerializeField] DialogueTrigger dialogue1;
    [SerializeField] DialogueTrigger dialogue2;
    [SerializeField] Animator bottomStripe;
    [SerializeField] Animator topStripe;
    [SerializeField] Animator screenMask;
    [SerializeField] Animator startMask;
    [SerializeField] Animator hudIcon;
    [SerializeField] RicBoss ricBoss;


    Player player;
    Boss boss;

    public static bool OnCutscene;

    float timer;

    bool canReturn;
    bool clothesChanged;
    bool mask;
    bool masked;

    public bool start;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        Debug.Log("player: ", player);
        masked = true;
        if (start) { startMask.SetTrigger("Start"); }
        if (f2) { Invoke("InvokeOnCutscene", 0.1f); }
        //start = false;
        boss = FindObjectOfType<Boss>();
    }

    void InvokeOnCutscene() { OnCutscene = true; }

    // Update is called once per frame
    void Update()
    {
        topStripe.SetBool("getTopStripe", OnCutscene);
        bottomStripe.SetBool("getBottomStripe", OnCutscene);
        screenMask.SetBool("GetMask", mask);
        //hudIcon.SetBool("OnCutscene", OnCutscene);

        Debug.Log("cutscene"+ OnCutscene);


        
        //Debug.Log("OnCutscene Cutscene: " + OnCutscene);
        //screenMask.SetBool("Masked", masked);
        if (start && f2) { return; }
        if (!start && !f2)
        {
            Debug.Log("EntrouPELOAMOR");
            OnCutscene = true;
            OnCutscene0();
        }

        if (FindObjectOfType<Boss>() != null)
        {
            if (boss.endingScene) { return; }
        }

        if (area1 != null && !f2)
        {
            if (area1.activated)
            {
                OnCutscene1();
            }
        }
        else { OnCutscene = false; }

        if (area2 != null)
        {
            if (area2.activated)
            {
                Debug.Log("Area2Activated!" + area2);
                OnCutscene2();
            }
        }
        else { OnCutscene = false; }

        if (area3 != null)
        {
            if (area3.activated)
            {
                OnCutscene3();
            }
        }
        else { OnCutscene = false; }

        if (f2_1)
        {
            if (area2_1 != null)
            {
                if (area2_1.activated)
                {
                    OnCutscene2_1();
                }
            }
            else { OnCutscene = false; }
        }


    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

    void OnCutscene0()
    {
        Debug.Log("Entrou aqui tbm");
        timer += Time.deltaTime;
        if (timer > 3 && !start && !DialogueManager.OnDialogue && timer < 4)
        {
            dialogue0.Dialogue0Activation();
            //Invoke("bugFixer", 1);
        }
        else if (timer > 4 && !DialogueManager.OnDialogue && !IsInvoking("bugFixer") && timer < 1000)
        {
            startMask.SetTrigger("Start");
            timer = 1000;
            Invoke("ActivatePart2",3);
            Invoke("bugFixer", 4);
        }
        else if (timer > 1004 && !start && !DialogueManager.OnDialogue)
        {
            start = true;
            OnCutscene = false;
            timer = 0;
        }  
    }

    private void ActivatePart2()
    {
        dialogue0.Dialogue0Part2Activation();
    }

    void OnCutscene1()
    {
        Debug.Log("Entrou");
        timer += Time.deltaTime;
        //Debug.Log("On cutscene");
        OnCutscene = true;
        player.Cutscene1Behaviour();
        //Debug.Log("timer: " + timer);
        //Debug.Log("Ondialogue: " + DialogueManager.OnDialogue);
        /*if(timer > 4 && !DialogueManager.OnDialogue && clothesChanged)
        {
            //DestroyCutsceneArea(area1.gameObject,1);
            Debug.Log("CanDestroy");
            //dialogue1.gameObject.SetActive(false);
            EndCutscene();
        }*/
        if (timer > 5 && player.cutsceneTimer < 1000 && !DialogueManager.OnDialogue && !IsInvoking("GetGun"))
        {
            //Debug.Log("PORQUE TA ENTRANDO AQUI");
            player.cutsceneTimer = 1000;
            Invoke("GetGun", 1.5f);
        }
    }

    private void GetGun()
    {
        mask = true;
        Invoke("PlayerChange", 2);
        Invoke("EndCutscene", 1.9f);
    }

    private void PlayerChange()
    {
        //if (f2) { return; }
        ChangePlayer cplayer = FindObjectOfType<ChangePlayer>();
        cplayer.PlayerChange();
    }

    private void EndCutscene()
    {
        Destroy(area1.gameObject);
        timer = 0;
        mask = false;
    }

    void OnCutscene2()
    {
        timer += Time.deltaTime;
        Debug.Log("On cutscene");
        OnCutscene = true;
        player.Cutscene2Behaviour();
        Debug.Log("timer: " + timer);
        Debug.Log("Ondialogue: " + DialogueManager.OnDialogue);
        if (timer > 10 && !DialogueManager.OnDialogue && player.cutsceneTimer > 1006)
        {
            //DestroyCutsceneArea(area1.gameObject,1);
            Debug.Log("CanDestroy");
            //dialogue1.gameObject.SetActive(false);
            Destroy(area2.gameObject);
            Debug.Log("DESTROYED");
            timer = 0;
        }
    }

    void OnCutscene3()
    {
        timer += Time.deltaTime;
        Debug.Log("On cutscene");
        OnCutscene = true;
        player.Cutscene3Behaviour();
    }

    void OnCutscene2_1()
    {
        timer += Time.deltaTime;
        //Debug.Log("OnCutscene");
        OnCutscene = true;
        player.Cutscene2_1Behaviour();
        if (!DialogueManager.OnDialogue && player.cutsceneTimer > 1003)
        {
            ricBoss.activated = true;
            //DestroyCutsceneArea(area1.gameObject,1);
            Debug.Log("CanDestroy");
            //dialogue1.gameObject.SetActive(false);
            Destroy(area2_1.gameObject);
            Debug.Log("DESTROYED");
            timer = 0;
        }
    }


    IEnumerator DestroyCutsceneArea(GameObject area, float secondsToDestroy)
    {
        Debug.Log("WILL DESTROY");
        yield return new WaitForSeconds(secondsToDestroy);
        Destroy(area.gameObject);
        Debug.Log("Destroyed");
    }


}
