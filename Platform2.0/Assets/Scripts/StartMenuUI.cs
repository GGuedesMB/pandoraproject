﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class StartMenuUI : MonoBehaviour
{

    [SerializeField] GameObject nameInputPanel;
    [SerializeField] TMP_InputField nameField;
    [SerializeField] GameObject logoPanel;

    int currentScene;

    // Start is called before the first frame update
    void Start()
    {
        nameInputPanel.SetActive(false);
        currentScene = SceneManager.GetActiveScene().buildIndex;
        FindObjectOfType<GameGlobalInfo>().Restart();
        Invoke("Logo", 11.25f);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Return)) { GameStart(); }
    }

    private void Logo() { logoPanel.SetActive(false); }

    public void ChoseYourName()
    {
        nameInputPanel.SetActive(true);
    }

    public void GameStart()
    {
        if (nameField.text == "" || nameField.text == " " || nameField.text == null)
        { GameGlobalInfo.playerName = "Tripulante"; } else { GameGlobalInfo.playerName = nameField.text; }
        SceneManager.LoadScene(currentScene + 1);
    }
}
