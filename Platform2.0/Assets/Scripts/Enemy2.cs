﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    [SerializeField] float health;
    [SerializeField] int bulletsPerBurst;
    [SerializeField] float bulletSpeed;
    [SerializeField] float secondsBetweenBursts;
    [SerializeField] float secondsBetweenShots;
    [SerializeField] GameObject enemy2Bullet;
    [SerializeField] GameObject hitFB;

    Animator myAnimator;
    Som som;

    int burstCounter;
    float feedbackTimer;
    Renderer rend;
    Vector4 startColor;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        myAnimator = GetComponent<Animator>();
        myAnimator.SetBool("willShoot", true);
        som = FindObjectOfType<Som>();
    }

    // Update is called once per frame
    void Update()
    {
        feedbackTimer += Time.deltaTime;
        HitFeedBack();
        Burst();
    }

    private void Burst()
    {
        if(!IsInvoking("Fire") && burstCounter < bulletsPerBurst) { Invoke("Fire", secondsBetweenShots);
            Invoke("ShootAnimation", secondsBetweenShots - 0.1f); }
        CoolDown();
    }

    private void ShootAnimation() { myAnimator.SetTrigger("shoot"); }
    private void AntecipationAnimation() { myAnimator.SetBool("willShoot", true); som.Ene2_Antecipacao(transform.position); }


    private void Fire()
    {
        GameObject bullet = Instantiate(enemy2Bullet,
            new Vector3(transform.position.x + (1* transform.localScale.x), transform.position.y + 0.35f, -1),
            Quaternion.identity) as GameObject;
        //bullet.transform.localScale = new Vector2(0.5f,0.25f);
        bullet.GetComponent<Rigidbody2D>().AddForce(new Vector2(bulletSpeed * transform.localScale.x, 0));
        Destroy(bullet, 5);
        burstCounter++;
        som.Ene2_Tiro(transform.position);
    }

    private void CoolDown()
    {
        if(burstCounter >= bulletsPerBurst && !IsInvoking("BurstCounterReset"))
        {
            myAnimator.SetBool("willShoot", false);
            Invoke("AntecipationAnimation", secondsBetweenBursts - 0.5f);
            Invoke("BurstCounterReset", secondsBetweenBursts);
        }
    }

    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }

    private void BurstCounterReset()
    {
        burstCounter = 0;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Bullet>() != null)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            feedbackTimer = 0;
            som.Ene_Recebe_Dano(transform.position);
            if (health <= 0)
            {
                som.Ene_Morre(transform.position);
                Destroy(gameObject);
            }
        }
    }
}
