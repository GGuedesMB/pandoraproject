﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatforms : MonoBehaviour
{
    [SerializeField] bool solo;
    [SerializeField] bool isAGate;
    [SerializeField] int maxActivations;
    [SerializeField] int tilesRight;
    [SerializeField] int tilesLeft;
    [SerializeField] int tilesUp;
    [SerializeField] int tilesDown;
    [SerializeField] bool startLeft;
    [SerializeField] bool startRight;
    [SerializeField] bool startDown;
    [SerializeField] bool startUp;
    [SerializeField] float speed;
    [SerializeField] float horizontalSpeed;
    [SerializeField] float verticalSpeed;
    [Header("1=esquerda/baixo; 2=direita/cima")]
    [Range(1,2)][SerializeField] int startDirection;

    Rigidbody2D myRigidBody;
    PolygonCollider2D myPolygon;
    BoxCollider2D myBox;
    Som som;

    int counter;
    int numberOfActivations;
    bool activated;
    Vector2 maxPos;
    Vector2 minPos;
    float bugPreventTimer;
    public static bool childRepelent;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myPolygon = GetComponent<PolygonCollider2D>();
        myBox = GetComponent<BoxCollider2D>();
        maxPos = new Vector2(transform.position.x + tilesRight, transform.position.y + tilesUp);
        minPos = new Vector2(transform.position.x - tilesLeft, transform.position.y - tilesDown);
        counter = startDirection;
        if (startLeft || startDown) { speed *= -1; }
        else if (startRight || startUp) { speed *= -1; }
        som = FindObjectOfType<Som>();
        //Invoke("BugPrevent", 1f);
    }

    private void FixedUpdate()
    {
        Move2();
        //Disable();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeSpeed();


        //Disable();
        if (solo)
        {
            Debug.Log("TouchingLayers: " + myPolygon.IsTouchingLayers(LayerMask.GetMask("Player")));
            Debug.Log("Enabled: " + myBox.enabled);
            Debug.Log("childRepelent" + childRepelent);

        }
        //if (numberOfActivations < maxActivations || !isAGate) { Move(); }
    }


    public int GetCounter()
    {
        return counter; 
    }

    public Vector2 GetSpeedVector()
    {
        return new Vector2(myRigidBody.velocity.x, myRigidBody.velocity.y);
    }

    private void Move2()
    {
        
        myRigidBody.velocity = new Vector2(0, 0);

        if (startLeft || startRight)
        {
            myRigidBody.velocity = new Vector2(speed, 0);
        }
        else if (startDown || startUp)
        {
            myRigidBody.velocity = new Vector2(0, speed);
        }
    }

    private void ChangeSpeed()
    {
        if (startLeft || startRight)
        {
            if ((transform.position.x > maxPos.x || transform.position.x < minPos.x) && !IsInvoking("BugPrevent"))
            {
                speed *= -1;
                numberOfActivations++;
                activated = false;
                Invoke("BugPrevent", 0.5f);
            }
        }
        else if (startDown || startUp)
        {
            if ((transform.position.y > maxPos.y || transform.position.y < minPos.y) && !IsInvoking("BugPrevent"))
            {
                speed *= -1;
                numberOfActivations++;
                activated = false;
                Invoke("BugPrevent", 0.5f);
            }
        }

    }

    private void BugPrevent()
    {

    }

    private void Move()
    {
        var speedAdjust = speed * Time.deltaTime;
        //float speedAdjust = speed;
        if (!isAGate)
        {
            if (counter % 2 == 0)
            {
                transform.position = Vector2.MoveTowards(transform.position, maxPos, speedAdjust);
                if (new Vector2(transform.position.x, transform.position.y) == maxPos) { counter++; som.Plat_Movel(transform.position); }
            }
            else
            {
                transform.position = Vector2.MoveTowards(transform.position, minPos, speedAdjust);
                if (new Vector2(transform.position.x, transform.position.y) == minPos) { counter++; som.Plat_Movel(transform.position); }
            }
        }
        else if (isAGate && activated)
        {
            if (counter % 2 == 0)
            {
                transform.position = Vector2.MoveTowards(transform.position, maxPos, speedAdjust);
                if (new Vector2(transform.position.x, transform.position.y) == maxPos)
                {
                    counter++;
                    activated = false;
                    numberOfActivations++;
                }
            }
            else 
            {
                transform.position = Vector2.MoveTowards(transform.position, minPos, speedAdjust);
                if (new Vector2(transform.position.x, transform.position.y) == minPos)
                {
                    counter++;
                    activated = false;
                    numberOfActivations++;
                }
            }
        }
    }

    private void Disable()
    {
        if (myPolygon.IsTouchingLayers(LayerMask.GetMask("Player")))
        {
            Debug.Log("PolygonTouching");
            myBox.enabled = false;
            childRepelent = true;
            Debug.Log("Please: " + childRepelent);
        }
        else
        {
            if (solo) { Debug.Log("Why is that shit hapenning?"); }
            myBox.enabled = true;
            childRepelent = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            activated = true;
        }
    }

}
