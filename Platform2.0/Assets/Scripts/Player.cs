﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //FASE 1 - Habilidades:
    //Correr, pular, atirar, pulo duplo; 


    //Confog parameters
    [Header("Fase:Section")]
    [SerializeField] bool f2;
    [SerializeField] bool isPlayer0;
    [SerializeField] int fase;
    [SerializeField] int section;
    [SerializeField] Vector2 revivePosition;
    [SerializeField] int reviveDirection;
    [SerializeField] int startingCamera;
    [SerializeField] Transform[] cheatPoints;

    [Header ("PLAYER PARAMETERS")]
    //[SerializeField] int tecnology;
    [SerializeField] int health;
    [SerializeField] int health2;
    [SerializeField] float runSpeed;
    [SerializeField] float halfjumpForce;
    [SerializeField] float jumpForce;
    [SerializeField] float climbSpeed;
    [SerializeField] Vector2 deathPushForce;
    [SerializeField] float invulnerabilityDuration;
    [SerializeField] float flipSpeed;
    [SerializeField] float dashSpeed;
    [SerializeField] float dashLength;
    [SerializeField] float afterPlatDecrease;

    [Header("Weapon")]
    [SerializeField] bool hasTheGun;
    [SerializeField] float gunMax;
    [SerializeField] float shootHeat;
    [SerializeField] Transform bulletExit;
    [SerializeField] GameObject shortCircuit;
    [SerializeField] GameObject shortCircuitVF;

    [Header ("REFERENCES")]
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject bullet2;
    [SerializeField] GameObject bullet3;
    [SerializeField] GameObject bullet4;
    [SerializeField] GameObject bullet5;
    [SerializeField] GameObject djEffect;
    [SerializeField] GameObject deathEffect;
    [SerializeField] CameraShake[] cameraShake;
    [SerializeField] Vector2 shakeParameters;
    [SerializeField] AnimationClip spaceSuit;
    [SerializeField] Animator ricAnimator;
    [SerializeField] DialogueTrigger dialogue2;
    [SerializeField] DialogueTrigger dialogue3;
    [SerializeField] DialogueTrigger dialogue2_1;
    [SerializeField] newGate[] gates;
    [SerializeField] FallColliders[] fallColliders;
    [SerializeField] Transform[] fallRevivePoints;
    [SerializeField] GameObject taylor;
    [SerializeField] FallingPlatform f2Plat;
    [SerializeField] Transform taylorBulletPos;
    [SerializeField] Enemy3 taylorEnemy;
    [SerializeField] Animator anim;
    [SerializeField] GameObject elevator;
    [SerializeField] GameObject realElevator;
    [SerializeField] Transform elevatorTarget;
    [SerializeField] Generator[] generator;
    [SerializeField] GameObject taylorCutscene;
    [SerializeField] GameObject firstRic;
    [SerializeField] GameObject elevatorForeground;
    [SerializeField] GameObject lastCutscene;

    //Player Cached components
    Rigidbody2D myRigidBody;
    Animator myAnimator;
    Animation myAnimation;
    AnimationClip animationClip;
    Collider2D myCollider2D;
    BoxCollider2D myFeet;
    PolygonCollider2D myBody;
    Renderer rend;
    //Renderer circuitRend;

    CameraShake cameraShake2;

    PasstroughPlatform passtroughPlat;

    VisualFeedback damageFeedback;

    bool hasTheRobotShoes;

    bool isClimbing;
    bool isShooting;
    bool isDead;
    bool willJump;
    bool canDoubleJump;
    bool isDashing;
    bool isGunHeated;
    bool stickToPlat;
    bool isInvulnerable;
    float gunHeat;
    float timeForFlip;
    float timeForShoot;
    float timeForJump;
    float timeForRevive;
    float invulnerableTimer;
    float invulnerableFBTimer;
    int counter;
    float mPHSpeed;
    float mPVSpeed;
    Vector2 teleportDestinyPos;
    GameObject movingPlat;
    Vector2 startingScale;
    CameraController cameraController;
    public float shakeTimer;

    Vector4 startColor;
    //Vector4 circuitStartColor;

    float xSpeedAdjust;
    float lastDirection;

    Vector2 lastCheckpoint;
    int lastCheckpointCameraIndex;

    bool goingUp;
    bool goingDown;

    Coroutine fireCoroutine;

    GameObject bullets;

    bool isFirePressing;
    bool stopFirePressing;
    float fireAuxiliarTimer;

    bool keepFiring;

    

    float afterPlatSpeed;
    float jumpOffMovingPlatAuxiliarTimer;
    float jumpOffMovingPlatAuxiliarTimer2;

    public float cutsceneTimer;
    float runDeltaX;

    bool jumpButtonDown;

    float shortCircuitTimer;

    float timeForResting;

    public bool oneBullet;

    float deadAnimationStartTime;

    int revivePoint;
    bool hasFelt;

    bool dash;
    bool canDash;
    float dashTimer;

    float f2Timer;
    bool hasDashBoots;

    float noShieldsTimer;

    Rigidbody2D taylorRb;
    Animator taylorAnim;

    Som som;
    AnimatorClipInfo[] animInfo;

    bool fallingSoundAuxiliar;

    float gunCharge;

    bool jumpTest;
    bool jump2Test;

    GameGlobalInfo globalInfo;

    Animator elevatorAnim;
    Vector3 elevatorStartPos;

    int cheatIndex;

    // Start is called before the first frame update
    void Start()
    {
        som = FindObjectOfType<Som>();
        fireAuxiliarTimer = 1;
        hasDashBoots = false;
        shortCircuitTimer = 2;
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myCollider2D = GetComponent<Collider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        myBody = GetComponent<PolygonCollider2D>();
        animationClip = GetComponent<AnimationClip>();
        rend = GetComponent<Renderer>();
        //circuitRend = shortCircuit.GetComponent<Renderer>();
        passtroughPlat = FindObjectOfType<PasstroughPlatform>();
        damageFeedback = FindObjectOfType<VisualFeedback>();
        startColor = rend.material.color;
        isClimbing = false;
        timeForFlip = 1.5f;
        timeForRevive = 0f;
        gunHeat = gunMax;
        startingScale = transform.localScale;
        cameraController = FindObjectOfType<CameraController>();
        cameraController.cameraIndex = startingCamera;
        shakeTimer = 3;
        if (f2) { hasTheRobotShoes = true; health2 = GameGlobalInfo.playerHealth; }
        canDash = true;
        animInfo = myAnimator.GetCurrentAnimatorClipInfo(0);
        globalInfo = FindObjectOfType<GameGlobalInfo>();
        //if (!isPlayer0)
        //{
            taylorRb = taylor.GetComponent<Rigidbody2D>();
            taylorAnim = taylor.GetComponent<Animator>();
            if (f2) { elevatorAnim = elevator.GetComponent<Animator>(); }
            elevatorStartPos = realElevator.transform.position;
            elevatorForeground.SetActive(false);
        //}
        
        
        //cameraShake = new CameraShake[cameraShake.Length];

    }

    private void FixedUpdate()
    {
        //Run();
        //JumpNew();
        //MovingPlatAdjust();
        //Debug.Log("Istouching?" + myFeet.IsTouchingLayers());

        /*if (jumpTest && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
    myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
    myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))))
        {
            jumpOffMovingPlatAuxiliarTimer = 0;
            myAnimator.SetTrigger("Jumping");
            Vector2 additionForce1 = new Vector2(0f, jumpForce);
            myRigidBody.AddForce(additionForce1);
            som.Pj_Pulo2(transform.position);
            jumpTest = false;
        }
        if (jump2Test &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
        {
            myAnimator.SetTrigger("DoubleJump");
            jumpButtonDown = false;
            myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpForce / 55);
            canDoubleJump = false;
            GameObject foguinho = Instantiate(djEffect, new Vector3(transform.position.x,
               transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
            foguinho.GetComponent<Transform>().transform.position = transform.position;
            Destroy(foguinho, 2f);
            jump2Test = false;
        }*/

        if (stickToPlat)
        {
            Vector2 originalScale = transform.localScale;
            transform.parent = movingPlat.transform;
            //Vector2 scaleAdjust = new Vector2(2/movingPlat.transform.localScale.x, 2/movingPlat.transform.localScale.y);
            //transform.localScale = scaleAdjust;

            float deltaX = Input.GetAxis("Horizontal");
            bool playerHasHorizontalSpeed = Mathf.Abs(deltaX) > Mathf.Epsilon;
            float direction = Mathf.Sign(deltaX);
            if (Mathf.Abs(deltaX) > Mathf.Epsilon) { lastDirection = direction; }
            if (playerHasHorizontalSpeed)
            {
                Vector2 newScale = new Vector2(direction * 1 / movingPlat.transform.localScale.x,
                1 / movingPlat.transform.localScale.y);
                transform.localScale = newScale;
            }
            else
            {
                Vector2 newScale = new Vector2(lastDirection * 1 / movingPlat.transform.localScale.x,
                1 / movingPlat.transform.localScale.y);
                transform.localScale = newScale;
            }
            if (jumpOffMovingPlatAuxiliarTimer > 0.3f && jumpOffMovingPlatAuxiliarTimer2 > 0.5f)
            { myRigidBody.AddForce(new Vector2(0f, -500)); }
            //Debug.Log("Last Direct: " + lastDirection);
            
            //myRigidBody.velocity = new Vector2(myRigidBody.velocity.x + xSpeedAdjust, myRigidBody.velocity.y);
        }
        else
        {
            transform.parent = null;
        }

        

        FlipPlayer();
        if (!isDead && !dash) { Run(); }
        //if (hasTheGun) { Fire(); }
        AnimationOnJumping();
        if (f2 && hasDashBoots) { Dash(); }
        
        //JumpNew();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (!isPlayer0)
        {
            if (Input.GetKeyDown(KeyCode.L)) { taylorCutscene.SetActive(true); }
            if (Input.GetKeyDown(KeyCode.P))
            {
                transform.position = cheatPoints[cheatIndex].position; cheatIndex++;
                transform.position = new Vector3(transform.position.x, transform.position.y, -1);
            }
            if (Input.GetKeyDown(KeyCode.O)) { cheatIndex = 0; }
            if (Input.GetKeyDown(KeyCode.Q)) { if (health2 < GameGlobalInfo.maxShields) { anim.SetTrigger("GetShield"); health2++; } }
            if (Input.GetKeyDown(KeyCode.T)) { GameGlobalInfo.tecnology += 100; }
        }*/

        if (health2 == 0 && !isPlayer0 && !DialogueManager.OnDialogue && !CutsceneManager.OnCutscene)
        {
            noShieldsTimer += Time.deltaTime;
            if (noShieldsTimer > 1.5f)
            {
                som.Pj_Batimento();
                cameraShake2 = FindObjectOfType<CameraShake>();
                StartCoroutine(cameraShake2.Shake(shakeParameters.x, 0.1f));
                noShieldsTimer = 0;
            }
        } else { noShieldsTimer = 0; }
        
        anim.SetInteger("life", health2);
        f2Timer += Time.deltaTime;
        if (f2Timer < 1 && f2) { myRigidBody.velocity = new Vector2(0,0); }
        //Debug.Log("Is touching fase 17"+myFeet.IsTouchingLayers());
        //Debug.Log("Is it ground? " + myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")));
        timeForResting += Time.deltaTime;
        shortCircuitTimer += Time.deltaTime;
        if (shortCircuitTimer >= 0.5f && !isPlayer0) { shortCircuit.SetActive(false); }
        if (!CutsceneManager.OnCutscene) { cutsceneTimer = 0; }
        //if (!ChangePlayer.spaceSuit) { rend.material.color = new Vector4(219, 219, 219, 0); }
        PasstroughPlatform.myFeetTimer += Time.deltaTime;

        //cameraShake = new CameraShake[cameraShake.Length];

        jumpOffMovingPlatAuxiliarTimer += Time.deltaTime;
        jumpOffMovingPlatAuxiliarTimer2 += Time.deltaTime;
        shakeTimer += Time.deltaTime;

        //if (cameraController.cameraIndex >= 3) { hasTheGun = true; }
        if(cameraController.cameraIndex >= 13) { hasTheRobotShoes = true; }
        if (cameraController.cameraIndex >= 6 && f2) { hasDashBoots = true; }

        JumpAuxiliar();
        FireAuxiliar();
        F2FallRevive();
        Die();
        if (isDead) { return; }
        if (!dash && !CutsceneManager.OnCutscene && !DialogueManager.OnDialogue)
        {
            JumpNew();
        }
        if (f2 && hasDashBoots) { DashCommand(); }
        Teleport();
        Invulnerable(); 
        if (hasTheGun && !DialogueManager.OnDialogue && !CutsceneManager.OnCutscene) { FireTry(); }

        if(timeForResting > 3) { myAnimator.SetBool("Resting", true); }
        else { myAnimator.SetBool("Resting", false); }
        if (f2) { myAnimator.SetBool("Dash", dash); }
        
        //if (!f2)
        //{
        if (f2)
        {
            if (generator[0].activated && generator[1].activated)
            { /*Debug.Log("hbhjvhgéisso");*/  elevatorAnim.SetBool("activated", true);
                /*taylorCutscene.SetActive(true);*/ taylor.SetActive(true); firstRic.SetActive(false); lastCutscene.SetActive(true);
            }
            else { /*taylorCutscene.SetActive(false);*/ taylor.SetActive(false); }
        }

            animInfo = myAnimator.GetCurrentAnimatorClipInfo(0);
            //Debug.Log("Current anim: " + animInfo[0].clip.name);
            if ((isPlayer0 && animInfo[0].clip.name == "NoGunRun") || (!isPlayer0 && animInfo[0].clip.name == "GunRun"))
            { som.Pj_Passo(transform.position); }
            //if (timeForResting == 3) { som.Pj_Idle(); }
            if (fallingSoundAuxiliar && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
                myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform"))))
        { som.Pj_Queda(transform.position); /*Debug.Log("Happening");*/ fallingSoundAuxiliar = false; }
            if (!isPlayer0 && cameraController.cameraIndex >= 16) { GetComponent<AudioSource>().Pause(); }
        //}

        //health2 = 5;
        //if (!isPlayer0) { GetComponent<AudioSource>().Play(); }
        //
        
    }

    //Cutscenes behaviour
    

    public void Cutscene1Behaviour()
    {
        cutsceneTimer += Time.deltaTime;
        //Debug.Log("PLayerTimer: " + cutsceneTimer);
        if (cutsceneTimer < 3) { runDeltaX = 1; }
        else if (cutsceneTimer > 5 && cutsceneTimer < 1001 && !DialogueManager.OnDialogue) { runDeltaX = 1; }
        else { runDeltaX = 0; } 
    }

    public void Cutscene2Behaviour()
    {
        cutsceneTimer += Time.deltaTime;
        if (cutsceneTimer < 3) { runDeltaX = 0.5f; }
        else { runDeltaX = 0; }
        if (cutsceneTimer > 3.5 && !DialogueManager.OnDialogue && cutsceneTimer < 4)
        {
            Dialogue2Trigger();
        }
        if (!DialogueManager.OnDialogue && cutsceneTimer > 4 && cutsceneTimer < 1000) { cutsceneTimer = 1000; }
        if (cutsceneTimer > 1000 && !IsInvoking("Dialogue2Part2Trigger") && !DialogueManager.OnDialogue)
        {
            timeForResting = 0;
            Invoke("CutsceneShooting",1);
            Invoke("RicCSAnimation", 2);
            Invoke("Dialogue2Part2Trigger", 6);
        }

    }

    public void Cutscene3Behaviour()
    {
        cutsceneTimer += Time.deltaTime;
        if (cutsceneTimer > 12 && !IsInvoking("CutsceneShooting") && cutsceneTimer < 13)
        { timeForResting = 0; Invoke("CutsceneShooting", 2); }
        if (cutsceneTimer > 12 && taylorEnemy != null) { taylorEnemy.Activation(); }
        if (!IsInvoking("TaylorShooting") && cutsceneTimer > 10 && cutsceneTimer < 14) { Invoke("TaylorShooting",0.5f);
            GameObject taylorBullet = Instantiate(bullet,taylorBulletPos.position,Quaternion.identity);
            taylorBullet.transform.rotation = Quaternion.Euler(0, 0, 90);
            taylorBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(20,0);
        }
        if (cutsceneTimer < 5 + 5) { runDeltaX = 1f; taylorAnim.SetBool("Shooting", true); }
        else { runDeltaX = 0; }
        if (cutsceneTimer > 15) { taylorAnim.SetBool("Shooting", false); }
        if (cutsceneTimer > 16) { taylor.GetComponent<Transform>().localScale = new Vector2(-1,1); }
        if (cutsceneTimer > 11 + 7 && !DialogueManager.OnDialogue && cutsceneTimer < 11.2f + 7)
        {
            dialogue3.Dialogue3Activation();
        }
        if (!DialogueManager.OnDialogue && cutsceneTimer > 11.2f + 7 && cutsceneTimer < 1000) { cutsceneTimer = 1000; }
        if (cutsceneTimer > 1000 && cutsceneTimer < 1001)
        {
            taylorAnim.SetBool("Running", true);
            taylorRb.velocity = new Vector2(-4,0);
        }
        else { taylorRb.velocity = new Vector2(0,0); taylorAnim.SetBool("Running", false); }
        if (cutsceneTimer > 1003 && !DialogueManager.OnDialogue && cutsceneTimer < 1003.5f)
        {
            dialogue3.Dialogue3Part2Activation();
        }
        if (cutsceneTimer > 1003.5f && !DialogueManager.OnDialogue && cutsceneTimer < 2000) { cameraShake2 = FindObjectOfType<CameraShake>();
            StartCoroutine(cameraShake2.Shake(1, shakeParameters.y)); cutsceneTimer = 2000; }
        if (cutsceneTimer > 2001) { GameGlobalInfo.playerHealth = health2; f2Plat.FallCommand(); StartCoroutine(FindObjectOfType<LevelExit>().ExitLevel()); }
    }

    public void Cutscene2_1Behaviour()
    {
        cutsceneTimer += Time.deltaTime;
        if (cutsceneTimer < 2.6f) { runDeltaX = -1; }
        else { runDeltaX = 0; }
        if (cutsceneTimer > 2 && cutsceneTimer < 4) { taylorRb.velocity = new Vector2(4, taylorRb.velocity.y); taylorAnim.SetBool("Running", true); }
        else { taylorRb.velocity = new Vector2(0, taylorRb.velocity.y); taylorAnim.SetBool("Running", false); }
        if (cutsceneTimer > 5 && cutsceneTimer < 5.16f && !DialogueManager.OnDialogue) { dialogue2_1.Dialogue0Activation(); }
        if (cutsceneTimer > 5.2f && cutsceneTimer < 500 && !DialogueManager.OnDialogue)
        { realElevator.transform.position = Vector3.MoveTowards(realElevator.transform.position, elevatorTarget.position, 2*Time.deltaTime); }
        if (realElevator.transform.position == elevatorTarget.position && cutsceneTimer < 900) { Debug.Log("Arrived"); cutsceneTimer = 1000; }
        if (cutsceneTimer > 1000 && cutsceneTimer < 1001) { runDeltaX = 1; }
        if (cutsceneTimer > 1001)
        { realElevator.transform.position = Vector3.MoveTowards(realElevator.transform.position, elevatorStartPos, 4 * Time.deltaTime); }
        //if (cutsceneTimer > 1001) { }
        if (cutsceneTimer > 1002 && cutsceneTimer < 1003 && !DialogueManager.OnDialogue)
        { dialogue2_1.Dialogue2Part2Activation(); elevatorForeground.SetActive(true); }
    }

    private void TaylorShooting() { taylorAnim.SetTrigger("ShootTrigger"); som.Pj_Tiro(); Debug.Log("Should shoot"); }

    private void Dialogue2Trigger()
    {
        dialogue2.Dialogue2Activation();
    }

    private void Dialogue2Part2Trigger()
    {
        dialogue2.Dialogue2Part2Activation();
    }

    private void Dialogue3Trigger()
    {
        dialogue2.Dialogue3Activation();
    }

    private void RicCSAnimation()
    {
        ricAnimator.SetBool("isCalm", true);
    }

    private void CutsceneShooting()
    {
        som.Pj_Tiro();
        if (GameGlobalInfo.bulletChip == 2)
        {
            bullets = Instantiate(bullet2,
            new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
            Quaternion.identity) as GameObject;
        }
        else if (GameGlobalInfo.bulletChip == 3)
        {
            bullets = Instantiate(bullet3,
            new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
            Quaternion.identity) as GameObject;
        }
        else if (GameGlobalInfo.bulletChip == 4)
        {
            bullets = Instantiate(bullet4,
            new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
            Quaternion.identity) as GameObject;
        }
        else if (GameGlobalInfo.bulletChip == 5)
        {
            bullets = Instantiate(bullet5,
            new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
            Quaternion.identity) as GameObject;
        }
        else
        {
            bullets = Instantiate(bullet,
            new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
            Quaternion.identity) as GameObject;
        }


        if (transform.localScale.x > 0)
        {
            bullets.transform.rotation = Quaternion.Euler(0, 0, 270);
            bullets.GetComponent<Rigidbody2D>().
                AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed(), 0f));
        }
        //oneBullet = true;
        timeForResting = 0;
    }

    //Methods 
    private void F2FallRevive()
    {
        for (int i = 0; i < fallColliders.Length; i++)
        {
            if (fallColliders[i].playerFelt) { hasFelt = true; revivePoint = i; }
        }
        if (hasFelt)
        {
            health2--;
            cameraShake2 = FindObjectOfType<CameraShake>();
            StartCoroutine(cameraShake2.Shake(shakeParameters.x, shakeParameters.y));
            damageFeedback.DamageFeedback();
            timeForResting = 0;
            if (health2 <= -1)
            {
                CameraShake.spawnAuxiliar = true;
                timeForRevive = 0;
                myRigidBody.velocity = new Vector2(0f, 0f);
            }
            else
            {
                Invoke("F2FallRespawn", 2);
            }
            fallColliders[revivePoint].playerFelt = false;
            hasFelt = false;
            FallingPlatform.specialRespawn = true;
        }
    }

    private void F2FallRespawn()
    {
        transform.position = new Vector3(fallRevivePoints[revivePoint].position.x, fallRevivePoints[revivePoint].position.y,-1);
        FallingPlatform.specialRespawn = false;
        fallColliders[revivePoint].playerFelt = false;
    }

    private void AnimationOnJumping()
    {
        //Debug.Log("Istouching?" + myFeet.IsTouchingLayers());
        //Debug.Log("goingUp: " + goingUp);
        if ((!myFeet.IsTouchingLayers() || myFeet.IsTouchingLayers(LayerMask.GetMask("HazardShooter")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("newGate")) || myFeet.IsTouchingLayers(LayerMask.GetMask("enemyTrigger"))) 
            && myRigidBody.velocity.y > Mathf.Epsilon) //|| 
            //(myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) && MovingPlatforms.childRepelent))
        {
            goingUp = true;
        }
        else if((((myFeet.IsTouchingLayers(LayerMask.GetMask("HazardShooter")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("newGate")) || myFeet.IsTouchingLayers(LayerMask.GetMask("enemyTrigger"))) &&
            myRigidBody.velocity.y < -0.1f) ||
            !myFeet.IsTouchingLayers()) && myRigidBody.velocity.y <= Mathf.Epsilon )//||
            //(myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) && MovingPlatforms.childRepelent))
        {
            goingUp = false;
            goingDown = true;
            fallingSoundAuxiliar = true;
        }
        else
        {
            goingDown = false;
            goingUp = false;
        }
        myAnimator.SetBool("GoingUp", goingUp);
        myAnimator.SetBool("GoingDown", goingDown);

    }

    private void Teleport()
    {
        if (myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) && 
            (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S)))
        {
            transform.position = teleportDestinyPos;
        }
    }

    private void Run()
    {
        //INPUT MANAGER - MUDANDO O VETOR VELOCIDADE
        //Debug.Log("afterSpeed: " + afterPlatSpeed);
        if (!CutsceneManager.OnCutscene && !DialogueManager.OnDialogue) { runDeltaX = Input.GetAxis("Horizontal"); }
        if (DialogueManager.OnDialogue) { runDeltaX = 0; }
        float velY = myRigidBody.velocity.y;
        if (velY > 14) { velY *= 0.96f; }
        Vector2 playerVelocity;
        /* if (Mathf.Abs(myRigidBody.velocity.x + afterPlatSpeed) > 5) { */afterPlatSpeed *= afterPlatDecrease;
        if (myFeet.IsTouchingLayers()) { afterPlatSpeed *= afterPlatDecrease / 2; }
        if(afterPlatSpeed > 0 && afterPlatSpeed < 0.5f) { afterPlatSpeed = 0; }
        if(afterPlatSpeed < 0 && afterPlatSpeed > -0.5f) { afterPlatSpeed = 0; }
        if (!stickToPlat) { playerVelocity = new Vector2(runDeltaX * runSpeed + afterPlatSpeed, velY); }
        else { playerVelocity = new Vector2(runDeltaX * runSpeed + xSpeedAdjust, myRigidBody.velocity.y); }
        
        myRigidBody.velocity = playerVelocity;
        bool isPlayerRunning = Mathf.Abs(runDeltaX) > Mathf.Epsilon;
        if (isPlayerRunning) { timeForResting = 0; }
        
        
        //myAnimator.speed *= Mathf.Abs(deltaX);

        myAnimator.SetBool("Running", isPlayerRunning);
    }

    private void DashCommand()
    {
        if (Input.GetButtonDown("Fire3") && canDash)
        {
            dash = true;
            if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) && !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
            {
                canDash = false;
            }
        }
    }

    private void Dash()
    {
        
        if (dash)
        {
            dashTimer += Time.deltaTime;
            if (dashTimer < dashLength)
            {
                myRigidBody.velocity = new Vector2(dashSpeed*lastDirection, 0);
            }
            else { dashTimer = 0; dash = false; }
        }
    }

    private void FlipPlayer()
    {
        //float runDeltaX = Input.GetAxis("Horizontal");
        bool playerHasHorizontalSpeed = Mathf.Abs(runDeltaX) > Mathf.Epsilon;

        if (playerHasHorizontalSpeed && !stickToPlat)
        {
            float direction = Mathf.Sign(runDeltaX);
            lastDirection = direction;
            Vector2 newScale = new Vector2 (direction * 1, transform.localScale.y);
            transform.localScale = newScale;
        }
        //else if(stickToPlat)
        //{
            //float direction = Mathf.Sign(deltaX);
            //Vector2 newScale = new Vector2(direction * 2/movingPlat.transform.localScale.x, 
                //2/movingPlat.transform.localScale.y);
            //transform.localScale = newScale;
        //}
    }

    private void Jump()
    {
        Vector3 fogPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        timeForJump += Time.deltaTime;
        if (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) || 
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))) { canDoubleJump = true; }
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) && 
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) &&
            !myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")) && !canDoubleJump) { return; }
        if (Input.GetButtonDown("Jump") && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))))
        {
            timeForJump = 0;
            willJump = true;
        }

        if (Input.GetButtonUp("Jump") && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))) && timeForJump < 0.07)
        {
            myAnimator.SetTrigger("Jumping");
            Vector2 additionForce1 = new Vector2(0f, halfjumpForce);
            myRigidBody.AddForce(additionForce1);
            willJump = false;
        }
        else if (willJump && timeForJump >= 0.07 && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))))
        {
            myAnimator.SetTrigger("Jumping");
            Vector2 additionForce2 = new Vector2(0f, jumpForce);
            myRigidBody.AddForce(additionForce2);
            willJump = false;
        }

        else if (Input.GetButtonDown("Jump") && 
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) && canDoubleJump && 
            (fase > 1 || section > 6))
        {
            myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpForce/55);
            canDoubleJump = false;
            GameObject foguinho = Instantiate(djEffect, new Vector3(transform.position.x,
               transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
            foguinho.GetComponent<Transform>().transform.position = transform.position;
            Destroy(foguinho, 2f);
        }
    }

    private void JumpAuxiliar()
    {
        if (Input.GetButtonDown("Jump"))
        {
            jumpButtonDown = true;
        }
        if (Input.GetButtonUp("Jump"))
        {
            jumpButtonDown = false;
        }
    }

    private void JumpNew()
    {
        //if ((myRigidBody.velocity.y > 0.1f || PasstroughPlatform.myFeetTimer < 0.25f)
            //&& !stickToPlat) { myFeet.enabled = false; }
        //else { myFeet.enabled = true; }
        Vector3 fogPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        timeForJump += Time.deltaTime;
        if (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))) { canDash = true; canDoubleJump = true; }
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) &&
            !myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")) && !canDoubleJump) { return; }
        if (Input.GetButtonDown("Jump") && (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) ||
            myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform"))))
        {
            jumpTest = true;
            //jumpButtonDown = false;

            jumpOffMovingPlatAuxiliarTimer = 0;
            myAnimator.SetTrigger("Jumping");
            Vector2 additionForce1 = new Vector2(0f, jumpForce);
            myRigidBody.AddForce(additionForce1);
            som.Pj_Pulo(transform.position);
        }

        else if (Input.GetButtonDown("Jump") &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Teleport Platform")) &&
            !myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")) && canDoubleJump && hasTheRobotShoes)
        {

            jump2Test = true;
            
            myAnimator.SetTrigger("DoubleJump");
            jumpButtonDown = false;
            myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, jumpForce / 55);
            canDoubleJump = false;
            som.Pj_Pulo2(transform.position);
            /*GameObject foguinho = Instantiate(djEffect, new Vector3(transform.position.x,
               transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
            foguinho.GetComponent<Transform>().transform.position = transform.position;
            Destroy(foguinho, 2f);*/
        }
    }

    private void Climb()
    {
        myAnimator.SetBool("Climbing", isClimbing);
        if (myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")) && 
            (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
        {
            isClimbing = true;
        }
        else if (isClimbing && Input.GetButtonDown("Jump")) isClimbing = false;
        if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder"))) isClimbing = false;

        if (isClimbing)
        {
            if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder"))) { return; }

            float deltaY = Input.GetAxis("Vertical"); //* Time.deltaTime;
            Vector2 playerVelocity = new Vector2(myRigidBody.velocity.x, deltaY * climbSpeed);
            myRigidBody.velocity = playerVelocity;
            Vector2 GravitySubtraction = new Vector2(0f, 22.5f);
            myRigidBody.AddForce(GravitySubtraction);
        }
        
    }

    private void FrontalFlip()
    {
        /*//print(timeForFlip += Time.deltaTime);
        timeForFlip += Time.deltaTime;
        if (Input.GetButtonDown("Fire3") && timeForFlip > dashTimeLength)
        {
            timeForFlip = 0;
            myRigidBody.velocity = new Vector2(0f,myRigidBody.velocity.y);
            myAnimator.SetTrigger("Buttom");
            isDashing = true;
        }
        if (timeForFlip > dashTimeLength)
        {
            isDashing = false;
            return;
        }
            Vector3 actuallPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        if (transform.localScale.x > 0)
        {
            Vector3 flipTargetPosition = new
                Vector3(transform.position.x + dashDeltaDistance, transform.position.y, transform.position.z);
            transform.position = Vector2.MoveTowards(actuallPos, flipTargetPosition, flipSpeed);
        } else
        {
            Vector3 flipTargetPosition = new
                Vector3(transform.position.x - dashDeltaDistance, transform.position.y, transform.position.z);
            transform.position = Vector2.MoveTowards(actuallPos, flipTargetPosition, flipSpeed);
        }
             */
    }

    private void GunCoolDown()
    {
        Debug.Log(gunHeat);
        gunHeat += 1f;
        gunHeat = Mathf.Clamp(gunHeat, 0f, gunMax);
        if(gunHeat <= 0) { isGunHeated = true; }
        if(isGunHeated && gunHeat == 250) { isGunHeated = false; }
    }

    private void Fire2()
    {
        /*fireAuxiliarTimer += Time.deltaTime;
        if (Input.GetButtonDown("Fire1") && fireAuxiliarTimer > 0.5f)
        {
            fireCoroutine = StartCoroutine(FireContinuously());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(fireCoroutine);
            if(fireAuxiliarTimer > 0.5f) { fireAuxiliarTimer = 0; }
        }
        */
    }

    private void FireAuxiliar()
    {
        if (Input.GetButtonDown("Fire1")) { keepFiring = true; }
        if (Input.GetButtonUp("Fire1"))
        {
            keepFiring = false;
            if(fireAuxiliarTimer > 0.51) { fireAuxiliarTimer = 0; }
        }
    }

    public float GetGunCharge() { return gunCharge; }

    private void FireTry()
    {
        float fireRate;
        float adrenalineAdd;
        if (GameGlobalInfo.bulletChip == 4) { fireRate = 0.2f; }
        else if (GameGlobalInfo.bulletChip == 5) { fireRate = 0.425f; }
        else { fireRate = 0.375f; }
        if ((GameGlobalInfo.hasAdrenaline && health2 == 0) || (GameGlobalInfo.hasAdrenaline && health2 == GameGlobalInfo.maxShields))
        { adrenalineAdd = 0.075f; }
        else { adrenalineAdd = 0; }
        fireAuxiliarTimer += Time.deltaTime;
        gunCharge = fireRate - adrenalineAdd;
        if (isPlayer0) { gunCharge = 0.375f; }
        if (keepFiring && fireAuxiliarTimer > fireRate - adrenalineAdd)
        {
            timeForResting = 0;
            if (GameGlobalInfo.bulletChip == 2)
            {
                bullets = Instantiate(bullet2,
                new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
                Quaternion.identity) as GameObject;
            }
            else if (GameGlobalInfo.bulletChip == 3)
            {
                bullets = Instantiate(bullet3,
                new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
                Quaternion.identity) as GameObject;
            }
            else if (GameGlobalInfo.bulletChip == 4)
            {
                bullets = Instantiate(bullet4,
                new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
                Quaternion.identity) as GameObject;
            }
            else if (GameGlobalInfo.bulletChip == 5)
            {
                bullets = Instantiate(bullet5,
                new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
                Quaternion.identity) as GameObject;
            }
            else 
            {
                bullets = Instantiate(bullet,
                new Vector3(bulletExit.transform.position.x, bulletExit.transform.position.y, -1),
                Quaternion.identity) as GameObject;
            }


            if (transform.localScale.x > 0)
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 270);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed(), 0f));
            }
            else
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 90);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() * -1f, 0f));
            }
            fireAuxiliarTimer = 0;
            som.Pj_Tiro();
        }
    }

    public float GetTimer()
    {
        return fireAuxiliarTimer;
    }

    public void AddTecnology(int amount)
    {
        float add;
        if (GameGlobalInfo.hasCollector) { add = amount * 1.5f; }
        else { add = amount; }
        GameGlobalInfo.tecnology += (int)add;
    }

    IEnumerator FireContinuously()
    {
        while (true)
        {
            GameObject bullets = Instantiate(bullet,
            new Vector3(transform.position.x + 0.1f * transform.localScale.x, transform.position.y + 0.25f,
            transform.position.z), Quaternion.identity) as GameObject;
            //bullets.GetComponent<Rigidbody2D>().velocity = new Vector2(0, bullets.GetComponent<Bullet>().GetBulletSpeed());
            if (transform.localScale.x > 0)
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 270);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed(), 0f));
            }
            else
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 90);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() * -1f, 0f));
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    private void Fire()
    {
        timeForShoot += Time.deltaTime;
        if(timeForShoot < 0.25f)
        {
            //isShooting = true;
            //if (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")))
            //{ myRigidBody.velocity = new Vector2(0f, myRigidBody.velocity.y); }
        }
        else { isShooting = false; }
        if (Input.GetButtonDown("Fire1") && timeForShoot > 0.25)
        {
            gunHeat -= shootHeat;
            timeForShoot = 0f;
            //myAnimator.SetTrigger("Shooting");
            GameObject bullets = Instantiate(bullet,
            new Vector3(transform.position.x + 0.1f* transform.localScale.x, transform.position.y + 0.25f,
            transform.position.z),
            Quaternion.identity) as GameObject;
            
            /*if (transform.localScale.x > 0 && Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 305);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed()/1.8f, 
                    FindObjectOfType<Bullet>().GetBulletSpeed()/1.8f));
            }
            else if (transform.localScale.x > 0 && Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 225);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f));
            }
            else if (transform.localScale.x < 0 && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 45);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f));
            }
            else if (transform.localScale.x < 0 && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 135);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f));
            }
            else if (Input.GetKey(KeyCode.W))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 0);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(0f, FindObjectOfType<Bullet>().GetBulletSpeed()));
            }
            else*/ if (transform.localScale.x > 0)
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 270);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed(), 0f));
            }
            else
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 90);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() * -1f, 0f));
            }
            
        } 
    }

    private void Die()
    {
        timeForRevive += Time.deltaTime;
        if (health2 <= -1 && !IsInvoking("Revive"))
        {
            //GameObject deathParticles = Instantiate(deathEffect, transform.position, Quaternion.identity);
            //Destroy(deathParticles, 1.5f);
            isDead = true;
            //rend.material.color = new Vector4(219, 219, 219, 0);
            Debug.Log("VER ISSO"+myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            //float x = myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            //if (myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime - deadAnimationStartTime >1) { Revive(); }
            Invoke("Revive", 2);
        }
        if (health2 <= -1 && myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")) && timeForRevive > 0.3)
        {
            myRigidBody.velocity = new Vector2(0f, 0f);
        }
        //if (timeForRevive > 2f && isDead)
        //{
        //Revive();
        //}
    }

    private void Revive()
    {
        anim.SetTrigger("UpdateHUD");
        Invoke("SpawnBug", 1);
        health2 = 5;
        isDead = false;
        rend.material.color = startColor;
        myAnimator.SetTrigger("Revive");
        cameraController.cameraIndex = lastCheckpointCameraIndex;
        if (!f2) { transform.position = lastCheckpoint; }
        else { transform.position = new Vector3(lastCheckpoint.x, lastCheckpoint.y,-1); }
        transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);
        myRigidBody.velocity = new Vector2(0,0);
        lastDirection = 1;
        if (!f2) { FindObjectOfType<Boss>().ResetBoss(); }
        Debug.Log("RightBeforeResetGate");
        if (!f2) { for (int i = 0; i < gates.Length; i++) { gates[i].closeGatePermanently = false; } } //gates[i].myAnimator.SetTrigger("Open"); }
    }

    private void SpawnBug() { CameraShake.spawnAuxiliar = false; }

    private void Invulnerable()
    {
        //Vector4 original = startColor;
        //Debug.Log("ESSEAQUIKKKK"+(int)invulnerableTimer);
        invulnerableTimer += Time.deltaTime;
        //Debug.Log("Timer: "+invulnerableTimer);

        if (invulnerableTimer < GameGlobalInfo.invulnerableTime && isInvulnerable)
        {
            //StartCoroutine(AppearAndDisappear());
            InvulnerableFB();
        }
        else
        {
            rend.material.color = startColor;
            isInvulnerable = false;
        }

    }

    private void InvulnerableFB()
    {

        invulnerableFBTimer += Time.deltaTime;

        if (invulnerableFBTimer < 0.2)
        {
            rend.material.color = new Vector4(219, 219, 219, 0);
        }
        else
        {
            rend.material.color = startColor;
        }

        if (invulnerableFBTimer > 0.4) { invulnerableFBTimer = 0; }
    }

    private void MovingPlatAdjust()
    {
        //Debug.Log(myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")));
        if (myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
        {
            MovingPlatforms movingPlat = FindObjectOfType<MovingPlatforms>();
            /*
            if (counter % 2 == 0)
            {
                myRigidBody.velocity = new Vector2(myRigidBody.velocity.x + mPHSpeed, 
                    myRigidBody.velocity.y);
            }
            else
            {
                myRigidBody.velocity = new Vector2(myRigidBody.velocity.x - mPHSpeed,
                    myRigidBody.velocity.y - Mathf.Abs(mPVSpeed*5));
            }
            */

            //Debug.Log("isTouching" + movingPlat.GetCounter());
        }
    }

    public int GetHealth()
    {
        return Mathf.Clamp(health2, 0, 10);
    }

    public int GetHealth2() { return health2; }

    /*public int GetTecnology()
    {
        return tecnology;
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //print("acertoukk");
        /*
        DamageDealer damageDealer = FindObjectOfType<DamageDealer>();
        Bullet bullet = FindObjectOfType<Bullet>();
        AirForce airForce = FindObjectOfType<AirForce>();
        PasstroughPlatform platform = FindObjectOfType<PasstroughPlatform>();
        SensitivePlatform sensitivePlatform = FindObjectOfType<SensitivePlatform>();
        if ((!damageDealer && !bullet) || airForce || platform || sensitivePlatform) { return; }
        */
        if (collision.gameObject.GetComponent<Collectable>() != null && !IsInvoking("BugFixer") && health2 != GameGlobalInfo.maxShields)
        {
            float add;
            if (GameGlobalInfo.hasCollector) { add = 1.5f; }
            else { add = 1; }
            //Destroy(collision.gameObject);
            /*if (!f2) {*/ som.Coletavel(); //}
            collision.gameObject.GetComponent<Collectable>().TurnOff();
            if (collision.gameObject.CompareTag("Shield"))
            { if (health2 != GameGlobalInfo.maxShields) { anim.SetTrigger("GetShield"); }
                health2 += collision.gameObject.GetComponent<Collectable>().GetShield();  }
            else if (collision.gameObject.CompareTag("Tecnology"))
            { GameGlobalInfo.tecnology += collision.gameObject.GetComponent<Collectable>().GetTecnology() * (int)add; }
            health2 = Mathf.Clamp(health2, -1, GameGlobalInfo.maxShields);
            Invoke("BugFixer", 0.25f);
        }

        if (collision.gameObject.CompareTag("Moving Platform") && !MovingPlatforms.childRepelent && 
            myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
        //&& myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
        {
            jumpOffMovingPlatAuxiliarTimer2 = 0;
            //Debug.Log("What about here?"+MovingPlatforms.childRepelent);
            movingPlat = collision.gameObject;
            stickToPlat = true;
        }

        if (collision.gameObject.GetComponent<CheckpointPlatform>() != null)
        {
            lastCheckpoint = new Vector2(collision.gameObject.GetComponent<Transform>().transform.position.x,
                collision.gameObject.GetComponent<Transform>().transform.position.y + 2);
            lastCheckpointCameraIndex = cameraController.cameraIndex;
        }

        if (collision.gameObject.GetComponent<DamageDealer>() == null || isInvulnerable || isDead ||
            collision.gameObject.GetComponent<HazardShooterPlatform>() != null) { return; }
        DamageDealer damageDealer = FindObjectOfType<DamageDealer>();

        /*if (!f2)
        {*/
            som.Pj_Dano();
            if (collision.gameObject.GetComponent<HazardSparkles>()) { som.Choq_Contato(transform.position); }
        //}
        //Debug.Log("tag: " + collision.gameObject.tag);
        //Debug.Log("HEYSHAKING");
        //Debug.Log(cameraController.cameraIndex);
        //Debug.Log(cameraShake2.gameObject.name);
        cameraShake2 = FindObjectOfType<CameraShake>();
        //Debug.Log(cameraShake2.gameObject.name);
        StartCoroutine(cameraShake2.Shake(shakeParameters.x, shakeParameters.y));
        //StartCoroutine(cameraShake[cameraController.cameraIndex].Shake(shakeParameters.x, shakeParameters.y));
        //StartCoroutine(cameraShake.CameraShaking(1f,0.1f,1));
        //shakeTimer = 0;
        shortCircuitTimer = 0;
        timeForResting = 0;
        if (GameGlobalInfo.shortCircuit) { shortCircuit.SetActive(true);
            GameObject circuitLight = Instantiate(shortCircuitVF, new Vector3
                (transform.position.x, transform.position.y, -0.5f), Quaternion.identity); Destroy(circuitLight, 1); }
        //shortCircuit.GetComponent<Renderer>().material.color = new Vector4();
        health2--;
        damageFeedback.DamageFeedback();
        //health -= damageDealer.GetDamage() ;
        if(health2 <= -1)
        {
            myAnimator.SetTrigger("Dead");
            som.Pj_Morte();
            anim.SetTrigger("UpdateHUD");
            CameraShake.spawnAuxiliar = true;
            timeForRevive = 0;
            myRigidBody.velocity = new Vector2(0f, 0f);
            myRigidBody.AddForce(new Vector2(deathPushForce.x * lastDirection * -1, deathPushForce.y * 1.5f)); 
            deadAnimationStartTime = myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }
        else
        {
            invulnerableTimer = 0;
            isInvulnerable = true;
        }
        //Debug.Log("timer: "+invulnerableTimer);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Moving Platform") && stickToPlat)
        //&& myFeet.IsTouchingLayers(LayerMask.GetMask("Moving Platform")))
        {
            movingPlat = collision.gameObject;
            stickToPlat = false;
            afterPlatSpeed = xSpeedAdjust;
            //Debug.Log("IsTheAdjust? " + xSpeedAdjust);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<TeleportationPlatform>() != null)
        {
            teleportDestinyPos = collision.gameObject.GetComponent<TeleportationPlatform>().TeleportDestiny();
        }

        if (collision.gameObject.CompareTag("Moving Platform"))
        {
            //Debug.Log("PUTAMERDA: "+collision.gameObject.GetComponent<MovingPlatforms>().GetComponent<Rigidbody2D>().velocity.x);
            if (collision.gameObject.GetComponent<MovingPlatforms>() != null)
            { xSpeedAdjust = collision.gameObject.GetComponent<MovingPlatforms>().GetComponent<Rigidbody2D>().velocity.x; }
            if (collision.gameObject.GetComponent<AutomaticGate>() != null)
            { xSpeedAdjust = collision.gameObject.GetComponent<AutomaticGate>().GetComponent<Rigidbody2D>().velocity.x; }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<AirForce>() != null)
        {
            myRigidBody.AddForce(collision.gameObject.GetComponent<AirForce>().GetAirPushVector());
        }
        if (collision.gameObject.GetComponent<DamageDealer>() != null && !isInvulnerable && !isDead)
        {
            //StartCoroutine(cameraShake.Shake(shakeParameters.x, shakeParameters.y));
            cameraShake2 = FindObjectOfType<CameraShake>();
            //Debug.Log(cameraShake2.gameObject.name);
            StartCoroutine(cameraShake2.Shake(shakeParameters.x, shakeParameters.y));
            damageFeedback.DamageFeedback();
            health2--;
            //health -= damageDealer.GetDamage() ;
            if (health2 <= -1)
            {
                timeForRevive = 0;
                myAnimator.SetTrigger("Dead");
                deadAnimationStartTime = myAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
                myRigidBody.velocity = new Vector2(0f, 0f);
                if (!isDead) { myRigidBody.AddForce(deathPushForce); }
                CameraShake.spawnAuxiliar = true;
            }
            else
            {
                isInvulnerable = true;
                invulnerableTimer = 0;
            }
        }
    }
}
