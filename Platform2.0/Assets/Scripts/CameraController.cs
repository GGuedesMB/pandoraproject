﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //[SerializeField] GameObject [] gates;
    [SerializeField] GameObject [] cameras;
    [SerializeField] bool levelTesting;

    Player player;

    public int cameraIndex;
    bool nextRoom;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        cameraIndex = 0;
        /*
        cameraIndex = 0;
        cameras[cameraIndex].SetActive(true); 
        */
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Current camera: " + cameraIndex);
        if (!levelTesting)
        {
            for (int i = 0; i < cameras.Length; i++)
            {
                if(i == cameraIndex - 2) { cameras[i].GetComponent<CameraShake>().Follow(); }
                if (i == cameraIndex)
                {
                    cameras[i].SetActive(true);
                }
                if (i == cameraIndex + 1) { cameras[i].GetComponent<CameraShake>().Unfollow(); }
                if (i == cameraIndex + 2) { cameras[i].GetComponent<CameraShake>().Follow(); }
                if (i != cameraIndex) { cameras[i].SetActive(false); }
            }
        }
        else
        {
            cameraIndex = 20;
        }

        //Debug.Log("index" + cameraIndex);
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
    }

}
