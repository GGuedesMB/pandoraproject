﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player0 : MonoBehaviour
{
    //Confog parameters
    [SerializeField] float runSpeed;
    [SerializeField] float jumpForce;
    [SerializeField] float climbSpeed;
    [SerializeField] float flipSpeed;
    [SerializeField] GameObject bullet;
    [SerializeField] int health;
    [SerializeField] Vector2 deathPushForce;

    //Cached components
    Rigidbody2D myRigidBody;
    Animator myAnimator;
    Animation myAnimation;
    Collider2D myCollider2D;
    BoxCollider2D myFeet;

    bool isClimbing;
    bool isShooting;
    bool isDead;
    bool canDoubleJump;
    float timeForFlip;
    float timeForShoot;
    float timeForRevive;


    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myCollider2D = GetComponent<Collider2D>();
        myFeet = GetComponent<BoxCollider2D>();
        myAnimation = GetComponent<Animation>();
        isClimbing = false;
        timeForFlip = 1.5f;
        timeForRevive = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        Die();
        if (isDead) { return; }
        if (!isShooting)
        {
            Run();
            FlipPlayer();
            //Climb();
            //FrontalFlip();
        }
        Jump();
        //Fire();
    }

    //Methods 
    private void Run()
    {
        //INPUT MANAGER - MUDANDO O VETOR VELOCIDADE
        float deltaX = Input.GetAxis("Horizontal");
        Vector2 playerVelocity = new Vector2(deltaX * runSpeed, myRigidBody.velocity.y);
        myRigidBody.velocity = playerVelocity;

        bool isPlayerRunning = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;
        myAnimator.SetBool("Running", isPlayerRunning);
    }

    private void FlipPlayer()
    {
        bool playerHasHorizontalSpeed = Mathf.Abs(myRigidBody.velocity.x) > Mathf.Epsilon;

        if (playerHasHorizontalSpeed)
        {
            float direction = Mathf.Sign(myRigidBody.velocity.x);
            Vector2 newScale = new Vector2(direction, transform.localScale.y);
            transform.localScale = newScale;
        }
    }

    private void Jump()
    {
        //if (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground"))) { canDoubleJump = true; }
        if (!myFeet.IsTouchingLayers(LayerMask.GetMask("Ground"))) { return; }
        if (Input.GetButtonDown("Jump") && myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            Vector2 additionForce = new Vector2(0f, jumpForce);
            myRigidBody.AddForce(additionForce);
        }
    }

    private void Climb()
    {
        myAnimator.SetBool("Climbing", isClimbing);
        if (myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder")) &&
            (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W)))
        {
            isClimbing = true;
        }
        else if (isClimbing && Input.GetButtonDown("Jump")) isClimbing = false;
        if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder"))) isClimbing = false;

        if (isClimbing)
        {
            if (!myCollider2D.IsTouchingLayers(LayerMask.GetMask("Ladder"))) { return; }

            float deltaY = Input.GetAxis("Vertical"); //* Time.deltaTime;
            Vector2 playerVelocity = new Vector2(myRigidBody.velocity.x, deltaY * climbSpeed);
            myRigidBody.velocity = playerVelocity;
            Vector2 GravitySubtraction = new Vector2(0f, 22.5f);
            myRigidBody.AddForce(GravitySubtraction);
        }

    }

    private void FrontalFlip()
    {
        //print(timeForFlip += Time.deltaTime);
        timeForFlip += Time.deltaTime;
        if (Input.GetButtonDown("Fire3") && timeForFlip > 0.3f)
        {
            timeForFlip = 0;
            myAnimator.SetTrigger("Buttom");
        }
        if (timeForFlip > 0.3) { return; }
        Vector3 actuallPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        if (transform.localScale.x > 0)
        {
            Vector3 flipTargetPosition = new
                Vector3(transform.position.x + 0.1f, transform.position.y, transform.position.z);
            transform.position = Vector2.MoveTowards(actuallPos, flipTargetPosition, flipSpeed);
        }
        else
        {
            Vector3 flipTargetPosition = new
                Vector3(transform.position.x - 0.1f, transform.position.y, transform.position.z);
            transform.position = Vector2.MoveTowards(actuallPos, flipTargetPosition, flipSpeed);
        }

    }

    private void Fire()
    {
        timeForShoot += Time.deltaTime;
        if (timeForShoot < 0.25f)
        {
            isShooting = true;
            if (myFeet.IsTouchingLayers(LayerMask.GetMask("Ground")))
            { myRigidBody.velocity = new Vector2(0f, myRigidBody.velocity.y); }
        }
        else { isShooting = false; }
        if (Input.GetButtonDown("Fire1") && timeForShoot > 0.25)
        {
            timeForShoot = 0f;
            myAnimator.SetTrigger("Shooting");
            GameObject bullets = Instantiate(bullet,
            new Vector3(transform.position.x + 0.5f * transform.localScale.x, transform.position.y - 0.35f, transform.position.z),
            Quaternion.identity) as GameObject;

            if (transform.localScale.x > 0 && Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 305);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f));
            }
            else if (transform.localScale.x > 0 && Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 225);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f));
            }
            else if (transform.localScale.x < 0 && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 45);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / 1.8f));
            }
            else if (transform.localScale.x < 0 && Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 135);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f,
                    FindObjectOfType<Bullet>().GetBulletSpeed() / -1.8f));
            }
            else if (Input.GetKey(KeyCode.W))
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 0);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(0f, FindObjectOfType<Bullet>().GetBulletSpeed()));
            }
            else if (transform.localScale.x > 0)
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 270);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed(), 0f));
            }
            else
            {
                bullets.transform.rotation = Quaternion.Euler(0, 0, 90);
                bullets.GetComponent<Rigidbody2D>().
                    AddForce(new Vector2(FindObjectOfType<Bullet>().GetBulletSpeed() * -1f, 0f));
            }

        }
    }

    private void Die()
    {
        print(timeForRevive);
        timeForRevive += Time.deltaTime;
        if (health <= 0)
        {
            isDead = true;
            if (myRigidBody.velocity.y == 0)
            {
                myRigidBody.velocity = new Vector2(0f, 0f);
            }
        }
        if (timeForRevive > 2f && isDead)
        {
            health = 500;
            isDead = false;
            myAnimator.SetTrigger("Revive");
            transform.position = new Vector2(-17f,2.5f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        print("acertoukk");
        DamageDealer damageDealer = FindObjectOfType<DamageDealer>();
        Bullet bullet = FindObjectOfType<Bullet>();
        if (!damageDealer && !bullet) { return; }
        health -= damageDealer.GetDamage();
        if (health <= 0)
        {
            myAnimator.SetTrigger("Dead");
            myRigidBody.velocity = new Vector2(0f, 0f);
            if (!isDead) { myRigidBody.AddForce(deathPushForce); }
            timeForRevive = 0;
        }
    }
}
