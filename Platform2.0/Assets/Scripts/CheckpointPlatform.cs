﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointPlatform : MonoBehaviour
{ 
    [SerializeField] bool isUpgradePoint;
    [SerializeField] GameObject buttons;


    Animator myAnimator;
    Som som;
    //Collider2D myCollider;

    bool activated;

    // Start is called before the first frame update
    void Start()
    {
        myAnimator = GetComponent<Animator>();
        if (!isUpgradePoint) { buttons.SetActive(false); }
        som = FindObjectOfType<Som>();
        //myCollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        myAnimator.SetBool("activated", activated);
        //myCollider.enabled = true;
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Player>() != null && !activated)
        {
            som.Checkpoint();
            activated = true;
            if (isUpgradePoint) { GameGlobalInfo.canAccessUpgrades = true; }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            if (isUpgradePoint) { GameGlobalInfo.canAccessUpgrades = true; }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            if (isUpgradePoint) { GameGlobalInfo.canAccessUpgrades = false; }
        }
    }

}
