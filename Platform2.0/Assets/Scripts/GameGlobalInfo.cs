﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameGlobalInfo : MonoBehaviour
{

    [SerializeField] Player player;

    public static bool canAccessUpgrades;
    public static int tecnology;

    public static int bulletChip;
    public static bool bullet2Acquired;
    public static bool bullet3Acquired;
    public static bool bullet4Acquired;
    public static bool bullet5Acquired;

    public static int suitChip;
    public static int maxShields;
    public static bool shortCircuit;
    public static float invulnerableTime;
    public static bool suit2Acquired;
    public static bool suit3Acquired;
    public static bool suit4Acquired;

    public static int habilitiesChip;
    public static bool hasBoostedBoots;
    public static bool dashInvulnerabilty;

    public static int pjChip;
    public static bool hasAdrenaline;
    public static bool hasCourage;
    public static bool hasCollector;
    public static bool pj2Acquired;
    public static bool pj3Acquired;
    public static bool pj4Acquired;

    public static string playerName;
    public static int playerHealth;
    

    private void Awake()
    {
        int objectQuantity = FindObjectsOfType<GameGlobalInfo>().Length;
        if (objectQuantity > 1) { Destroy(gameObject); }
        else { DontDestroyOnLoad(gameObject); }
    }

    // Start is called before the first frame update
    void Start()
    {
        bulletChip = 1;
        suitChip = 1;
        pjChip = 1;
        invulnerableTime = 2;
        maxShields = 5;
        shortCircuit = false;
        //tecnology = 1000;
        tecnology = 0;
        canAccessUpgrades = false;
        hasAdrenaline = false;
        hasCollector = false;
        hasCourage = false;
    }

    public void Restart()
    {
        bulletChip = 1;
        suitChip = 1;
        pjChip = 1;
        invulnerableTime = 2;
        maxShields = 5;
        shortCircuit = false;
        //tecnology = 1000;
        tecnology = 0;
        canAccessUpgrades = false;
        hasAdrenaline = false;
        hasCollector = false;
        hasCourage = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*Debug.Log("tecnology: " + tecnology);
        Debug.Log("BulletChip: " + bulletChip);
        Debug.Log("SuitChip: " + suitChip);
        Debug.Log("PjChip: " + pjChip);*/
        if(suitChip == 1)
        {
            invulnerableTime = 2;
            maxShields = 5;
            shortCircuit = false;
        }
        else if(suitChip == 2)
        {
            maxShields = 7;
            invulnerableTime = 2;
            shortCircuit = false;
        }
        else if (suitChip == 3)
        {
            maxShields = 5;
            invulnerableTime = 3;
            shortCircuit = false;
        }
        else if (suitChip == 4)
        {
            shortCircuit = true;
            maxShields = 5;
            invulnerableTime = 2;
        }

        if (pjChip == 1)
        {
            hasAdrenaline = false;
            hasCollector = false;
            hasCourage = false;
        }
        else if (pjChip == 2)
        {
            hasAdrenaline = true;
            hasCollector = false;
            hasCourage = false;
        }
        else if (pjChip == 3)
        {
            hasAdrenaline = false;
            hasCollector = true;
            hasCourage = false;
        }
        else if (pjChip == 4)
        {
            hasAdrenaline = false;
            hasCollector = false;
            hasCourage = true;
        }
        
        //Debug.Log("MaxShields: " + maxShields + "//Invulnerable time: " + invulnerableTime);
        //Debug.Log("name: " + playerName);
    }

    public void GetHealth() { playerHealth = player.GetHealth2(); }

}
