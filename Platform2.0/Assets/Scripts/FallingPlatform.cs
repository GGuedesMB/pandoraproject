﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{

    [SerializeField] float timeBeforeFalling;
    [SerializeField] float fallingAcceleration;
    [SerializeField] float respawnAfter;
    [SerializeField] float gravity;
    [SerializeField] bool special;
    [SerializeField] bool transition;
    public bool dontRespawn;

    public static bool specialRespawn;

    Rigidbody2D myRigidBody;
    GameObject sensiblePlat;
    Player player;
    CameraController cameraController;

    Vector2 startPos;
    bool isCrushing;
    bool falling;
    float timeCounter;
    float gravityForce;
    

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        cameraController = FindObjectOfType<CameraController>();
        startPos = transform.position;
        myRigidBody = GetComponent<Rigidbody2D>();
        isCrushing = false;
        sensiblePlat = gameObject;
        gravityForce = fallingAcceleration;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("respawnPlat: " + specialRespawn);
        //Debug.Log("boolCheck: " + isCrushing);
        if (isCrushing && !transition)
        {
            timeCounter += Time.deltaTime;
            if(timeCounter > timeBeforeFalling)
            {
                falling = true;
                //Destroy(gameObject, destroyAfter);
                if (!special || (specialRespawn && cameraController.cameraIndex <= 5))
                {
                    if (!dontRespawn) { Invoke("Respawn", respawnAfter); }
                    timeCounter = 0;
                    isCrushing = false;
                }  
                //else { Destroy(gameObject, 3); }
            }
        }
        if (falling) { Fall(); }
    }

    public void FallCommand() { falling = true; }

    private void Fall()
    {
        gravityForce *= gravity;
        gravityForce = Mathf.Clamp(gravityForce, -20, 20);
        myRigidBody.velocity = new Vector2(0f, -gravityForce);
        //if (myRigidBody.velocity.y > 50) { gravityForce = 0; }
    }

    public void Respawn() { if (dontRespawn) { return; } transform.position = startPos; myRigidBody.velocity = new Vector2(0, 0);
        falling = false; gravityForce = fallingAcceleration; }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null && player.GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            isCrushing = true;
        }
    }
}
