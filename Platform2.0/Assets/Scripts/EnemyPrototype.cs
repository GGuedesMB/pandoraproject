﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPrototype : MonoBehaviour
{

    //Config parameters
    [SerializeField] float speed;
    [SerializeField] float patrolTime;
    [SerializeField] int health;
    [SerializeField] GameObject explosionParticles;

    //Cached reference
    Rigidbody2D myRigidBody;
    BoxCollider2D fallingPrevent;

    //Variables declarations
    float time = 0;
    bool willFall;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Flip();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<DamageDealer>() != null) { return; }
        speed *= -1;
    }

    private void Move()
    {
        myRigidBody.velocity = new Vector2(speed, myRigidBody.velocity.y);
    }

    
    /*
    private void Patrol()
    {
        myRigidBody.velocity = new Vector2(speed, 0f);
        CountDownAndTurn();
    }

    
    private void CountDownAndTurn()
    {
        time -= Time.deltaTime;
        if (time <= 0f)
        {
            speed *= -1;
            time = patrolTime;
        }
    }
    */

    private void Flip()
    {
            float direction = Mathf.Sign(myRigidBody.velocity.x);
            Vector2 newScale = new Vector2(direction, transform.localScale.y);
            transform.localScale = newScale;
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        //DamageDealer damageDealer = collision.gameObject.GetComponent<DamageDealer>();
        if (collision.gameObject.GetComponent<DamageDealer>() != null)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            if (health <= 0)
            {
                Destroy(gameObject);
                GameObject explosion = Instantiate(explosionParticles,
                    new Vector3(transform.position.x, transform.position.y, transform.position.z),
                    Quaternion.identity) as GameObject;
                Destroy(explosion.gameObject, 1f);
            }
        }
    }
}
