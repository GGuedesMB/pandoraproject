﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportationPlatform : MonoBehaviour
{
    [SerializeField] Transform nextPos;

    public Vector2 TeleportDestiny()
    {
        return new Vector2(nextPos.transform.position.x, nextPos.transform.position.y + 2);
    }

}
