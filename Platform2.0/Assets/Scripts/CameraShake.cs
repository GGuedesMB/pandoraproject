﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
    //[SerializeField] Player[] players;
    [SerializeField] bool ricBoss;
    [SerializeField] bool bossCamera;
    [SerializeField] Boss boss;
    [SerializeField] float size;
    [SerializeField] float amplitudeGain_;
    [SerializeField] float frequencyGain_;
    [SerializeField] float limiteEsquerda;
    [SerializeField] float limiteDireita;
    [SerializeField] float limiteCima;
    [SerializeField] float limiteBaixo;
    [SerializeField] float smoothTimeX;
    [SerializeField] float smoothTimeY;
    [SerializeField] Transform[] positions;
    [SerializeField] GameObject[] enemiesPrefabs;
    [SerializeField] bool solo;

    GameObject[] enemies;
    float timerForEnemies;

    CinemachineVirtualCamera vcam;
    CinemachineBasicMultiChannelPerlin noise;

    Rigidbody2D myRigidBody;

    Player player;

    CameraController cameraController;

    float posX;
    float posY;

    bool atRightLimit;
    bool atLeftLimit;
    bool atTopLimit;
    bool atBottomLimit;

    float playerSpeedX;
    float playerSpeedY;

    float smoothVelX;
    float offSetX;
    float difX;

    float smoothVelY;
    float offSetY;
    float difY;
    public bool canSpawn;
    public static bool spawnAuxiliar;

    private void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
        noise = GetComponent<CinemachineBasicMultiChannelPerlin>();
        myRigidBody = GetComponent<Rigidbody2D>();
        cameraController = FindObjectOfType<CameraController>();
        player = FindObjectOfType<Player>();
        vcam.Follow = null;
        //Debug.Log(player.gameObject.name);
        vcam.m_Lens.OrthographicSize = size;
        canSpawn = true;

        //Debug.Log(GameObject.Find(enemies[i].name + "(Clone)"));
        enemies = new GameObject[positions.Length];

        //SpawnEnemies();

        //vcam.Follow = FindObjectOfType<Player>().GetComponent<Transform>();
    }

    public void SpawnEnemies()
    {
        //if (canSpawn)
        //{
            for (int i = 0; i < positions.Length; i++)
            {
                //Debug.Log(GameObject.Find(enemies[i].name + "(Clone)"));
                Destroy(enemies[i]);
                //else
                enemies[i] = Instantiate(enemiesPrefabs[i], positions[i].position, Quaternion.identity);
            }
            //canSpawn = false;
        //}
    }

    private void FixedUpdate()
    {
        
        //Debug.Log("its working");
        //if (FindObjectOfType<Player>() != null)
        //{
            float x = transform.localPosition.x;
            difX = player.transform.position.x - transform.localPosition.x;

            if (difX < limiteEsquerda) { offSetX = difX - limiteEsquerda; }
            if (difX > limiteDireita) { offSetX = difX - limiteDireita; }

            if (Mathf.Abs(smoothVelX) < 0.5f) { smoothVelX = 0; }
            float newX = Mathf.SmoothDamp(x, x + offSetX, ref smoothVelX, smoothTimeX);

            float y = transform.localPosition.y;
            difY = player.transform.position.y - transform.localPosition.y;

            if (difY < limiteBaixo) { offSetY = difY - limiteBaixo; }
            if (difY > limiteCima) { offSetY = difY - limiteCima; }

            if(Mathf.Abs(smoothVelY) < 0.5f) { smoothVelY = 0; }
            float newY = Mathf.SmoothDamp(y, y + offSetY, ref smoothVelY, smoothTimeY);

            transform.localPosition = new Vector3(newX, newY, transform.localPosition.z);

            /*Debug.Log("achou o objeto?");
            playerSpeedX = Mathf.Abs(player.GetComponent<Rigidbody2D>().velocity.x);
            playerSpeedY = Mathf.Abs(player.GetComponent<Rigidbody2D>().velocity.y);
            float x = Input.GetAxis("Horizontal");

            if (atRightLimit && x > Mathf.Epsilon)
            {
                Debug.Log("GoRight");
                Debug.Log(playerSpeedX);
                myRigidBody.velocity = new Vector2(playerSpeedX, myRigidBody.velocity.y); }
            else if (atLeftLimit && x < Mathf.Epsilon)
            { myRigidBody.velocity = new Vector2(-playerSpeedX, myRigidBody.velocity.y); }
            else { myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y); }

            if (atTopLimit)
            { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, 3); }
            else if (atBottomLimit)
            { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, -3); }
            else { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, 0); }*/
        //}
    }

    public void FindPlayer()
    {
        player = FindObjectOfType<Player>();
        Debug.Log(player.name);
    }

    private void Update()
    {
        if (ricBoss && CutsceneManager.OnCutscene) { vcam.m_Lens.OrthographicSize = 5; } else if (ricBoss) { vcam.m_Lens.OrthographicSize = 10; }
        if (spawnAuxiliar) { canSpawn = true; }
        if (bossCamera && boss.angry) { vcam.m_Lens.OrthographicSize *= 1.005f;
            vcam.m_Lens.OrthographicSize = Mathf.Clamp(vcam.m_Lens.OrthographicSize, 8, 8.8f); }
        else if(bossCamera && !boss.angry)
        {
            vcam.m_Lens.OrthographicSize *= 0.995f;
            vcam.m_Lens.OrthographicSize = Mathf.Clamp(vcam.m_Lens.OrthographicSize, 8, 8.8f);
        }
        //if (player.GetHealth() <= 0) { canSpawn = true; }
        //if (solo) { Debug.Log(player.gameObject.name); }
        timerForEnemies = Time.deltaTime;
        //Debug.Log("Its working2");
        //if(FindObjectOfType<Player>() != null)
        //{
            //if(Mathf.Abs(player.transform.position.x - transform.localPosition.x) > 4)

            //Debug.Log("working?");
            if (player.transform.position.x - transform.localPosition.x >= 3) { atRightLimit = true; }
            else { atRightLimit = false; }
            if (player.transform.position.x - transform.localPosition.x <= -3) { atLeftLimit = true; }
            else { atLeftLimit = false; }

            if (player.transform.position.y - transform.localPosition.y >= 1) { atTopLimit = true; }
            else { atTopLimit = false; }
            if (player.transform.position.y - transform.localPosition.y <= -1) { atBottomLimit = true; }
            else { atBottomLimit = false; }

            //Debug.Log("AtLeft" + atLeftLimit);
            //Debug.Log("AtRight" + atRightLimit);
        //}
    }


    //Função do follow player
    public void Follow()
    {
        vcam.Follow = FindObjectOfType<Player>().GetComponent<Transform>();
    }

    public void Unfollow()
    {
        vcam.Follow = null;
    }


    //OPÇÃO USANDO O NOISE
    public IEnumerator CameraShaking(float duration, float amplitudeGain, float frequencyGain)
    {
        Debug.Log("Começou");

        Debug.Log(noise.m_AmplitudeGain);
        noise.m_AmplitudeGain = amplitudeGain;
        noise.m_FrequencyGain = frequencyGain;
        yield return new WaitForSeconds(duration);
        noise.m_AmplitudeGain = 0;
        noise.m_FrequencyGain = 0;
        Debug.Log("Terminou");
    }

    //OPÇÃO DESABILITANDO O FOLLOW PLAYER
    public IEnumerator Shake(float duration, float magnitude)
    {
        vcam.Follow = null;
        //Debug.Log("vai ficar top");
        Vector3 originalPos = transform.localPosition;
        //Debug.Log(originalPos);
        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            //Debug.Log("YAY");
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x + originalPos.x, y + originalPos.y, originalPos.z);
            //transform.localPosition = new Vector3(x + transform.position.x, y + transform.position.y, 
                //transform.position.z);

            elapsed += Time.deltaTime;
            yield return null;
        }

        
        //Debug.Log(transform.localPosition);
        //vcam.Follow = FindObjectOfType<Player>().GetComponent<Transform>();
        transform.localPosition = originalPos;
    }
}
