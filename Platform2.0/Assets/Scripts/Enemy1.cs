﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float health;
    [SerializeField] bool atMovingPlatform;
    [SerializeField] float platSpeed;
    [SerializeField] MovingPlatforms localPlatform;
    [SerializeField] GameObject hitFB;
    [SerializeField] Transform groundDetector;
    [SerializeField] float rayLength;

    Rigidbody2D myRigidBody;
    BoxCollider2D myBoxCollider;
    CircleCollider2D myCircleCollider;
    CapsuleCollider2D myCapsuleCollider;

    bool turning;
    bool stuned;
    float stunTime;

    float velocityBack;

    MovingPlatforms movingPlatform;

    float timeToTurn;
    Vector2 originalScale;
    int counter;
    float platXSpeed;

    LayerMask layerMask;

    float shortCircuitTimer;

    float feedbackTimer;
    Renderer rend;
    Vector4 startColor;

    float patrolSoundTimer;
    Som som;

    // Start is called before the first frame update
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myBoxCollider = GetComponent<BoxCollider2D>();
        myCircleCollider = GetComponent<CircleCollider2D>();
        myCapsuleCollider = GetComponent<CapsuleCollider2D>();
        originalScale = transform.localScale;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        som = FindObjectOfType<Som>();
    }

    private void FixedUpdate()
    {
        //if (atMovingPlatform)
        //{
        //    StayOnMovingPlatform();
        //    Flip();
        //}
        //Move();
        
        Move();
        layerMask = 1 << LayerMask.NameToLayer("Ground");
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetector.position, Vector2.down, rayLength, layerMask);
        //Debug.Log(groundInfo.collider.name);
        if (groundInfo.collider == false && !turning && !stuned)
        {
            //Debug.Log("RayTest");
            speed *= -1;
            turning = true;
        }
        else if (groundInfo.collider == false && stuned)
        {
            Debug.Log("ThisMoment");
            velocityBack = 0;
        }

        else if (turning && groundInfo.collider == true) { turning = false; }
        

        if (atMovingPlatform) { StayOnMovingPlatform(); }//MoveAtPlatform(); }
        if (!atMovingPlatform) { Flip(); }
        //if (!stuned) { Move(); }
       
    }

    // Update is called once per frame
    void Update()
    {
        patrolSoundTimer += Time.deltaTime;
        if (patrolSoundTimer > 0.5f) { som.Ene1_Passo(transform.position); patrolSoundTimer = 0; }
        feedbackTimer += Time.deltaTime;
        HitFeedBack();
        //Debug.Log("VelocityBack: "+ velocityBack + "Stuned: "+stuned);
        stunTime += Time.deltaTime;
        shortCircuitTimer += Time.deltaTime;
        if (Mathf.Abs(velocityBack) > Mathf.Epsilon) { stuned = true; }
        else { stuned = false; }

        velocityBack *= 0.9f;
        if (Mathf.Abs(velocityBack) < 0.5f) { velocityBack = 0; }

        if (myCircleCollider.IsTouchingLayers(LayerMask.GetMask("Ground"))) { velocityBack = 0; }
        //Debug.Log("OntheGround: " + myBoxCollider.IsTouchingLayers(LayerMask.GetMask("Ground")));
        if (!myBoxCollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && stunTime > 1)
        {
            Debug.Log("Entrei na função");
            //myRigidBody.AddForce(new Vector2(0, -20));
            myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, -5);
        }
        else { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, 0); }
        //Debug.Log("DebugTest");
        /*layerMask = 1 << LayerMask.NameToLayer("Ground");
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetector.position, Vector2.down, rayLength, layerMask);
        //Debug.Log(groundInfo.collider.name);
        if (groundInfo.collider == false && !turning)
        {
            Debug.Log("RayTest");
            speed *= -1;
            turning = true;
        }
        else if (turning && groundInfo.collider == true) { turning = false; }*/
        //if (atMovingPlatform) { Debug.Log("Enemy speed" + myRigidBody.velocity.x + gameObject.name); }
        //Debug.Log("$$$$"+localPlatform.GetComponent<Rigidbody2D>().velocity.x);
        //counter = localPlatform.GetCounter();
        //Move();
        //Debug.Log("EnemyVelocity: " + myRigidBody.velocity.x);
        //StayOnMovingPlatform();
        //ChangeDirection();
    }

    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }

    private void Move()
    {
        if (!atMovingPlatform) { myRigidBody.velocity = new Vector2(speed + velocityBack, myRigidBody.velocity.y); }
        else { myRigidBody.velocity = new Vector2(speed + localPlatform.GetComponent<Rigidbody2D>().velocity.x, 
            myRigidBody.velocity.y); }
    }

    private void Flip()
    {
        float direction = Mathf.Sign(Mathf.Sign(speed));//myRigidBody.velocity.x);
            Vector2 newScale = new Vector2(1.25f *direction, transform.localScale.y);
            transform.localScale = newScale;
    }

    private void ChangeDirection()
    {
        timeToTurn += Time.deltaTime;
        if(timeToTurn > 0.2)
        {
            if (myCircleCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            {
                speed *= -1;
                timeToTurn = 0;
            }
            if (!myBoxCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            {
                speed *= -1;
                timeToTurn = 0;
            }
        }
    }

    private void MoveAtPlatform()
    {
        /*float x;
        if(counter % 2 == 0)
        {
            x = speed + platSpeed;
        }
        else
        {
            x = speed - platSpeed;
        }*/

        float x = speed + platXSpeed;
        //if (x < 4 && x > 0) { x = 4; }
        //if (x > -4 && x < 0) { x = -4; }
        myRigidBody.velocity = new Vector2(x, myRigidBody.velocity.y);
        //Debug.Log("Countersifude: "+ localPlatform.GetCounter());
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<DamageDealer>() != null || 
            collision.gameObject.GetComponent<Player>() != null || 
            collision.gameObject.GetComponent<PasstroughPlatform>() != null ||
            collision.gameObject.GetComponent<Collectable>() != null) { return; }
        if (atMovingPlatform)
        {
            if (collision.gameObject.GetComponent<MovingPlatforms>() != null)
            {
                speed *= -1;
            }
            else return;
        }
        else
        {
            //speed *= -1;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Moving Platform"))
        {
            platXSpeed = collision.gameObject.GetComponent<MovingPlatforms>().GetComponent<Rigidbody2D>().velocity.x;
            counter = collision.gameObject.GetComponent<MovingPlatforms>().GetCounter();
            //Debug.Log("This velocity: " + 
                //collision.gameObject.GetComponent<MovingPlatforms>().GetComponent<Rigidbody2D>().velocity.x);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<MoleColisor>() != null) { return; }
        if(collision.gameObject.GetComponent<Bullet>() != null || (collision.gameObject.CompareTag("Short Circuit") &&
            shortCircuitTimer > 1))
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            feedbackTimer = 0;
            //Debug.Log(collision.gameObject.tag);
            if (collision.gameObject.CompareTag("Short Circuit"))
            {
                shortCircuitTimer = 0;
            }
            if (collision.gameObject.CompareTag("Player Bullet 3"))
            {
                if (collision.gameObject.GetComponent<Bullet>().GetComponent<Rigidbody2D>().velocity.x > Mathf.Epsilon)
                { velocityBack = 7; }
                else { velocityBack = -7; }
                Debug.Log("FoundBullet");
                myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
                Debug.Log("Myvelocity: "+myRigidBody.velocity.x);
                myRigidBody.AddForce(new Vector2(velocityBack, 0));
            }
            if (health <= 0)
            {
                som.Ene_Morre(transform.position);
                Destroy(gameObject);
                FindObjectOfType<Player>().GetComponent<Player>().AddTecnology((int)Random.Range(15, 50));
            }
        }
    }

    private void StayOnMovingPlatform()
    {
        if(localPlatform == null) { return; }
        transform.parent = localPlatform.transform;
        Vector2 scaleAdjust = new Vector2(originalScale.x / localPlatform.transform.localScale.x,
                originalScale.y / localPlatform.transform.localScale.y);
        transform.localScale = scaleAdjust;
        //Move();

        float direction = Mathf.Sign(myRigidBody.velocity.x);
        Vector2 newScale = new Vector2(direction, transform.localScale.y /* * originalScale.x / localPlatform.transform.localScale.x,
            originalScale.y / localPlatform.transform.localScale.y*/);
        transform.localScale = newScale;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        
    }

}
