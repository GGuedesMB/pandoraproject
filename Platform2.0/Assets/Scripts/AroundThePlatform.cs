﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AroundThePlatform : MonoBehaviour
{
    //[SerializeField] int horizontalLength;
    [SerializeField] float platVerticalSize;
    [SerializeField] float platHorizontalSize;
    [SerializeField] float speed;
    [Header("1=esquerda; 2=direita")]
    [Range(1, 2)] [SerializeField] int startDirection;
    [Header("0=direita; 1=baixo; 2=esquerda; 3=cima")]
    [Range(0, 3)] [SerializeField] int startingSide;
    [SerializeField] Transform platPosition;
    int inWhichSide;
    
    // Start is called before the first frame update
    void Start()
    {
        if (platPosition != null)
        {
            inWhichSide = startingSide;
            if (startingSide == 3)
            {
                transform.position = new Vector2(platPosition.transform.position.x,
                platPosition.transform.position.y + platVerticalSize / 2);
            }
            if (startingSide == 2)
            {
                transform.position = new Vector2(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y);
            }
            if (startingSide == 1)
            {
                transform.position = new Vector2(platPosition.transform.position.x,
                platPosition.transform.position.y - platVerticalSize / 2);
            }
            if (startingSide == 0)
            {
                transform.position = new Vector2(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y);
            }
        }
            //relação errada
           

    }

    // Update is called once per frame
    void Update()
    {
        if (platPosition != null) { GoArround(); }
    }

    private void GoArround()
    {
        if(startDirection % 2 == 0)
        {
            if(inWhichSide == 0)
            {
                Vector2 firstTarget = new Vector2(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, firstTarget, speed*Time.deltaTime);

                if(transform.position == new Vector3(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 1)
            {
                Vector2 secondTarget = new Vector2(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, secondTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 2)
            {
                Vector2 thirdTarget = new Vector2(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, thirdTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 3)
            {
                Vector2 fourthTarget = new Vector2(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, fourthTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2, transform.position.z)) { inWhichSide=0; }
            }
        }
        else
        {
            if (inWhichSide == 0)
            {
                Vector2 firstTarget = new Vector2(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, firstTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 1)
            {
                Vector2 secondTarget = new Vector2(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, secondTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x - platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 2)
            {
                Vector2 thirdTarget = new Vector2(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, thirdTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y - platVerticalSize / 2, transform.position.z)) { inWhichSide++; }
            }
            else if (inWhichSide == 3)
            {
                Vector2 fourthTarget = new Vector2(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2);
                transform.position = Vector2.MoveTowards(transform.position, fourthTarget, speed * Time.deltaTime);

                if (transform.position == new Vector3(platPosition.transform.position.x + platHorizontalSize / 2,
                platPosition.transform.position.y + platVerticalSize / 2, transform.position.z)) { inWhichSide = 0; }
            }
        }
    }


}
