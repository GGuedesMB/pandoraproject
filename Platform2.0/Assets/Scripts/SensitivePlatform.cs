﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensitivePlatform : MonoBehaviour
{
    [SerializeField] bool destroy;
    [SerializeField] float secondsTurnedOff;
    [SerializeField] int hitsToTurnOff;
    [SerializeField] GameObject on;
    [SerializeField] GameObject off;
    [SerializeField] GameObject getHitFeedback;

    BoxCollider2D myCollider;
    Animator myAnimator;
    Som som;

    int hitsCounter;
    bool isTurnedOff;
    float time;
    bool inFront;

    // Start is called before the first frame update
    void Start()
    {
        myCollider = GetComponent<BoxCollider2D>();
        time = secondsTurnedOff;
        myAnimator = GetComponent<Animator>();
        som = FindObjectOfType<Som>();
    }

    // Update is called once per frame
    void Update()
    {
        if (destroy) { return; }
        time += Time.deltaTime;
        if (time < secondsTurnedOff)
        {
            myCollider.enabled = false;
            hitsCounter = 0;
            GameObject offSignal = Instantiate(off, transform.position, Quaternion.identity) as GameObject;
        }
        else if(time >= secondsTurnedOff && !inFront)
        {
            myCollider.enabled = true;
            //GameObject onSignal = Instantiate(on, transform.position, Quaternion.identity) as GameObject;
        }
        //Debug.Log("inFront: "+ inFront);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Bullet>() != null && time >= secondsTurnedOff && !destroy)
        {
            hitsCounter++;
            if(hitsCounter >= hitsToTurnOff*2)
            {
                time = 0;
                GameObject offSignal = Instantiate(off, transform.position, Quaternion.identity) as GameObject;
                Destroy(offSignal, secondsTurnedOff);
            }
        }
        if (collision.gameObject.GetComponent<Bullet>() != null && destroy)
        {
            som.Plat_Queb(transform.position);
            hitsCounter++;
            myAnimator.SetTrigger("Hitten");
            if (hitsCounter >= hitsToTurnOff) //* 2)
            {
                //Destroy(gameObject, 0.3f);
                myCollider.enabled = false;
            }
            /*else
            {
                GameObject hitFeedback = Instantiate(getHitFeedback, transform.position, Quaternion.identity) as GameObject;
                hitFeedback.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y,
                    transform.localScale.z);
                Destroy(hitFeedback.gameObject, 0.1f);
            }*/
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            //time -= Time.deltaTime;
            inFront = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player1"))
        {
            //time -= Time.deltaTime;
            inFront = false;
        }
    }
}
