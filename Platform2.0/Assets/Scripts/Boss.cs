﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    //Colocar smoothDamp no walk;
    [SerializeField] Player player;
    [SerializeField] bool activated;

    [SerializeField] int health;

    [SerializeField] float seconsAnalysing;
    [SerializeField] float secondsToAttack;
    [SerializeField] float secondsToDash;
    [SerializeField] float secondsTojump;
    [SerializeField] float secondsToWalk;
    [SerializeField] float secondsWalking;

    [SerializeField] float secondsAfterAttack;
    [SerializeField] float secondsAfterDash;
    [SerializeField] float secondsAfterjump;
    [SerializeField] float secondsAfterWalk;

    [SerializeField] float jump1Force;
    [SerializeField] float walkingSpeed;
    [SerializeField] float dashSpeed;

    [SerializeField] Transform leftJumpSpot;
    [SerializeField] Transform rightJumpSpot;
    [SerializeField] Transform leftSpot;
    [SerializeField] Transform rightSpot;

    [SerializeField] Slider healthBar;
    [SerializeField] GameObject getHitFB;
    [SerializeField] HazardShooterPlatform[] canons;
    [SerializeField] GameObject[] afterBossPrefab;
    [SerializeField] GameObject bulletVFPrefab;


    Rigidbody2D myRigidBody;
    Animator myAnimator;
    CameraController cameraController;
    //Player player;
    CapsuleCollider2D myCollider;

    Vector3 startPosition;
    Vector2 originalScale;
    Vector2 target;
    float targetDirection;
    float targetDistance;

    float timer;
    float jumpInicialTime;
    float jumpFinalTime;
    float inicialDashSpeed;

    float decision;

    public bool angry;
    bool analyse;
    bool willJump;
    bool willDash;
    bool willAttack;
    bool willWalk;

    int hitsCounter;
    bool canJump;
    Vector2 jumpSpot;
    Vector2 spot;
    float canonTimer;
    bool activateCanon;
    int shootingPattern;
    bool getAngry;

    Renderer rend;
    Vector4 startColor;
    float feedbackTimer;

    public float endingTimer;
    public bool endingScene;

    float patternCounter;
    float patternDuration;

    Som som;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        feedbackTimer = 1;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        myCollider = GetComponent<CapsuleCollider2D>();
        originalScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        transform.localScale = originalScale;
        //player = FindObjectOfType<Player>();
        myAnimator = GetComponent<Animator>();
        myRigidBody = GetComponent<Rigidbody2D>();
        cameraController = FindObjectOfType<CameraController>();
        analyse = true;
        inicialDashSpeed = dashSpeed;
        healthBar.maxValue = health;
        healthBar.value = health;
        healthBar.gameObject.SetActive(false);
        angry = false;
        timer = -2;
        patternCounter = 0;
        patternDuration = 6;
        som = FindObjectOfType<Som>();
        GetComponent<AudioSource>().Pause();
    }

    // Update is called once per frame
    void Update()
    {
        if (FindObjectOfType<CameraController>().cameraIndex == 19 && !activated && !IsInvoking("InvokeActivated"))
        { Invoke("InvokeActivated",2); }
        else if (FindObjectOfType<CameraController>().cameraIndex != 19) { activated = false; }
        if (!activated) { GetComponent<AudioSource>().Pause(); return; }
        if (activated) { healthBar.gameObject.SetActive(true); }
        if (endingScene) { EndingScene(); }
        if (CutsceneManager.OnCutscene) { return; }
        if (DialogueManager.OnDialogue) { return; }
        
        feedbackTimer += Time.deltaTime;
        HitFeedBack();
        timer += Time.deltaTime;
        healthBar.value = health;
        canonTimer += Time.deltaTime;

        //if (Input.GetKeyDown(KeyCode.Alpha0)) { health = 100; }
        //Debug.Log("Analyse: " + analyse);
        //Debug.Log("Timer: " + timer);
        //Debug.Log("At floor: " + myCollider.IsTouchingLayers(LayerMask.GetMask("Ground")));
        //if (hitsCounter == 25) { angry = true; }
        //Debug.Log("CanJump" + canJump);
        //Debug.Log("velY" + Mathf.Abs(myRigidBody.velocity.y));
        if (analyse)
        {

            //myRigidBody.velocity = new Vector2(0, 0);
            LookAtPlayer();
            //dashSpeed = inicialDashSpeed;
            willAttack = false;
            willDash = false;
            willJump = false;
            willWalk = false;
            if (angry)
            {
                timer = 0;
                if (transform.position.x > 656.5f)
                {
                    targetDistance = rightSpot.position.x - transform.position.x;
                    jumpSpot = rightJumpSpot.position;
                    spot = rightSpot.position;
                }
                else
                {
                    targetDistance = leftSpot.position.x - transform.position.x;
                    jumpSpot = leftJumpSpot.position;
                    spot = leftSpot.position;
                }
                getAngry = true;
                analyse = false;
                Debug.Log("ANALYSEDtoAngry");
                return;
            }
            //Debug.Log("Player Distance: " + PlayerDistance());

            if (PlayerDistance() < 4) { willDash = true; Debug.Log("Dash"); }
            else if (PlayerDistance() >= 4)
            {
                decision = Random.Range(0, 10);
                if (decision >= 8) { willWalk = true; Debug.Log("Walk"); }
                else
                {
                    if (PlayerDistance() >= 4 && PlayerDistance() < 10)
                    {
                        decision = Random.Range(0, 10);
                        if (decision >= 7) { willJump = true; Debug.Log("Jump"); }
                        else { willDash = true; Debug.Log("Dash"); }
                    }
                    //oi guedesss
                    else if (PlayerDistance() >= 10)
                    {
                        decision = Random.Range(0, 10);
                        if (decision >= 3) { willJump = true; Debug.Log("Jump"); }
                        else { willWalk = true; Debug.Log("Walk"); }
                        }
                }
            }

            //else { willWalk = true; Debug.Log("Walk"); }
            timer = 0;
            targetDistance = PlayerDistance();
            targetDirection = PlayerDirection();
            analyse = false;
            if (willJump) { Invoke("AnimatorTrigger2", 1); }
            else if (willDash){ Invoke("AnimatorTrigger1", 1); }
            if (willDash)
            { som.B1_Ant_Dash(transform.position); Invoke("InvokeDashTrigger", secondsToDash); }
                Debug.Log("ANALYSED");
            if (willWalk) { Invoke("InvokeWalk", secondsToWalk); }
        }

        if (angry && getAngry)
        {
            Debug.Log("velY"+ Mathf.Abs(myRigidBody.velocity.y));
            willAttack = false;
            willDash = false;
            willJump = false;
            willWalk = false;
            //Debug.Log("reptiçãodaconfirmaçãokkk");
            if (!canJump && timer > 2)
            {
                transform.position = Vector2.MoveTowards(transform.position,
                          new Vector2(jumpSpot.x, transform.position.y),
                          walkingSpeed * Time.deltaTime);
                LookAhead();
                if (transform.position.x == jumpSpot.x)
                { Jump1(); canJump = true; activateCanon = true; canonTimer = 0; myAnimator.SetTrigger("Jump"); Invoke("InvokeGoingDown", 0.5f); }
            }
            if (canJump && Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon)
            {
                Debug.Log("AngryMoveOnJump");
                transform.position = Vector2.MoveTowards(transform.position,
                     new Vector2(spot.x, transform.position.y),
                     10 * Time.deltaTime);
                //Debug.Log("AngryMoveOnJump");
                LookAhead();
            }
            if (activateCanon && !IsInvoking("BackToGround"))
            {
                
                //LookAtPlayer();
                ShootingPatterns();
                Invoke("BackToGround", patternDuration);
                Invoke("InvokeAnalyse", patternDuration + 2);
                //getAngry = false;
            }
        }


        if (!analyse && !getAngry)
        {

            if (willAttack)
            {
                if (timer > secondsToAttack && timer < secondsToAttack + 0.5f) { /*Debug.Log("Attacking!");*/ }
                if (timer > secondsToAttack + secondsAfterAttack) { analyse = true; Debug.Log("Attacked"); }
            }

            if (willDash)
            {
                if (timer > secondsToDash && timer < secondsToDash + 0.5f)
                {
                    if(myRigidBody.velocity.x == 0) { som.B1_Dash(transform.position); }
                    Debug.Log("Dashing");
                    dashSpeed = inicialDashSpeed;
                    myRigidBody.velocity = new Vector2(dashSpeed * targetDirection, myRigidBody.velocity.y);
                }
                if (timer > secondsToDash + 0.5f)
                {
                    if (Mathf.Abs(dashSpeed) < 0.5f && !IsInvoking("Analyse"))
                    {
                        dashSpeed = 0;
                        myAnimator.SetTrigger("afterAction2");
                        Invoke("Analyse", secondsAfterDash);
                    }
                    else { dashSpeed *= 0.90f; }
                }
            }

            if (willJump)
            {
                if (!IsInvoking("Jump1") && timer < 0.5f)
                {
                    Invoke("Jump1", secondsTojump); Invoke("InvokeGoingDown", secondsTojump + 0.75f);
                }
                if (timer > + 0.5f && Mathf.Abs(myRigidBody.velocity.y) > Mathf.Epsilon) /*!myCollider.IsTouchingLayers(LayerMask.GetMask("Ground"))*/
                {
                   transform.position = Vector2.MoveTowards(transform.position,
                        new Vector2(transform.position.x + targetDistance * targetDirection, transform.position.y),
                        JumpXSpeed() * Time.deltaTime);
                    Debug.Log("ItShouldFollowOnJump");
                }
                if (timer > 0.5f + secondsTojump && myCollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && !IsInvoking("Analyse"))
                { Invoke("Analyse", secondsAfterjump); Invoke("AfterAction", 1); }//myAnimator.SetTrigger("Landing"); }
                
            }

            if (willWalk)
            {
                if (timer > secondsToWalk && timer < secondsToWalk + secondsWalking)
                {
                    transform.position = Vector2.MoveTowards(transform.position,
                          new Vector2(transform.position.x + targetDistance * targetDirection, transform.position.y),
                          walkingSpeed * Time.deltaTime);
                }
                else if (timer > secondsToWalk + secondsWalking) { analyse = true; myAnimator.SetTrigger("afterAction2");
                    myAnimator.SetBool("Walking", false); }
            }
        }
        if (!angry)
        { transform.position = new Vector2(Mathf.Clamp(transform.position.x, 644, 666), Mathf.Clamp(transform.position.y, 14.78f, 100)); }
        else { transform.position = new Vector2(transform.position.x, Mathf.Clamp(transform.position.y, 14.78f, 100)); }

        myAnimator.SetBool("willDash", willDash);
    }

    private void ShootingPatterns()
    {
        if (patternCounter == 0)
        {
            Invoke("CanonShooter0", 2);
            Invoke("CanonShooter1", 2);
            Invoke("CanonShooter2", 2);
            Invoke("CanonShooter3", 2);
            Invoke("CanonShooter4", 2);

            Invoke("CanonShooter5", 3);
            Invoke("CanonShooter6", 3);
            Invoke("CanonShooter7", 3);
            Invoke("CanonShooter8", 3);
            Invoke("CanonShooter9", 3);

            Invoke("CanonShooter10", 4);
            Invoke("CanonShooter11", 4);
            Invoke("CanonShooter12", 4);
            Invoke("CanonShooter13", 4);
            Invoke("CanonShooter14", 4);
            patternCounter++;
            patternDuration = 6;
        }
        else if (patternCounter == 1)
        {
            Invoke("CanonShooter0", 2);
            Invoke("CanonShooter1", 2.33f);
            Invoke("CanonShooter2", 2.67f);
            Invoke("CanonShooter3", 3);
            Invoke("CanonShooter4", 3.33f);

            Invoke("CanonShooter5", 3.67f);
            Invoke("CanonShooter6", 4);
            Invoke("CanonShooter7", 4.33f);
            Invoke("CanonShooter8", 4.67f);
            Invoke("CanonShooter9", 5);

            Invoke("CanonShooter10", 5.33f);
            Invoke("CanonShooter11", 6.67f);
            Invoke("CanonShooter12", 6.33f);
            Invoke("CanonShooter13", 6);
            Invoke("CanonShooter14", 5.67f);
            Invoke("CanonShooter4", 3.33f);

            Invoke("CanonShooter10", 7);
            Invoke("CanonShooter9", 7.33f);
            Invoke("CanonShooter8", 7.67f);
            Invoke("CanonShooter7", 8);
            Invoke("CanonShooter6", 8.33f);

            Invoke("CanonShooter5", 8.67f);
            patternCounter++;
            patternDuration = 10;
        }
        else if (patternCounter == 2)
        {
            Invoke("CanonShooter0", 2);
            Invoke("CanonShooter1", 2);
            Invoke("CanonShooter2", 2);
            Invoke("CanonShooter3", 2);
            Invoke("CanonShooter4", 2);
            Invoke("CanonShooter5", 2);
            Invoke("CanonShooter10", 2);
            Invoke("CanonShooter11", 2);
            Invoke("CanonShooter12", 2);
            Invoke("CanonShooter13", 2);
            Invoke("CanonShooter14", 2);
            //Pause
            Invoke("CanonShooter0", 3.5f);
            Invoke("CanonShooter1", 3.5f);
            Invoke("CanonShooter6", 3.5f);
            Invoke("CanonShooter7", 3.5f);
            Invoke("CanonShooter8", 3.5f);
            Invoke("CanonShooter9", 3.5f);
            Invoke("CanonShooter10", 3.5f);
            Invoke("CanonShooter11", 3.5f);
            Invoke("CanonShooter12", 3.5f);
            Invoke("CanonShooter13", 3.5f);
            Invoke("CanonShooter14", 3.5f);
            //pause
            Invoke("CanonShooter0", 5);
            Invoke("CanonShooter1", 5);
            Invoke("CanonShooter2", 5);
            Invoke("CanonShooter3", 5);
            Invoke("CanonShooter4", 5);
            Invoke("CanonShooter5", 5);
            Invoke("CanonShooter6", 5);
            Invoke("CanonShooter11", 5);
            Invoke("CanonShooter12", 5);
            Invoke("CanonShooter13", 5);
            Invoke("CanonShooter14", 5);
    
            patternCounter++;
            patternDuration = 7;
        }
        else if (patternCounter == 3)
        {
            Invoke("CanonShooter0", 2);
            Invoke("CanonShooter1", 2);
            Invoke("CanonShooter2", 2);
            //Invoke("CanonShooter3", 2);
            //Invoke("CanonShooter4", 2);
            Invoke("CanonShooter5", 2);
            //Invoke("CanonShooter6", 3);
            //Invoke("CanonShooter7", 3);
            //Invoke("CanonShooter8", 3);
            Invoke("CanonShooter9", 2);
            //Invoke("CanonShooter10", 4);
            //Invoke("CanonShooter11", 4);
            Invoke("CanonShooter12", 2);
            Invoke("CanonShooter13", 2);
            Invoke("CanonShooter14", 2);
            //PAUSE
            //Invoke("CanonShooter0", 4);
            //Invoke("CanonShooter1", 4);
            Invoke("CanonShooter2", 4);
            Invoke("CanonShooter3", 4);
            Invoke("CanonShooter4", 4);
            Invoke("CanonShooter5", 4);
            //Invoke("CanonShooter6", 4);
            //Invoke("CanonShooter7", 4);
            //Invoke("CanonShooter8", 4);
            Invoke("CanonShooter9", 4);
            Invoke("CanonShooter10", 4);
            Invoke("CanonShooter11", 4);
            Invoke("CanonShooter12", 4);
            //Invoke("CanonShooter13", 4);
            //Invoke("CanonShooter14", 4);
            //Pause
            Invoke("CanonShooter0", 5.5f);
            //Invoke("CanonShooter1", 5.5f);
            //Invoke("CanonShooter2", 5.5f);
            //Invoke("CanonShooter3", 5.5f);
            Invoke("CanonShooter4", 5.5f);
            Invoke("CanonShooter5", 5.5f);
            Invoke("CanonShooter6", 5.5f);
            Invoke("CanonShooter7", 5.5f);
            Invoke("CanonShooter8", 5.5f);
            Invoke("CanonShooter9", 5.5f);
            Invoke("CanonShooter10", 5.5f);
            //Invoke("CanonShooter11", 5.5f);
            //Invoke("CanonShooter12", 5.5f);
            //Invoke("CanonShooter13", 5.5f);
            Invoke("CanonShooter14", 5.5f);
            //PAUSE
            Invoke("CanonShooter0", 6.5f);
            Invoke("CanonShooter1", 6.5f);
            Invoke("CanonShooter2", 6.5f);
            Invoke("CanonShooter3", 6.5f);
            Invoke("CanonShooter4", 6.5f);
            Invoke("CanonShooter5", 6.5f);
            //Invoke("CanonShooter6", 6.5f);
            //Invoke("CanonShooter7", 6.5f);
            //Invoke("CanonShooter8", 6.5f);
            //Invoke("CanonShooter9", 6.5f);
            Invoke("CanonShooter10", 6.5f);
            Invoke("CanonShooter11", 6.5f);
            Invoke("CanonShooter12", 6.5f);
            Invoke("CanonShooter13", 6.5f);
            Invoke("CanonShooter14", 6.5f);
            patternCounter++;
            patternDuration = 8;
        }

    }

    private void InvokeWalk() { myAnimator.SetBool("Walking", true); }

    private void InvokeActivated()
    {
        //GetComponent<AudioSource>().Play();
        activated = true;
    }

    private void AnimatorTrigger1() { myAnimator.SetTrigger("willDoSomething2"); }
    private void AnimatorTrigger2() { myAnimator.SetTrigger("WillJump"); som.B1_Ant_Pulo(transform.position); }
    private void InvokeAfterAction() { myAnimator.SetTrigger("afterAction2"); }
    private void InvokeGoingDown() { myAnimator.SetTrigger("Landing"); }
    private void InvokeDashTrigger() { myAnimator.SetTrigger("Dash"); }

    private void BackToGround()
    {
        LookAtPlayer();
        angry = false;
        canonTimer = 0;
        timer = 0;
        hitsCounter = 0;
        canJump = false;
        activateCanon = false;
        myRigidBody.AddForce(new Vector2(600 * PlayerDirection(), 0));
        getAngry = false;
        Invoke("InvokeGoingDown", 0.9f);
    }

    public void ResetBoss()
    {
        willAttack = false;
        willJump = false;
        willDash = false;
        willWalk = false;
        feedbackTimer = 1;
        analyse = true;
        health = (int)healthBar.maxValue;
        healthBar.gameObject.SetActive(false);
        angry = false;
        getAngry = false;
        canJump = false;
        canonTimer = 0;
        activateCanon = false;
        activated = false;
        //timer = 0;
        transform.position = startPosition;
        hitsCounter = 0;
        rend.material.color = startColor;
        transform.localScale = originalScale;
        timer = -2;
        patternCounter = 0;
    }

    private void InvokeAnalyse()
    {
        analyse = true;
    }

    void CanonShooter0()
    {
        canons[0].SingleShot();
    }
    void CanonShooter1()
    {
        canons[1].SingleShot();
    }
    void CanonShooter2()
    {
        canons[2].SingleShot();
    }
    void CanonShooter3()
    {
        canons[3].SingleShot();
    }
    void CanonShooter4()
    {
        canons[4].SingleShot();
    }
    void CanonShooter5()
    {
        canons[5].SingleShot();
    }
    void CanonShooter6()
    {
        canons[6].SingleShot();
    }
    void CanonShooter7()
    {
        canons[7].SingleShot();
    }
    void CanonShooter8()
    {
        canons[8].SingleShot();
    }
    void CanonShooter9()
    {
        canons[9].SingleShot();
    }
    void CanonShooter10()
    {
        canons[10].SingleShot();
    }
    void CanonShooter11()
    {
        canons[11].SingleShot();
    }
    void CanonShooter12()
    {
        canons[12].SingleShot();
    }
    void CanonShooter13()
    {
        canons[13].SingleShot();
    }
    void CanonShooter14()
    {
        canons[14].SingleShot();
    }

    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }

    private void Analyse()
    {
        analyse = true;
    }

    private float Decision()
    {
        if (PlayerDistance() < 5) { decision = 1; }
        if (PlayerDistance() >= 5 && PlayerDistance() < 8) { decision = 2; }
        else { decision = 3; }
        return decision;
    }

    private float JumpXSpeed()
    {
        return targetDistance / 1.08f;
    }

    private float PlayerDistance()
    {
        return Mathf.Abs(player.transform.position.x - transform.position.x);
    }

    private float PlayerDirection()
    {
        return Mathf.Sign(player.transform.position.x - transform.position.x);
    }

    private void LookAtPlayer()
    {
        transform.localScale = new Vector2(Mathf.Abs(originalScale.x) * PlayerDirection(), originalScale.y);
    }

    private void LookAhead()
    {
        float direction = Mathf.Sign(myRigidBody.velocity.x);
        transform.localScale = new Vector2(Mathf.Abs(originalScale.x) * direction, originalScale.y);
    }

    private void Jump1()
    {
        som.B1_Pulo(transform.position);
        myAnimator.SetTrigger("Jump");
        if (angry) { myRigidBody.AddForce(new Vector2(0, 1500)); } 
        else { myRigidBody.AddForce(new Vector2(0, jump1Force)); }
    }

    void EndingScene()
    {
        endingTimer += Time.deltaTime;
        myRigidBody.velocity = new Vector2(0,0);
        if (endingTimer < 1000 && !IsInvoking("bulletEffectTrigger")) 
            { Time.timeScale = 0.5f; Invoke("bulletEffectTrigger",0.15f); }
        if (endingTimer < 1 && !IsInvoking("explosionSoundTrigger"))
        { Invoke("explosionSoundTrigger", 0.15f); }
        if (endingTimer > 0.5f && endingTimer < 2f) { FindObjectOfType<EndingVF>().BossEnding(); }
        else if (endingTimer >= 1000)
        {
            healthBar.gameObject.SetActive(false);
            afterBossPrefab[0].SetActive(false); afterBossPrefab[1].SetActive(false); afterBossPrefab[2].SetActive(true);
            CutsceneManager.OnCutscene = false;
            Time.timeScale = 1;
            endingScene = false;
            Destroy(gameObject);
        }   
    }

    private void explosionSoundTrigger()
    {
        som.explosao_som();
    }

    private void bulletEffectTrigger()
    {
        Debug.Log("Entrou");
        float x = Random.Range(-1, 1);
        float y = Random.Range(-3, 3);
        GameObject bulletVF = Instantiate(bulletVFPrefab,
            new Vector3(transform.position.x + x, transform.position.y + y, -5),Quaternion.identity);
        bulletVF.GetComponent<SpriteRenderer>().sortingLayerName = "Hazard";
        bulletVF.GetComponent<SpriteRenderer>().sortingOrder = 6;
        Destroy(bulletVF,1f);
        Debug.Log("Saiu");
    }

    private void TriggerSound()
    {
        som.B1_Morte();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Bullet>() != null && activated)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            hitsCounter += collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            if (health > 0) { feedbackTimer = 0; }
            if (hitsCounter >= 2000 && !angry)
            {
                angry = true;
                Debug.Log("Angry");
                //willAttack = false;
                //willDash = false;
                //willJump = false;
                //willWalk = false;
                /*if (transform.position.x > 656.5f)
                {
                    targetDistance = rightSpot.position.x - transform.position.x;
                    jumpSpot = rightJumpSpot.position;
                    spot = rightSpot.position;
                }
                else
                {
                    targetDistance = leftSpot.position.x - transform.position.x;
                    jumpSpot = leftJumpSpot.position;
                    spot = leftSpot.position;
                }*/
                //timer = 0;
                //Invoke("Jump1", 2);
            }
            if (health <= 0 && !CutsceneManager.OnCutscene)
            {
                Invoke("TriggerSound", 1);
                myRigidBody.velocity = new Vector2(0, 0);
                rend.material.color = startColor;
                CutsceneManager.OnCutscene = true;
                endingScene = true;
            }
        }
    }
}
