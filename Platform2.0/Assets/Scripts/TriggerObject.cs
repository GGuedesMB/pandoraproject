﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerObject : MonoBehaviour
{
    [Header("Activation")]
    [SerializeField] bool isActive;
    [SerializeField] bool shootTrigger;
    [SerializeField] bool secondJumpTrigger;
    [SerializeField] bool passByTrigger;

    [Header("Deactivation")]
    [SerializeField] bool byTime;
    [SerializeField] float activeDuration;
    [SerializeField] bool onlyOneCanBeActivated;
    [SerializeField] bool theSameWayItGetActivated;

    Collider2D myCollider;

    float time;

    private void Start()
    {
        myCollider = GetComponent<Collider2D>();
        time = 0;
    }

    private void Update()
    {
        Debug.Log("ACTIVATED?: " + isActive);
        if (byTime) { DisableByTime(); }
        if (secondJumpTrigger) { SecondJumpActivation(); }
    }

    public bool IsActive()
    {
        return isActive;
    }

    private void DisableByTime()
    {
        time += Time.deltaTime;
        if (time > activeDuration) { isActive = false; }
    }

    private void SecondJumpActivation()
    {
        if (myCollider.IsTouchingLayers(LayerMask.GetMask("Player")) && Input.GetButtonDown("Jump") && !isActive)
        {
            isActive = true;
            time = 0;
        }
        else if (myCollider.IsTouchingLayers(LayerMask.GetMask("Player")) && 
            Input.GetButtonDown("Jump") && isActive && theSameWayItGetActivated)
        {
            isActive = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Bullet>() != null && shootTrigger && !isActive)
        {
            isActive = true;
            time = 0;
        }
        else if (collision.gameObject.GetComponent<Bullet>() != null && shootTrigger && isActive && theSameWayItGetActivated)
        {
            isActive = false;
        }

        if (collision.gameObject.GetComponent<Player>() != null && passByTrigger && !isActive)
        {
            isActive = true;
            time = 0;
        }
        else if (collision.gameObject.GetComponent<Player>() != null && passByTrigger && isActive && theSameWayItGetActivated)
        {
            isActive = false;
        }
    }

    
}
