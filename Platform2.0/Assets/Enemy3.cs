﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3 : MonoBehaviour
{
    [Header("TYPE:")]
    [SerializeField] bool kamikazi;
    [SerializeField] bool chilling;
    [SerializeField] bool solo;
    [SerializeField] bool boss;

    [Header("Parameters")]
    [SerializeField] float speed;
    [SerializeField] float health;
    [SerializeField] float ascensionSpeed;
    [SerializeField] float antecipationTime;
    [SerializeField] float followForce;
    [SerializeField] float destroyTimer;

    [Header("References")]
    [SerializeField] Transform groundDetector;
    [SerializeField] float rayLength;
    [SerializeField] MoleColisor detector;
    [SerializeField] Transform target;
    [SerializeField] GameObject explosionPrefab;
    

    Rigidbody2D myRigidBody;
    BoxCollider2D myBoxCollider;
    CircleCollider2D myCircleCollider;
    CapsuleCollider2D myCapsuleCollider;
    Player player;
    Animator myAnim;

    Vector2 forceToPlayer;

    bool turning;
    bool stuned;
    float stunTime;
    bool atGround;
    float direction;
    float newSpeed;

    float timer;

    float velocityBack;

    float timeToTurn;
    Vector2 originalScale;
    int counter;

    LayerMask layerMask;

    float shortCircuitTimer;

    float feedbackTimer;
    Renderer rend;
    Vector4 startColor;

    public bool bossActivation;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        myRigidBody = GetComponent<Rigidbody2D>();
        myBoxCollider = GetComponent<BoxCollider2D>();
        myCircleCollider = GetComponent<CircleCollider2D>();
        myCapsuleCollider = GetComponent<CapsuleCollider2D>();
        originalScale = transform.localScale;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        newSpeed = 1;
        myAnim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {

        if (atGround)
        { 
            if (!kamikazi) { Move(); }
            layerMask = 1 << LayerMask.NameToLayer("Ground");
            RaycastHit2D groundInfo = Physics2D.Raycast(groundDetector.position, Vector2.down, rayLength, layerMask);
            if (groundInfo.collider == false && !turning && !stuned && !kamikazi)
            {
                speed *= -1;
                turning = true;
            }
            else if (groundInfo.collider == false && stuned)
            {
                velocityBack = 0;
            }
            else if (turning && groundInfo.collider == true) { turning = false; }

            //myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, myRigidBody.velocity.y);
            if (kamikazi) { StartCoroutine(Follow());}
            Flip();
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (RicBoss.ricDead) { Destroy(gameObject); }

        

        if (solo) { Debug.Log(atGround); Debug.Log(myRigidBody.velocity); }
        if ((detector.triggered && !boss) || boss) { timer += Time.deltaTime; myAnim.SetBool("AtGround", true); }
        if (!atGround && ((detector.triggered && !boss) ||boss) && timer > antecipationTime)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, ascensionSpeed * Time.deltaTime);
            if (new Vector2(transform.position.x,transform.position.y) == new Vector2(target.position.x, target.position.y))
            {
                atGround = true;
                if (kamikazi) { Destroy(gameObject, destroyTimer); Invoke("Explosion", destroyTimer - 0.05f); }
            }
        }

        if (atGround)
        {
            feedbackTimer += Time.deltaTime;
            HitFeedBack();
            stunTime += Time.deltaTime;
            shortCircuitTimer += Time.deltaTime;
            Bullet3Push(); 
        }

        if (chilling) { transform.position = new Vector3(transform.position.x, transform.position.y, 1.1f); }
        transform.position = new Vector3(transform.position.x, transform.position.y, 0.05f);
    }

    public void Activation()
    {
        myAnim.SetBool("AtGround", true);
        transform.position = Vector2.MoveTowards(transform.position, target.position, ascensionSpeed * Time.deltaTime);
        if (new Vector2(transform.position.x, transform.position.y) == new Vector2(target.position.x, target.position.y))
        {
            atGround = true;
        }
        transform.position = new Vector3(transform.position.x, transform.position.y, 1.1f);
    }

    void Explosion() {
        if (boss) { RicBoss.enemyCounter--; }
        GameObject explosion = Instantiate(explosionPrefab,transform.position,Quaternion.identity);
        Destroy(explosion, 1); }

    private void Bullet3Push()
    {
        if (Mathf.Abs(velocityBack) > Mathf.Epsilon) { stuned = true; }
        else { stuned = false; }

        velocityBack *= 0.9f;
        if (Mathf.Abs(velocityBack) < 0.5f) { velocityBack = 0; }

        if (myCircleCollider.IsTouchingLayers(LayerMask.GetMask("Ground"))) { velocityBack = 0; }
        if (!myBoxCollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && stunTime > 1)
        {
            if (!kamikazi) { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, -5); }
            else { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, -10); }
        }
        else { myRigidBody.velocity = new Vector2(myRigidBody.velocity.x, 0); }
    }

    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }


    private void Move()
    {
        myRigidBody.velocity = new Vector2(speed + velocityBack, myRigidBody.velocity.y); 
    }

    IEnumerator Follow()
    {
        Debug.Log(forceToPlayer);
        float velX = Mathf.Clamp(myRigidBody.velocity.x + forceToPlayer.x,-speed,speed);
        myRigidBody.velocity = new Vector2(velX, myRigidBody.velocity.y);
        yield return null;
        
        forceToPlayer = new Vector2(Mathf.Sign(player.transform.position.x - transform.position.x) * followForce, 0);
        Debug.Log("ForceToPlayer: " + forceToPlayer);
    }


    private void Flip()
    {
        float flipDirection;
        if (!kamikazi) { flipDirection = Mathf.Sign(Mathf.Sign(speed)); }//myRigidBody.velocity.x);
        else { flipDirection = Mathf.Sign(Mathf.Sign(myRigidBody.velocity.x)); }
        Vector2 newScale = new Vector2(Mathf.Abs(originalScale.x) * flipDirection, transform.localScale.y);
        transform.localScale = newScale;
    }



    private void ChangeDirection()
    {
        timeToTurn += Time.deltaTime;
        if (timeToTurn > 0.2)
        {
            if (myCircleCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            {
                speed *= -1;
                timeToTurn = 0;
            }
            if (!myBoxCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            {
                speed *= -1;
                timeToTurn = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<MoleColisor>() != null) { return; }
        if (collision.gameObject.GetComponent<Bullet>() != null || (collision.gameObject.CompareTag("Short Circuit") &&
            shortCircuitTimer > 1))
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            feedbackTimer = 0;
            if (collision.gameObject.CompareTag("Short Circuit"))
            {
                shortCircuitTimer = 0;
            }
            if (collision.gameObject.CompareTag("Player Bullet 3"))
            {
                if (collision.gameObject.GetComponent<Bullet>().GetComponent<Rigidbody2D>().velocity.x > Mathf.Epsilon)
                { velocityBack = 7; }
                else { velocityBack = -7; }
                Debug.Log("FoundBullet");
                myRigidBody.velocity = new Vector2(0, myRigidBody.velocity.y);
                Debug.Log("Myvelocity: " + myRigidBody.velocity.x);
                myRigidBody.AddForce(new Vector2(velocityBack, 0));
            }
            if (health <= 0)
            {
                FindObjectOfType<Player>().GetComponent<Player>().AddTecnology((int)Random.Range(15, 50));
                if (boss && !kamikazi && !RicBoss.lastPart) { RicBoss.enemyCounter--; }
                else if (boss && !kamikazi && RicBoss.lastPart) { RicBoss.lastCounter--; }
                if (kamikazi) { Explosion(); }
                Destroy(gameObject);
            }
        }
    }

}
