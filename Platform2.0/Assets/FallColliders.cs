﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallColliders : MonoBehaviour
{
    public bool playerFelt;

    private void PlayerRespawn() { playerFelt = false; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() == null) { return; }
        playerFelt = true;
        //Invoke("PlayerRespawn",2);
    }
}
