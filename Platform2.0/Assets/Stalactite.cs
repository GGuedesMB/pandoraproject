﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stalactite : MonoBehaviour
{
    [SerializeField] GameObject destroyVF;

    Collider2D myCollider;
    Rigidbody2D myRigidBody;

    FallingPlatform collisionPlat;

    bool child;


    private void Start()
    {
        myCollider = GetComponent<Collider2D>();
        myRigidBody = GetComponent<Rigidbody2D>();
    }


    void InstantiateVF()
    {
        GameObject vf = Instantiate(destroyVF,transform.position,Quaternion.identity);
        Destroy(vf);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (myCollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && collision.gameObject.GetComponent<FallingPlatform>() == null)
        {
            //Invoke("InstantiateVF", 0.2f);
            myRigidBody.gravityScale = 0;
            myRigidBody.velocity = new Vector2 (0,0);
            Destroy(gameObject, 1);
        }
    }
}
