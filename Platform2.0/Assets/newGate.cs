﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class newGate : MonoBehaviour
{
    [SerializeField] bool solo;
    [SerializeField] bool closePermanently;
    [SerializeField] int closeAtThisCamera;
    [SerializeField] GameObject enemyCollider;

    public Animator myAnimator;
    BoxCollider2D myBox;
    CameraController cameraController;
    Som som;

    public bool closeGatePermanently;
    bool isOpen;
    bool isClosed;

    // Start is called before the first frame update
    void Start()
    {
        cameraController = FindObjectOfType<CameraController>();
        myAnimator = GetComponent<Animator>();
        myBox = GetComponent<BoxCollider2D>();
        som = FindObjectOfType<Som>();
    }

    private void Update()
    {
        if (solo)
        {
            //Debug.Log("isop: " + isOpen);
            Debug.Log("ClosePermanently" + closeGatePermanently);
        }
        myAnimator.SetBool("isOpen", isOpen);
        myAnimator.SetBool("isClosed", isClosed);
    }

    void EnableCollisor()
    {
        myBox.enabled = true;
        if (enemyCollider != null) { enemyCollider.SetActive(true); }
    }

    void DisableCollisor()
    {
        myBox.enabled = false;
        if (enemyCollider != null) { enemyCollider.SetActive(false); }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (closePermanently && cameraController.cameraIndex >= closeAtThisCamera) { return; }
        if (collision.gameObject.GetComponent<Player>() != null && !IsInvoking("DisableCollisor"))
        {
            som.Portao(transform.position);
            isClosed = false;
            isOpen = true;
            Invoke("DisableCollisor", 1);
            if (solo)
            {
                Debug.Log("afterEnter: " + isOpen);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //if (closePermanently && cameraController.cameraIndex >= closeAtThisCamera) { return; }
        if (collision.gameObject.GetComponent<Player>() != null && !IsInvoking("EnableCollisor"))
        {
            som.Portao(transform.position);
            isOpen = false;
            isClosed = true;
            Invoke("EnableCollisor", 1);
            if (solo)
            {
                Debug.Log("afterLeave: " + isOpen);
            }
        }
    }
}
