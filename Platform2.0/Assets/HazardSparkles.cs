﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardSparkles : MonoBehaviour
{
    [SerializeField] int atCamera;
    [SerializeField] int oneChanceIn;
    [SerializeField] GameObject sparkles;
    [SerializeField] bool right;
    [SerializeField] bool down;
    [SerializeField] bool up;
    [SerializeField] float halfVerticalAmplitude;
    [SerializeField] float halfHorizontalAmplitude;

    Som som;
    CameraController cameraController;

    int randomFactor;
    // Start is called before the first frame update
    void Start()
    {
        som = FindObjectOfType<Som>();
        cameraController = FindObjectOfType<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        randomFactor = (int)Random.Range(0,oneChanceIn);
        if (randomFactor == 1 && cameraController.cameraIndex == atCamera)
        {
            som.Choq_Contato(transform.position);
            GameObject sparklesParticles = Instantiate(sparkles, new Vector3
                (Random.Range(transform.position.x - halfHorizontalAmplitude, transform.position.x + halfHorizontalAmplitude),
                Random.Range(transform.position.y - halfVerticalAmplitude, transform.position.y + halfVerticalAmplitude),
                transform.position.z), Quaternion.identity);
            float rotY;
            if (right) { rotY = 180; }
            else if (down) { rotY = 90; }
            else if (up) { rotY = 270; }
            else { rotY = 0; }

            sparklesParticles.transform.rotation = Quaternion.Euler(0, rotY, 0);
            Destroy(sparklesParticles, 2);
        }
    }
}
