﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RicBoss : MonoBehaviour
{
    [Header("Start Parameters")]
    [SerializeField] float startDelay;
    [SerializeField] int health;
    [SerializeField] int tiredDeltaLife;
    public bool activated;

    [Header("Habilities")]
    [SerializeField] float dashForce;
    [SerializeField] float dashCooldown;
    [SerializeField] float dashDeltaLife;
    [SerializeField] float dashTargetAimLength;
    [SerializeField] float dashLength;
    [SerializeField] float flyingSpeed;
    [SerializeField] float tiredDuration;
    [SerializeField] float summonCD;
    [SerializeField] int summonDeltaLife;

    [Header("References")]
    [SerializeField] Player player;
    [SerializeField] Transform summonWaypoint;
    [SerializeField] Transform[] enemies3SpawnPoints0;
    [SerializeField] Transform[] enemies3SpawnPoints1;
    [SerializeField] Transform[] enemies3SpawnPoints2;
    [SerializeField] Transform[] enemies3SpawnPoints3;
    [SerializeField] Transform[] enemies3SpawnPoints4;
    [SerializeField] Transform[] enemies4SpawnPoints0;
    [SerializeField] Transform[] enemies4SpawnPoints1;
    [SerializeField] Transform[] enemies4SpawnPoints2;
    [SerializeField] Transform[] enemies4SpawnPoints3;
    [SerializeField] Transform[] enemies4SpawnPoints4;
    [SerializeField] Transform[] lastSpawns;
    [SerializeField] GameObject[] enemy3;
    [SerializeField] GameObject enemy4;
    [SerializeField] FallingPlatform[] fPlat;
    [SerializeField] FallingPlatform centerPlat;
    //[SerializeField] Text fases;
    [SerializeField] GameObject finalVF;


    //References
    Rigidbody2D myRigidBody;
    CapsuleCollider2D myCollider;
    Animator anim;
    Renderer rend;
    

    //Variables
    GameObject[] enemies;

    Vector3 target;
    Vector3 originalScale;
    Vector4 startColor;
    //Vector2 dist;
    //Vector2 unitVector;
    //Vector2 dashRef;
    //Vector2 forceToPlayer;

    //bool think;
    //bool isReady;
    bool[] enemyDead;
    public bool willDash;
    public bool willSummon;
    bool tired;
    bool spawned;
    bool hasEnemies;
    public static bool lastPart;
    public static bool ricDead;

    public static int enemyCounter;
    public static int lastCounter;
    int maxHealth;
    int currentHealth;
    int tiredHealthMark;
    int summonLevel;
    int summonPointsIndex;
    int lifeCounter;

    float magnitude;
    float dashTimer;
    float dashSmooth;
    float feedbackTimer;


    //States
    //Summoning
    //Dashing
    //??

        /*
         CICLO: Uma opção é o ciclo mais rigido. Transição entre dashs e invocação. Ele fica cansado sempre depois que invoca e voce joga a estalactite/atira nele;
         PLATAFORMAS SENSÍVEIS: Uma outra opção pro dash é colocá-las de maneira aleatória setando varias posições e sorteando-as;

        LIFE COUNTER: life counter vai contar quantas vezes richarlyson ficou cansado. Para cada ponto no life counter a ideia é adicionar novos parâmetros.
        Quantos life counters teremos?
            Parametros que podem ser alterados: certeza --> waves e plataformas sensiveis; possibilidades --> intervalo entre o dash. dar o dash logo depois de invocar os inimigos
         */

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        myCollider = GetComponent<CapsuleCollider2D>();
        myRigidBody = GetComponent<Rigidbody2D>();
        rend = GetComponent<Renderer>();
        maxHealth = health;
        willDash = false;
        willSummon = false;
        Invoke("IvkDash", startDelay);
        enemies = new GameObject[100];
        tiredHealthMark = health;
        currentHealth = health;
        lifeCounter = 0;
        summonLevel = 0;
        startColor = rend.material.color;
        originalScale = transform.localScale;
        spawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        /*Debug.Log("Spawned: " + spawned);
        Debug.Log("Counter: " + enemyCounter);*/

        //fases.text = lifeCounter.ToString();
        if (!activated) { return; }

        if (ricDead) { finalVF.gameObject.SetActive(true); anim.SetBool("TiredBool", true); return; }

        //if (Input.GetKeyDown(KeyCode.Alpha0)) { lifeCounter = 3; tired = true; }

        feedbackTimer += Time.deltaTime;
        HitFeedBack();     

        if (!willDash && !willSummon) { /*Debug.Log("AfterDash/Summon");*/ target = transform.position; /*myRigidBody.gravityScale = 1;*/ }
        if (tiredHealthMark - health > tiredDeltaLife) { /*Debug.Log("WillGetTired");*/ tired = true; }

        if (tired && !willDash && !willSummon && !IsInvoking("IvkTiredCD"))
        {
            Debug.Log("tired");
            Invoke("IvkCollider", 1); 
            Invoke("IvkTiredAnim", 1);
            myRigidBody.gravityScale = 1;
            willDash = false;
            willSummon = false;
            Invoke("IvkTiredCD", tiredDuration);
            Invoke("IvkDash", tiredDuration + 2);
        }

        if (willDash)
        {
            //willSummon = false;
            myRigidBody.gravityScale = 0;
            myCollider.enabled = false;
            dashTimer += Time.deltaTime;
            if (dashTimer < dashCooldown + dashTargetAimLength) { target = player.transform.position; LookAtPlayer(); }
            if (dashTimer > dashCooldown)
            {
                //transform.position = Vector2.SmoothDamp(transform.position, target, ref dashRef, dashLength);
                transform.position = Vector2.Lerp(transform.position, target, dashForce);
            }
            if (dashTimer > dashCooldown + dashLength) { dashTimer = 0; }
            if (currentHealth - health > dashDeltaLife)
            {
                Debug.Log("EndDash");
                willDash = false;    
                if (!tired) { Invoke("IvkSummon", 1); }
                for (int i = 0; i < fPlat.Length; i++)
                { fPlat[i].FallCommand(); fPlat[i].dontRespawn = true; }
                centerPlat.FallCommand(); centerPlat.dontRespawn = true;
            }
            
        }

        /*
        if (willDash)
        {
            myRigidBody.gravityScale = 0;
            dashTimer += Time.deltaTime;
            if (dashTimer > dashCooldown)
            {
                dist = new Vector2(player.transform.position.x - transform.position.x,
           player.transform.position.y - transform.position.y);
                magnitude = Mathf.Sqrt((dist.x * dist.x) + (dist.y * dist.y));
                unitVector = new Vector2(dist.x / magnitude, dist.y / magnitude);
                forceToPlayer = new Vector2(unitVector.x * dashForce, unitVector.y * dashForce);
                myRigidBody.AddForce(forceToPlayer);
                dashTimer = 0;
            }
            myRigidBody.velocity = new Vector2(myRigidBody.velocity.x * dashReduction, myRigidBody.velocity.y * dashReduction);
            if (dashTimer > 0.1f && Mathf.Abs(myRigidBody.velocity.x) > 0.1f && Mathf.Abs(myRigidBody.velocity.y) > 0.1f && myCollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
            { myRigidBody.velocity = new Vector2(0, 0); }
            if (currentHealth - health > dashDeltaLife) { willDash = false; dashTimer = 0; }
        }
        */

        /*if (willDash)
        {
            //Debug.Log("dash");
            myRigidBody.gravityScale = 0;
            if ((transform.position == target || (Mathf.Abs(myRigidBody.velocity.x) < 0.1f && Mathf.Abs(myRigidBody.velocity.y) < 0.1f)) && !IsInvoking("IvkBugFixer"))
            { Debug.Log("Reached"); isReady = false; Invoke("Dash", dashCooldown); Invoke("IvkBugFixer", dashCooldown + 0.2f); }
            else if (isReady)
            {
                transform.position = Vector2.SmoothDamp(transform.position, target,ref dashRef, dashSmooth);
                if (health - currentHealth > dashDeltaLife) { willDash = false; }
            }
        }*/

        if (willSummon)
        {
            
            willDash = false;
            myRigidBody.gravityScale = 0;
            myCollider.enabled = false;
            //for (int i = 0;i < fPlat.Length; i++) { fPlat[i].dontRespawn = true; }
            if (transform.position != summonWaypoint.position)
            {
                FlipToDirection();
                transform.position = Vector2.MoveTowards(transform.position, summonWaypoint.position, flyingSpeed * Time.deltaTime);
                //for (int i = 0; i < fPlat.Length; i++) { fPlat[i].FallCommand(); } centerPlat.FallCommand(); 
            }
            else 
            {
                //Debug.Log("CHEGOU!");
                if (!spawned) { Debug.Log("summon"); EnemiesSpawn(); spawned = true; anim.SetBool("SummonBool",true); }
                //EnemiesSpawn();
                //Invoke("IvkSpawnCoolDown", summonCD);
                //Invoke("IvkBugFixer", summonCD);
                if (enemyCounter > 0) { for (int i = 0; i < fPlat.Length; i++) { fPlat[i].FallCommand(); } centerPlat.FallCommand(); }
                if (spawned && enemyCounter <= 0 && !IsInvoking("IvkAfterSummon"))
                {
                    anim.SetBool("SummonBool", false);
                    for (int i = 0;i < 4; i++)
                    { fPlat[i].dontRespawn = false; fPlat[i].Respawn(); fPlat[i].dontRespawn = true; }
                    centerPlat.dontRespawn = false; centerPlat.Respawn(); centerPlat.dontRespawn = true;
                    Invoke("IvkAfterSummon", 7);
                    if (!tired) { Invoke("IvkDash", 9); }
                }
            }
        }

        if (lastPart)
        {
            int random;
            if (lastCounter < 3)
            {
                random = Random.Range(0, 10);
                enemies[random] = Instantiate(enemy3[Random.Range(0,2)], enemies3SpawnPoints0[random].position, Quaternion.identity);
                lastCounter++;
            }
        }

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, 319.33f, 354.57f), 
            Mathf.Clamp(transform.position.y, 77.17f, 94.88f));
    }

    //Methods
    void EnemiesSpawn()
    {
        int random;
        enemyCounter = 0;
        anim.SetTrigger("Summon");
        //if (health - currentHealth > summonDeltaLife) { willSummon = false; summonLevel++; summonPointsIndex++; }
        //else
        //{
            if (summonLevel == 0)
            {
                for (int i = 0; i < enemies3SpawnPoints0.Length; i++)
                { 
                    random = Random.Range(0, 2);                
                    enemies[i] = Instantiate(enemy3[random], enemies3SpawnPoints0[i].position, Quaternion.identity);
                    enemyCounter++;
                }
                for (int i = 0; i < enemies4SpawnPoints0.Length; i++)
                {
                    enemies[i] = Instantiate(enemy4, enemies4SpawnPoints0[i].position, Quaternion.identity);
                    enemyCounter++;
                }
            }
            else if (summonLevel == 1)
            {
                for (int i = 0; i < enemies3SpawnPoints1.Length; i++)
                {
                    random = Random.Range(0, 2);
                    enemies[i] = Instantiate(enemy3[random], enemies3SpawnPoints1[i].position, Quaternion.identity);
                    enemyCounter++;
                }
                for (int i = 0; i < enemies4SpawnPoints1.Length; i++)
                {
                    enemies[i] = Instantiate(enemy4, enemies4SpawnPoints1[i].position, Quaternion.identity);
                    enemyCounter++;
                }
            }
            else if (summonLevel == 2)
            {
                for (int i = 0; i < enemies3SpawnPoints2.Length; i++)
                {
                    random = Random.Range(0, 2);
                    enemies[i] = Instantiate(enemy3[random], enemies3SpawnPoints2[i].position, Quaternion.identity);
                    enemyCounter++;
                }
                for (int i = 0; i < enemies4SpawnPoints2.Length; i++)
                {
                    enemies[i] = Instantiate(enemy4, enemies4SpawnPoints2[i].position, Quaternion.identity);
                    enemyCounter++;
                }
            }
            else if (summonLevel == 3)
            {
                for (int i = 0; i < enemies3SpawnPoints3.Length; i++)
                {
                    random = Random.Range(0, 2);
                    enemies[i] = Instantiate(enemy3[random], enemies3SpawnPoints3[i].position, Quaternion.identity);
                    enemyCounter++;
                }
                for (int i = 0; i < enemies4SpawnPoints3.Length; i++)
                {
                    enemies[i] = Instantiate(enemy4, enemies4SpawnPoints3[i].position, Quaternion.identity);
                    enemyCounter++;
                }
            }
            else if (summonLevel == 4)
            {
                for (int i = 0; i < enemies3SpawnPoints4.Length; i++)
                {
                    random = Random.Range(0, 2);
                    enemies[i] = Instantiate(enemy3[random], enemies3SpawnPoints4[i].position, Quaternion.identity);
                    enemyCounter++;
                }
                for (int i = 0; i < enemies4SpawnPoints4.Length; i++)
                {
                    enemies[i] = Instantiate(enemy4, enemies4SpawnPoints4[i].position, Quaternion.identity);
                    enemyCounter++;
                }
            }
        //}
        
    }
    //void Dash() { isReady = true; target = player.transform.position; }
    void HitFeedBack()
    {
        if (feedbackTimer < 0.16f) { rend.material.color = new Vector4(255, 0, 0, 255); }
        else { rend.material.color = startColor; }
    }
    void LookAtPlayer()
    {
        float dir = Mathf.Sign(player.transform.position.x - transform.position.x);
        transform.localScale = new Vector2(Mathf.Abs(originalScale.x)*dir,originalScale.y);
    }
    void FlipToDirection()
    {
        float dir = Mathf.Sign(summonWaypoint.position.x - transform.position.x);
        transform.localScale = new Vector2(Mathf.Abs(originalScale.x) * dir, originalScale.y);
    }

    //Invoke functions
    //void IvkThink() { think = true; }
    void IvkDash()
    {
        if (tired) { return; }
        currentHealth = health; willDash = true;
        if (lifeCounter == 0) { for (int i = 0; i < fPlat.Length; i++)
            { fPlat[i].dontRespawn = false; fPlat[i].Respawn(); } centerPlat.dontRespawn = false; centerPlat.Respawn();}
        else if (lifeCounter == 1) { for (int i = 0; i < fPlat.Length; i++)
        { if (i != 2 && i != 4 && i != fPlat.Length - 1) { fPlat[i].dontRespawn = false; fPlat[i].Respawn(); } }
            centerPlat.dontRespawn = false; centerPlat.Respawn(); }
        else if (lifeCounter == 2) { for (int i = 0; i < fPlat.Length; i++)
            { if (i != 3 && i != 5 && i != fPlat.Length - 1) { fPlat[i].dontRespawn = false; fPlat[i].Respawn(); } }
        centerPlat.dontRespawn = false; centerPlat.Respawn(); }
        else { for (int i = 0; i < fPlat.Length; i++) { fPlat[i].dontRespawn = false; fPlat[i].Respawn(); }
        centerPlat.dontRespawn = false; centerPlat.Respawn(); }
    }
    void IvkSummon()
    {
        if (tired) { return; }
        Debug.Log("SummonInvoked"); currentHealth = health; willSummon = true; spawned = false;
        for (int i = 0; i < fPlat.Length; i++) { fPlat[i].FallCommand(); }
        centerPlat.FallCommand();
    }
    void IvkAfterSummon()
    {
        willSummon = false;
    }
    void IvkBugFixer()
    {
        Debug.Log("BugFixer");
    }
    void IvkTiredCD()
    {
        Debug.Log("EndTired ");
        anim.SetBool("TiredBool",false);
        tired = false;
        tiredHealthMark = health;
        summonLevel++;
        lifeCounter++;
        myRigidBody.gravityScale = 0;
        if (lifeCounter == 2) { /*dashCooldown -= 1f;*/ }
        else if (lifeCounter == 3) { lastPart = true; }
        else if (lifeCounter == 4) { ricDead = true; StartCoroutine(FindObjectOfType<LevelExit>().ExitLevel()); }
    }
    void IvkCollider()
    {
        Debug.Log("Disable Collider"); myCollider.enabled = true;
    }
    void IvkTiredAnim()
    {
        anim.SetBool("TiredBool", true);
    }

    //Collisions
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Bullet>() != null)
        {
            health -= collision.gameObject.GetComponent<DamageDealer>().GetDamage();
            feedbackTimer = 0;
        }
    }
}
