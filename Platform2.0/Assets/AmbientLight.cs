﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientLight : MonoBehaviour
{
    [SerializeField] int whichCameraIsAt;

    CameraController cameraController;

    Light myLight;

    float intensity;

    // Start is called before the first frame update
    void Start()
    {
        cameraController = FindObjectOfType<CameraController>();
        myLight = GetComponent<Light>();
        intensity = myLight.intensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (whichCameraIsAt > cameraController.cameraIndex + 1 || whichCameraIsAt < cameraController.cameraIndex - 1)
        {
            myLight.intensity = 0;
        }
        else { myLight.intensity = intensity; }
    }
}
