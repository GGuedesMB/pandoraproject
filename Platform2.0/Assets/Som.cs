﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Som : MonoBehaviour
{
    [SerializeField] Player pj;
    [SerializeField] CameraShake[] cameras;

    [Header("Parametros")]
    [SerializeField] public bool f1;
    [SerializeField] public bool f2;
    [SerializeField] bool start;
    [SerializeField] bool gameover;
    [SerializeField] bool menu;
    [SerializeField] public bool Tiro_Normal;
    [SerializeField] public bool Tiro_Bloqueador;
    [SerializeField] public bool Tiro_Impacto;
    [SerializeField] public bool Tiro_Rapido;
    [SerializeField] public bool Tiro_Dano;

    //Misc
    [Header("Misc")]
    [SerializeField] AudioClip fase1_musica;
    [SerializeField] AudioClip fase2_musica;
    [SerializeField] AudioClip fase1_ambiente;
    [SerializeField] AudioClip fase2_ambiente;
    [SerializeField] AudioClip portao;
    [SerializeField] AudioClip checkpoint;
    [SerializeField] AudioClip coletavel;
    [SerializeField] AudioClip int_clique;
    [SerializeField] AudioClip int_radio;
    [SerializeField] AudioClip equip_upgd;
    [SerializeField] AudioClip elevador;

    //PJ
    [Header("PJ")]
    [SerializeField] float TempoPasso;
    [SerializeField] float TempoTiro;
    [SerializeField] AudioClip[] pj_passo_nave;
    [SerializeField] AudioClip[] pj_passo_caverna;
    [SerializeField] AudioClip pj_dash;
    [SerializeField] AudioClip pj_tiro_normal;
    [SerializeField] AudioClip pj_tiro_bloqueador;
    [SerializeField] AudioClip pj_tiro_impacto;
    [SerializeField] AudioClip pj_tiro_rapido;
    [SerializeField] AudioClip pj_tiro_dano;
    [SerializeField] AudioClip pj_tiro_bloq_impacto;
    [SerializeField] AudioClip pj_morte;
    [SerializeField] AudioClip pj_dano;
    [SerializeField] AudioClip pj_pulo;
    [SerializeField] AudioClip pj_pulo2; //Double jump
    [SerializeField] AudioClip pj_queda; //Contato com o chão.
    [SerializeField] AudioClip pj_idle;
    [SerializeField] AudioClip pj_batimento; //0 escudos.
    [SerializeField] AudioClip bulletCollision;

    //Inimigos...... ene = enemy.
    [Header("Inimigos")]
    [SerializeField] AudioClip ene_recebe_dano;
    [SerializeField] AudioClip ene_morre;
    [SerializeField] AudioClip ene2_antecipacao;
    [SerializeField] AudioClip ene2_tiro;
    [SerializeField] AudioClip ene1_passo;
    [SerializeField] AudioClip fe_voo;
    [SerializeField] AudioClip ene3_passo;
    [SerializeField] AudioClip ene3_ant;
    [SerializeField] AudioClip ene4_morte;
    [SerializeField] AudioClip star_tiro;
    [SerializeField] AudioClip explosao;

    //Hazards
    [Header("Hazards")]
    [SerializeField] AudioClip choq_contato;
    [SerializeField] AudioClip choq_conti;
    [SerializeField] AudioClip estala_ant;
    [SerializeField] AudioClip estala_queda;
    [SerializeField] AudioClip canhao_ant;
    [SerializeField] AudioClip canhao_tiro;

    //Plataformas
    [Header("Plataformas")]
    [SerializeField] AudioClip plat_movel;
    [SerializeField] AudioClip plat_queb;
    [SerializeField] AudioClip plat_sens_ant;
    [SerializeField] AudioClip plat_sens_queda;

    //Boss 1
    [Header("Boss 1")]
    [SerializeField] AudioClip b1_ant_dash;
    [SerializeField] AudioClip b1_dash;
    [SerializeField] AudioClip b1_morte;
    [SerializeField] AudioClip b1_passo;
    [SerializeField] AudioClip b1_ant_pulo;
    [SerializeField] AudioClip b1_pulo;


    CameraShake cameraShake;
    CameraController cameraController;
    Vector3 pj_pos;
    Vector3 cameraPos;

    float timer_passo;
    float timer_tiro;
    float volume = 1;
    bool passo;
    bool tiro;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Musica Tocando");
        //Musica();
        Debug.Log("Ambiente Tocando");
        //Ambiente();
        cameraController = FindObjectOfType<CameraController>();
    }

    // Update is called once per frame
    void Update()
    {
        pj_pos = new Vector3(pj.transform.position.x, pj.transform.position.y, pj.transform.position.z);
        cameraPos = new Vector3(cameras[cameraController.cameraIndex].transform.position.x, 
            cameras[cameraController.cameraIndex].transform.position.y, cameras[cameraController.cameraIndex].transform.position.z);

        //PJ
        //if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow)) { Debug.Log("Andou"); Pj_Passo(); }
        /*
        if (Input.GetButtonDown("Horizontal")) { passo = true; }
        if (Input.GetButtonUp("Horizontal")) { passo = false; }
        if (Input.GetKeyDown(KeyCode.LeftShift)) { Debug.Log("Dash"); Pj_Dash(); }
        if (Input.GetButtonDown("Fire1")) { tiro = true; }
        if (Input.GetButtonUp("Fire1")) { tiro = false; }
        if (pj.GetHealth2() == 0) { Debug.Log("Batimento"); Pj_Batimento(); }
        if (pj.GetHealth2() < 0) { Debug.Log("Morreu"); Pj_Morte(); }
        if (Input.GetKeyDown(KeyCode.O)) { Debug.Log("Tomou Dano"); Pj_Dano(); }
        if (Input.GetKeyDown(KeyCode.UpArrow)) { Debug.Log("Pulou"); Pj_Pulo(); }
        if (Input.GetKeyDown(KeyCode.I)) { Debug.Log("Pulou 2"); Pj_Pulo2(); }
        if (Input.GetKeyDown(KeyCode.P)) { Debug.Log("Caiu"); Pj_Queda(); }
        if (Input.GetKeyDown(KeyCode.Y)) { Debug.Log("Idle"); Pj_Idle(); }
        */
        //Debug.Log(passo);
        //if (passo) { Pj_Passo(); }
        //if (tiro) { Pj_Tiro(); }

        //Inimigos
        /*
        if (Input.GetKeyDown(KeyCode.R)) { Debug.Log("Ene toma dano"); Ene_Recebe_Dano(); }
        if (Input.GetKeyDown(KeyCode.F)) { Debug.Log("Ene morre"); Ene_Morre(); }
        if (Input.GetKeyDown(KeyCode.E)) { Debug.Log("Ene2 Antecipacao"); Ene2_Antecipacao(); }
        if (Input.GetKeyDown(KeyCode.Q)) { Debug.Log("Ene2 Atira"); Ene2_Tiro(); }
        if (Input.GetKeyDown(KeyCode.G)) { Debug.Log("Ene1 passo"); Ene1_Passo(); }
        if (Input.GetKeyDown(KeyCode.H)) { Debug.Log("Fe voo"); Fe_Voo(); }
        */

        //Hazards
        /*
        if (Input.GetKeyDown(KeyCode.N)) { Debug.Log("Choque Contato"); Choq_Contato(); }
        if (Input.GetKeyDown(KeyCode.B)) { Debug.Log("Choque Continuo"); Choq_Conti(); }
        if (Input.GetKeyDown(KeyCode.V)) { Debug.Log("Estalactite Queda"); Estala_Queda(); }
        */

        //Boss 1
        /*
        if (Input.GetKeyDown(KeyCode.J)) { Debug.Log("B1 Dash"); B1_Dash(); }
        if (Input.GetKeyDown(KeyCode.K)) { Debug.Log("B1 Morte"); B1_Morte(); }
        if (Input.GetKeyDown(KeyCode.L)) { Debug.Log("B1 Passo"); B1_Passo(); }
        if (Input.GetKeyDown(KeyCode.M)) { Debug.Log("B1 Pulo"); B1_Pulo(); }
        */
        //if(cameraController.cameraIndex >= 19) { }
    }

    //Misc
    public void Musica()
    {
        if (f1)
        {
            //if (menu) { volume = 0.5f; } else { volume = 1; }
            //AudioSource.PlayClipAtPoint(fase1_musica, pj.transform.position, 0.4f);
        }
        if (f2)
        {
            //if (menu) { volume = 0.5f; } else { volume = 1; }
            //AudioSource.PlayClipAtPoint(fase2_musica, pj.transform.position, 0.4f);
        }
    }
    public void Ambiente()
    {
        if (f1)
        {
            //if (menu) { volume = 0.5f; } else { volume = 1; }
            AudioSource.PlayClipAtPoint(fase1_ambiente, pj_pos, 1);
        }
        if (f2)
        {
            //if (menu) { volume = 0.5f; } else { volume = 1; }
            AudioSource.PlayClipAtPoint(fase2_ambiente, pj_pos, 1);
        }
    }

    public void Portao(Vector3 pos) { AudioSource.PlayClipAtPoint(portao, pos, 1); } //trocar posição de saída do som.
    public void Checkpoint() { AudioSource.PlayClipAtPoint(checkpoint, pj.transform.position, 1); } //trocar posição de saída do som.
    public void Coletavel() { AudioSource.PlayClipAtPoint(coletavel, pj.transform.position, 1); } //trocar posição de saída do som.
    public void Int_Clique(Vector3 pos) { AudioSource.PlayClipAtPoint(int_clique, pj.transform.position, 1); } //trocar posição de saída do som.
    public void Int_Radio(Vector3 pos) { AudioSource.PlayClipAtPoint(int_radio, pos, 1); } //trocar posição de saída do som.
    public void Equip_Upgrd(Vector3 pos) { AudioSource.PlayClipAtPoint(equip_upgd, pos, 1); } //trocar posição de saída do som.
    public void Elevador() { AudioSource.PlayClipAtPoint(elevador, pj.transform.position, 1); } //trocar posição de saída do som.


    //PJ
    public void Pj_Passo(Vector3 pos)
    {
        if (f1)
        {

            timer_passo += Time.deltaTime;
            int abelha;
            if (timer_passo > TempoPasso)
            {
                abelha = Random.Range(0, pj_passo_nave.Length);
                AudioSource.PlayClipAtPoint(pj_passo_nave[1], pos, 1);
                timer_passo = 0;
            }
        }
        if (f2)
        {
            timer_passo += Time.deltaTime;
            int abelha;
            if (timer_passo > TempoPasso)
            {
                abelha = Random.Range(0, pj_passo_caverna.Length);
                AudioSource.PlayClipAtPoint(pj_passo_caverna[5], pj.transform.position, 1);
                timer_passo = 0;
            }
        }
    }
    public void Pj_Tiro()
    {
        if (GameGlobalInfo.bulletChip == 1)
        {
            AudioSource.PlayClipAtPoint(pj_tiro_normal, pj.transform.position, 1);
        }
        if (GameGlobalInfo.bulletChip == 2)
        {
                AudioSource.PlayClipAtPoint(pj_tiro_bloqueador, pj.transform.position, 1);
        }
        if (GameGlobalInfo.bulletChip == 3)
        {
                AudioSource.PlayClipAtPoint(pj_tiro_impacto, pj.transform.position, 1);
        }
        if (GameGlobalInfo.bulletChip == 4)
        {
                AudioSource.PlayClipAtPoint(pj_tiro_rapido, pj.transform.position, 1);
        }
        if (GameGlobalInfo.bulletChip == 5)
        {
                AudioSource.PlayClipAtPoint(pj_tiro_dano, pj.transform.position, 0.5f);
        }
    }

    public void Pj_Dash() { AudioSource.PlayClipAtPoint(pj_dash, pj_pos, 1); }
    public void Pj_Tiro_Bloq_Impacto() { AudioSource.PlayClipAtPoint(pj_tiro_bloq_impacto, pj_pos, 1); }
    public void Pj_Morte() { AudioSource.PlayClipAtPoint(pj_morte, pj.transform.position, 1); }
    public void Pj_Dano() { AudioSource.PlayClipAtPoint(pj_dano, pj.transform.position, 1); }
    public void Pj_Pulo(Vector3 pos) { AudioSource.PlayClipAtPoint(pj_pulo, pos, 1); }
    public void Pj_Pulo2(Vector3 pos) { AudioSource.PlayClipAtPoint(pj_pulo2, pos, 1); }
    public void Pj_Queda(Vector3 pos) { AudioSource.PlayClipAtPoint(pj_queda, pos, 1); }
    public void Pj_Idle() { AudioSource.PlayClipAtPoint(pj_idle, pj.transform.position, 1); }
    public void Pj_Batimento() { AudioSource.PlayClipAtPoint(pj_batimento, pj.transform.position, 1); }
    public void Pj_BulletCollision(Vector3 pos) { AudioSource.PlayClipAtPoint(bulletCollision, pos, 1); }

    //Inimigos
    public void Ene_Recebe_Dano(Vector3 pos) { AudioSource.PlayClipAtPoint(ene_recebe_dano, pj.transform.position, 0.75f); } //trocar posição de saída do som.
    public void Ene_Morre(Vector3 pos) { AudioSource.PlayClipAtPoint(ene_morre, pj.transform.position, 1); } //trocar posição de saída do som.
    public void Ene2_Antecipacao(Vector3 pos) { AudioSource.PlayClipAtPoint(ene2_antecipacao, pos, 1); } //trocar posição de saída do som.
    public void Ene2_Tiro(Vector3 pos) { AudioSource.PlayClipAtPoint(ene2_tiro, pos, 1); } //trocar posição de saída do som.
    public void Ene1_Passo(Vector3 pos) { AudioSource.PlayClipAtPoint(ene1_passo, pos, 0.5f); } //trocar posição de saída do som.
    public void Fe_Voo() { AudioSource.PlayClipAtPoint(fe_voo, pj_pos, 1); } //trocar posição de saída do som.
    public void Ene3_Passo() { AudioSource.PlayClipAtPoint(ene3_passo, pj_pos, 1); } //trocar posição de saída do som.
    public void Ene3_Ant() { AudioSource.PlayClipAtPoint(ene3_ant, pj_pos, 1); } //trocar posição de saída do som.
    public void Ene4_Morte() { AudioSource.PlayClipAtPoint(ene4_morte, pj.transform.position, 1); } //trocar posição de saída do som.
    public void Star_Tiro() { AudioSource.PlayClipAtPoint(star_tiro, pj_pos, 1); } //trocar posição de saída do som.
    public void explosao_som() { AudioSource.PlayClipAtPoint(explosao, pj.transform.position, 0.6f); }

    //Hazards
    public void Choq_Contato(Vector3 pos) { AudioSource.PlayClipAtPoint(choq_contato, pos, 1); } //trocar posição de saída do som.
    public void Choq_Conti() { AudioSource.PlayClipAtPoint(choq_conti, pj_pos, 1); } //trocar posição de saída do som.
    public void Estala_Ant() { AudioSource.PlayClipAtPoint(estala_ant, pj_pos, 1); } //trocar posição de saída do som.
    public void Estala_Queda() { AudioSource.PlayClipAtPoint(estala_queda, pj_pos, 1); } //trocar posição de saída do som.
    public void Canhao_Ant(Vector3 pos) { AudioSource.PlayClipAtPoint(canhao_ant, pos, 1); } //trocar posição de saída do som.
    public void Canhao_Tiro(Vector3 pos) { AudioSource.PlayClipAtPoint(canhao_tiro, pos, 1); } //trocar posição de saída do som.

    //Plataformas
    public void Plat_Movel(Vector3 pos) { AudioSource.PlayClipAtPoint(plat_movel, pos, 1); } //trocar posição de saída do som.
    public void Plat_Queb(Vector3 pos) { AudioSource.PlayClipAtPoint(plat_queb, pos, 1); } //trocar posição de saída do som.
    public void Plat_Sens_Ant() { AudioSource.PlayClipAtPoint(plat_sens_ant, pj_pos, 1); } //trocar posição de saída do som.
    public void Plat_Sens_Queda() { AudioSource.PlayClipAtPoint(plat_sens_queda, pj_pos, 1); } //trocar posição de saída do som.

    //Boss 1
    public void B1_Ant_Dash(Vector3 pos) { AudioSource.PlayClipAtPoint(b1_ant_dash, pj.transform.position, 1); } //trocar posição de saída do som.
    public void B1_Dash(Vector3 pos) { AudioSource.PlayClipAtPoint(b1_dash, pj.transform.position, 1); } //trocar posição de saída do som.
    public void B1_Morte() { AudioSource.PlayClipAtPoint(b1_morte, pj.transform.position, 1); } //trocar posição de saída do som.
    public void B1_Passo() { AudioSource.PlayClipAtPoint(b1_passo, pj.transform.position, 1); } //trocar posição de saída do som.
    public void B1_Ant_Pulo(Vector3 pos) { AudioSource.PlayClipAtPoint(b1_ant_pulo, pj.transform.position, 1); } //trocar posição de saída do som.
    public void B1_Pulo(Vector3 pos) { AudioSource.PlayClipAtPoint(b1_pulo, pj.transform.position, 1); } //trocar posição de saída do som.
}
