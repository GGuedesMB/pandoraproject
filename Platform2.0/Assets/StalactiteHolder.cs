﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalactiteHolder : MonoBehaviour
{
    [SerializeField] float antecipationTime;
    [SerializeField] float respawnTime;
    [SerializeField] float gravityScale;
    [SerializeField] StalactiteActivationCollider activationCollider;
    [SerializeField] GameObject stalactitePrefab;
    [SerializeField] GameObject antecipationVF;

    GameObject stalactite;

    bool stopAntecipationParticles;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        stalactite = Instantiate(stalactitePrefab, transform.position, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if (activationCollider.activated)
        {
            timer += Time.deltaTime;
            if (timer > antecipationTime - 0.5f && !stopAntecipationParticles)
            {
                GameObject vf = Instantiate(antecipationVF, transform.position, Quaternion.identity);
                Destroy(vf, 0.5f);
            }
            if (timer > antecipationTime && !IsInvoking("InstantiateGO"))
            {
                stopAntecipationParticles = true;
                if (stalactite.GetComponent<Rigidbody2D>() != null) { stalactite.GetComponent<Rigidbody2D>().gravityScale = gravityScale; }
                Invoke("InstantiateGO", respawnTime);
            }
        }
    }

    void InstantiateGO()
    {
        stopAntecipationParticles = false;
        activationCollider.activated = false;
        timer = 0;
        stalactite = Instantiate(stalactitePrefab, transform.position, Quaternion.identity);
    }
}
