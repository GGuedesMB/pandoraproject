﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayPlatform : MonoBehaviour
{
    [SerializeField] float waitTime;
    [SerializeField] bool solo;
    [SerializeField] bool stop;

    float timer;

    bool is180;

    PlatformEffector2D effector;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        effector = GetComponent<PlatformEffector2D>();
    }

    private void FixedUpdate()
    {
        //if (solo) { Debug.Log("is180: " + is180); }
        /*
        if (is180) { effector.surfaceArc = 0; }
        else { effector.surfaceArc = 180; }
        */
        
    }

    void Update()
    {
        if (solo)
        {
           // Debug.Log("OneWayTimer: " + timer);
           // Debug.Log("Vazador rs: " + effector.surfaceArc);
        }

        
        if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && !stop) 
        {
            if (timer > waitTime)
            {
                effector.surfaceArc = 0;
                timer = 0;
                Invoke("TurnOn", 0.39f);
                stop = true;
            }
            else { timer += Time.deltaTime; }
        }
        if (Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
        {
            timer = 0;
            stop = false;
        }
        
    }

    private void TurnOn()
    {
        effector.surfaceArc = 180;
    }
}
