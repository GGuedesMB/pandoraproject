﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingVF : MonoBehaviour
{
    [SerializeField] GameObject endingPanel;
    Animator anim;
    //AnimatorClipInfo[] animInfo;
    AnimatorStateInfo animInfo;
    Boss boss;

    //AnimationState animation;

    float timer;
    // Start is called before the first frame update
    void Start()
    {
        boss = FindObjectOfType<Boss>();
        timer = 5;
        anim = endingPanel.GetComponent<Animator>();
        animInfo = GetComponent<AnimatorStateInfo>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("animatioon point: " + anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
        //animInfo = anim.GetCurrentAnimatorClipInfo(0);
        if (boss != null) { if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1) { Time.timeScale = 1; boss.endingTimer = 1000; } }
    }

    public void BossEnding()
    { 
        endingPanel.SetActive(true);
        Time.timeScale = 1;
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1) { Time.timeScale = 1; }
    }

    private void FinishEffect()
    {
        endingPanel.SetActive(false);
        Time.timeScale = 1;
    }
}
