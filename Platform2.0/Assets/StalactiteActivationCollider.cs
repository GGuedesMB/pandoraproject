﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StalactiteActivationCollider : MonoBehaviour
{

    public bool activated;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() == null) { return; }
        activated = true;
    }
}
