﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationPop : MonoBehaviour
{

    [SerializeField] GameObject interaction;

    GameObject note;

    bool popped;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Notification()
    {
        note = Instantiate(interaction, new Vector3(transform.position.x, transform.position.y+4,
                transform.position.z), Quaternion.identity);
        popped = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null && !popped)
        {
            if (!CutsceneManager.OnCutscene) { Notification(); }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null && note != null)
        {
            Destroy(note.gameObject);
            popped = false;
        }
    }
}
