﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleColisor : MonoBehaviour
{
    [SerializeField] GameObject terraTremendo;

    public bool triggered;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Player>() == null) { return; }
        if (!triggered)
        {
            GameObject particles = Instantiate(terraTremendo, transform.position, Quaternion.identity);
            Destroy(particles, 4);
        }
        triggered = true;
        GetComponent<Collider2D>().enabled = false;
        
    }
}
